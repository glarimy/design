package code;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BetterClock {
    public static void main(String[] args) {
        Runnable task = new Runnable() {
            ThreadLocal<Integer> counter = new ThreadLocal<>(){
                @Override
                protected Integer initialValue() {
                    return 0;
                }
            };

            @Override
            public void run() {
                int i = 0;
                while (i<10) {
                    counter.set(counter.get()+1);
                    System.out.println(Thread.currentThread().getName() + ": " + counter.get());
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {

                    }
                    i++;
                }
                counter.remove();
            }
        };
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.submit(task);
        service.submit(task);
        service.submit(task);
        service.submit(task);
    }
}
