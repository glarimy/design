package code;

import java.io.File;
import java.io.PrintWriter;

/*
/usr/bin/env /opt/homebrew/Cellar/openjdk@11/11.0.19/libexec/openjdk.jdk/Contents/Home/bin/java -XX:+PrintCompilation -cp /Users/krishnamohankoyya/Library/Application\ Support/Code/User/workspaceStorage/e24698732af6876588a0635dd87b2dad/redhat.java/jdt_ws/performance_85740172/bin code.Logger
/usr/bin/env /opt/homebrew/Cellar/openjdk@11/11.0.19/libexec/openjdk.jdk/Contents/Home/bin/java -XX:Tier3InvocationThreshold=50 -XX:Tier4InvocationThreshold=75 -XX:+PrintCompilation -cp /Users/krishnamohankoyya/Library/Application\ Support/Code/User/workspaceStorage/e24698732af6876588a0635dd87b2dad/redhat.java/jdt_ws/performance_85740172/bin code.Hello
*/

public class Logger {
    public static void print(PrintWriter w, String s) throws Exception {
        w.println("logging...");
    }
    public static void write(PrintWriter w, String s) throws Exception {
        w.println("logging...");
    }
    public static void main(String[] args) throws Exception {
        PrintWriter pw1 = new PrintWriter(new File("glarimy-1.log"));
        PrintWriter pw2 = new PrintWriter(new File("glarimy-2.log"));
        for(int i=0; i<10000; i++){
            print(pw1, "logging...");
        }
        Thread.sleep(1000);
        for(int i=0; i<10000000; i++){
            write(pw2, "logging...");
        }
        Thread.sleep(1000);
        for(int i=0; i<10000; i++){
            print(pw1, "logging...");
        }
        pw1.close();
        pw2.close();
    }
}