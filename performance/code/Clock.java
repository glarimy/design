package code;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Clock {
    public static void main(String[] args) {
        Runnable task = new Runnable() {
            int counter;

            @Override
            public void run() {
                int i = 0;
                while (i<10) {
                    counter++;
                    System.out.println(Thread.currentThread().getName() + ": " + counter);
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {

                    }
                    i++;
                }
            }
        };
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.submit(task);
        service.submit(task);
        service.submit(task);
        service.submit(task);
    }
}
