package code;

public class User {
    private String name;
    private int uid;
    private String[] subscriptions = {"News", "Announcement", "Movies", "Sports", "Politics", "Stocks", "Gossip"};

    public User(String name, int uid) {
        this.name = name;
        this.uid = uid;
        System.out.println(subscriptions);
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getUid() {
        return uid;
    }
    public void setUid(int uid) {
        this.uid = uid;
    }

    
}
