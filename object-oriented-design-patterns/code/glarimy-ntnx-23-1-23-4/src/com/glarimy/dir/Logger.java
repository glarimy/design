package com.glarimy.dir;

import java.util.List;

public class Logger implements Directory {
	private Directory target;

	public Logger(Directory target) {
		this.target = target;
	}

	@Override
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException {
		System.out.println("Logger: add - entering");
		try {
			Employee result = target.add(e);
			System.out.println("Logger: add - leaving");
			return result;
		} catch (DirectoryException de) {
			System.out.println("Logger: add - existing");
			throw de;
		}
	}

	@Override
	public Employee find(int eid) throws EmployeeNotFoundException, DirectoryException {
		System.out.println("Logger: find - entering");
		try {
			Employee result = target.find(eid);
			System.out.println("Logger: find - leaving");
			return result;
		} catch (DirectoryException de) {
			System.out.println("Logger: find - existing");
			throw de;
		}
	}

	@Override
	public List<Employee> search(String name) throws DirectoryException {
		System.out.println("Logger: search - entering");
		try {
			List<Employee> result = target.search(name);
			System.out.println("Logger: search - leaving");
			return result;
		} catch (DirectoryException de) {
			System.out.println("Logger: search - existing");
			throw de;
		}
	}

}
