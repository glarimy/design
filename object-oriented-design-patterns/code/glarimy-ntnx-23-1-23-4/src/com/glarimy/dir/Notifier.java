package com.glarimy.dir;

import java.util.List;

public class Notifier implements Directory {
	private Directory target;

	public Notifier(Directory target) {
		this.target = target;
	}

	@Override
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException {
		try {
			Employee result = target.add(e);
			System.out.println("Notifier:  SMS - employee added successfully");
			return result;
		} catch (DirectoryException de) {
			System.out.println("Notifier:  SMS - failed to add employee");
			throw de;
		}
	}

	@Override
	public Employee find(int eid) throws EmployeeNotFoundException, DirectoryException {
		Employee result = target.find(eid);
		return result;
	}

	@Override
	public List<Employee> search(String name) throws DirectoryException {
		List<Employee> result = target.search(name);
		return result;
	}

}
