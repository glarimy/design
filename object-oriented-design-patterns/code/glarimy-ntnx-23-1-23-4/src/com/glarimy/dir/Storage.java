package com.glarimy.dir;

import java.util.List;

public interface Storage {
	public Employee save(Employee e) throws DirectoryException;
	public Employee fetch(int eid) throws DirectoryException;
	public List<Employee> fetch(String name) throws DirectoryException;
}
