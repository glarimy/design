package com.glarimy.dir;

import java.util.Properties;

public class DirectoryFactory implements Factory {
	private Directory instance;

	public DirectoryFactory(Properties config) throws DirectoryException {
		Storage storage;
		if (config.getProperty("storage").equals("cache"))
			storage = new InMemoryStorage();
		else
			throw new DirectoryException();
		instance = new SimpleDirectory(storage);
		if (config.getProperty("logging").equals("enabled"))
			instance = new Logger(instance);
		if (config.getProperty("sms").equals("enabled"))
			instance = new Notifier(instance);

	}

	@Override
	public Directory getDirectory() throws DirectoryException {
		return instance;
	}

}
