package com.glarimy.dir;

import java.util.List;

public class SimpleDirectory implements Directory {
	private Storage storage;

	public SimpleDirectory(Storage storage) {
		this.storage = storage;
	}

	@Override
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException {
		if (e == null || e.getName() == null || e.getEmail() == null || e.getPhone() < 1)
			throw new InvalidEmployeeException();
		return storage.save(e);
	}

	@Override
	public Employee find(int eid) throws EmployeeNotFoundException, DirectoryException {
		Employee e = storage.fetch(eid);
		if (e != null)
			return e;
		throw new EmployeeNotFoundException();
	}

	@Override
	public List<Employee> search(String name) throws DirectoryException {
		return storage.fetch(name);
	}
}
