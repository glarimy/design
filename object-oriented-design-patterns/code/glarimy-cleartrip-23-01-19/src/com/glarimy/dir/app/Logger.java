package com.glarimy.dir.app;

import java.util.Date;

public class Logger implements EmployeeController {
	private EmployeeController target;

	public Logger(EmployeeController target) {
		this.target = target;
	}

	@Override
	public Response find(int id) {
		System.out.println("Logger: " + new Date() + " find - start");
		Response response = target.find(id);
		System.out.println("Logger: " + new Date() + " find - ends");
		return response;
	}

	@Override
	public Response register(NewEmployee employee) {
		System.out.println("Logger: " + new Date() + " register - start");
		Response response = target.register(employee);
		System.out.println("Logger: " + new Date() + " register - ends");
		return response;
	}

	@Override
	public Response search(String name) {
		System.out.println("Logger: " + new Date() + " search - start");
		Response response = target.search(name);
		System.out.println("Logger: " + new Date() + " search - ends");
		return response;
	}
}
