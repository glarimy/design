package com.glarimy.dir.app;

import com.glarimy.dir.domain.DirectoryException;

public interface Factory {
	public Object get(String key) throws DirectoryException;
}
