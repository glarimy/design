package com.glarimy.dir.app;

import java.util.Properties;

import com.glarimy.dir.domain.DirectoryException;
import com.glarimy.dir.domain.EmployeeRepo;
import com.glarimy.dir.infra.InMemoryEmployeeRepo;

public class ObjectFactory implements Factory {
	private Properties config;

	public ObjectFactory(Properties config) {
		this.config = config;
	}

	@Override
	public Object get(String key) throws DirectoryException {
		if (key.equalsIgnoreCase("controller")) {
			EmployeeRepo repo = (EmployeeRepo) get("repo");
			EmployeeController o = new SimpleEmployeeController(repo);
			if (config.getProperty("logging").equals("enabled"))
				o = new Logger(o);
			if (config.getProperty("email").equals("enabled"))
				o = new Emailer(o);
			return o;
		}
		if (key.equalsIgnoreCase("repo")) {
			return InMemoryEmployeeRepo.getInstance();
		}
		throw new DirectoryException();
	}
}
