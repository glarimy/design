package com.glarimy.dir.app;

import java.util.List;
import java.util.Optional;

import com.glarimy.dir.domain.DirectoryException;
import com.glarimy.dir.domain.Email;
import com.glarimy.dir.domain.Employee;
import com.glarimy.dir.domain.EmployeeRepo;
import com.glarimy.dir.domain.Name;
import com.glarimy.dir.domain.PhoneNumber;

public class SimpleEmployeeController implements EmployeeController {
	private EmployeeRepo repo;

	public SimpleEmployeeController(EmployeeRepo repo) {
		this.repo = repo;
	}

	@Override
	public Response register(NewEmployee employee) {
		Response response = new Response();
		try {
			Name name = new Name(employee.firstName, employee.lastName);
			PhoneNumber phone = new PhoneNumber(employee.phone);
			Email email = new Email(employee.email);
			com.glarimy.dir.domain.Address present = new com.glarimy.dir.domain.Address(employee.present.location,
					employee.present.city, employee.present.pincode);
			com.glarimy.dir.domain.Address permanent = new com.glarimy.dir.domain.Address(employee.permanent.location,
					employee.permanent.city, employee.permanent.pincode);
			Employee e = new Employee(name, phone, email, present, permanent);
			Employee result = repo.save(e);
			response.success = true;
			EmployeeData data = new EmployeeData();
			data.id = result.getId();
			data.name = result.getName().getFirstName() + " " + result.getName().getLastName();
			data.phone = result.getPhone().getValue();
			data.email = result.getEmail().getValue();
			data.presentAddress = result.getPresent().getLocation() + ", " + result.getPresent().getCity() + " - "
					+ result.getPresent().getPincode();
			data.permanentAddress = result.getPermanent().getLocation() + ", " + result.getPermanent().getCity() + " - "
					+ result.getPermanent().getPincode();
			response.body = data;
		} catch (DirectoryException e) {
			response.success = false;
			Error error = new Error();
			error.code = 400;
			error.message = e.getMessage();
			response.body = error;
		}
		return response;
	}

	@Override
	public Response find(int id) {
		Response response = new Response();

		Optional<Employee> result = repo.fetch(id);
		if (result.isEmpty()) {
			response.success = false;
			Error error = new Error();
			error.code = 404;
			error.message = "Employee Not Found";
			response.body = error;
		} else {
			response.success = true;
			response.body = result.get();
		}
		return response;

	}

	@Override
	public Response search(String name) {
		Response response = new Response();
		List<Employee> result = repo.search(name);
		response.success = true;
		response.body = result;
		return response;
	}

}
