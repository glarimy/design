package com.glarimy.dir.app;

public class EmployeeData {
	public int id;
	public String name;
	public String email;
	public long phone;
	public String presentAddress;
	public String permanentAddress;

	@Override
	public String toString() {
		return "EmployeeData [id=" + id + ", name=" + name + ", email=" + email + ", phone=" + phone
				+ ", presentAddress=" + presentAddress + ", permanentAddress=" + permanentAddress + "]";
	}

}
