package com.glarimy.dir.app;

public interface EmployeeController {
	public Response register(NewEmployee employee);
	public Response find(int id);
	public Response search(String name);
}
