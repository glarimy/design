package com.glarimy.dir.app;

public class Emailer implements EmployeeController {
	private EmployeeController target;

	public Emailer(EmployeeController target) {
		this.target = target;
	}

	@Override
	public Response find(int id) {
		Response response = target.find(id);
		return response;
	}

	@Override
	public Response register(NewEmployee employee) {
		Response response = target.register(employee);
		if (response.success)
			System.out.println("Emailer: Email has been sent to " + employee.email + " with registration details");
		else
			System.out.println("Emailer: Email has been sent to " + employee.email + " with error details");
		return response;
	}

	@Override
	public Response search(String name) {
		Response response = target.search(name);
		return response;
	}
}
