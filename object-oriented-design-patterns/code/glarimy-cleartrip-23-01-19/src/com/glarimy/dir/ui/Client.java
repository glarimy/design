package com.glarimy.dir.ui;

import java.util.Properties;

import com.glarimy.dir.app.Address;
import com.glarimy.dir.app.EmployeeController;
import com.glarimy.dir.app.EmployeeData;
import com.glarimy.dir.app.Factory;
import com.glarimy.dir.app.NewEmployee;
import com.glarimy.dir.app.ObjectFactory;
import com.glarimy.dir.app.Response;
import com.glarimy.dir.app.Error;

public class Client {
	public static void main(String[] args) {
		try {
			Properties config = new Properties();
			config.setProperty("logging", "disabled");
			config.setProperty("email", "enabled");

			Factory factory = new ObjectFactory(config);
			EmployeeController app = (EmployeeController) factory.get("controller");
			
			Address present = new Address();
			present.location = "Pai Layout";
			present.city = "Bengaluru";
			present.pincode = 560016;
			Address permanent = new Address();
			permanent.location = "Pai Layout";
			permanent.city = "Bengaluru";
			permanent.pincode = 560016;
			NewEmployee e = new NewEmployee();
			e.firstName = "Krishna Mohan";
			e.lastName = "Koyya";
			e.email = "krishna@glarimy.com";
			e.phone = 9731423166L;
			e.present = present;
			e.permanent = permanent;
			Response response = app.register(e);
			if (response.success) {
				EmployeeData data = (EmployeeData) response.body;
				System.out.println("Employee is added successfully with ID: " + data.id);
				System.out.println(data);
			} else {
				Error error = (Error) response.body;
				System.out.println("Operation failed with code: " + error.code);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
