package com.glarimy.dir.domain;

public class Address {
	private String location;
	private String city;
	private int pincode;

	public Address(String location, String city, int pincode) throws DirectoryException {
		if (location == null || city == null || pincode < 1)
			throw new DirectoryException();
		this.location = location;
		this.city = city;
		this.pincode = pincode;
	}

	public String getLocation() {
		return location;
	}

	public String getCity() {
		return city;
	}

	public int getPincode() {
		return pincode;
	}
}
