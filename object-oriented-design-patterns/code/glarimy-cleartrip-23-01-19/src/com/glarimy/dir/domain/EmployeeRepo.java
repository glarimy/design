package com.glarimy.dir.domain;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepo {
	public Employee save(Employee employee);
	public Optional<Employee> fetch(int id);
	public List<Employee> search(String name);
}
