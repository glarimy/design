package com.glarimy.dir.domain;

public class Email {
	private String value;

	public Email(String value) throws DirectoryException {
		if (value == null)
			throw new DirectoryException();
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
