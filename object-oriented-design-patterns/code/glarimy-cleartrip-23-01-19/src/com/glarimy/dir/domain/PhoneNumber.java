package com.glarimy.dir.domain;

public class PhoneNumber {
	private long value;

	public PhoneNumber(long value) throws DirectoryException {
		if (value < 1)
			throw new DirectoryException();
		this.value = value;
	}

	public long getValue() {
		return value;
	}

}
