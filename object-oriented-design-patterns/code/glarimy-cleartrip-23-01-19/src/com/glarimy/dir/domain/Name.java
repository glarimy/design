package com.glarimy.dir.domain;

public class Name {
	private String firstName;
	private String lastName;

	public Name(String firstName, String lastName) throws DirectoryException {
		if (firstName == null || lastName == null)
			throw new DirectoryException();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

}
