package com.glarimy.dir.domain;

public class Employee {
	private int id;
	private Name name;
	private PhoneNumber phone;
	private Email email;
	private Address present;
	private Address permanent;

	public Employee(Name name, PhoneNumber phone, Email email, Address present, Address permanent) {
		super();
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.present = present;
		this.permanent = permanent;
	}

	public Employee(int id, Name name, PhoneNumber phone, Email email, Address present, Address permanent) {
		super();
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.present = present;
		this.permanent = permanent;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public PhoneNumber getPhone() {
		return phone;
	}

	public void setPhone(PhoneNumber phone) {
		this.phone = phone;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public Address getPresent() {
		return present;
	}

	public void setPresent(Address present) {
		this.present = present;
	}

	public Address getPermanent() {
		return permanent;
	}

	public void setPermanent(Address permanent) {
		this.permanent = permanent;
	}

}
