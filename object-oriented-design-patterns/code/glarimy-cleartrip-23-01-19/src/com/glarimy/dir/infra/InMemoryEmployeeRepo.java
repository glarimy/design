package com.glarimy.dir.infra;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.glarimy.dir.domain.Employee;
import com.glarimy.dir.domain.EmployeeRepo;

public class InMemoryEmployeeRepo implements EmployeeRepo {
	private Map<Integer, Employee> entries;
	private int nextId;
	private static InMemoryEmployeeRepo INSTANCE;

	private InMemoryEmployeeRepo() {
		entries = new HashMap<Integer, Employee>();
		nextId = 1;
	}

	public static synchronized InMemoryEmployeeRepo getInstance() {
		if (INSTANCE == null)
			INSTANCE = new InMemoryEmployeeRepo();
		return INSTANCE;
	}

	@Override
	public Employee save(Employee employee) {
		employee.setId(nextId);
		entries.put(nextId, employee);
		nextId++;
		return employee;
	}

	@Override
	public Optional<Employee> fetch(int id) {
		if (entries.containsKey(id))
			return Optional.of(entries.get(id));
		return Optional.empty();
	}

	@Override
	public List<Employee> search(String name) {
		return entries.values().stream()
				.filter(e -> e.getName().getFirstName().contains(name) || e.getName().getLastName().contains(name))
				.collect(Collectors.toList());
	}

}
