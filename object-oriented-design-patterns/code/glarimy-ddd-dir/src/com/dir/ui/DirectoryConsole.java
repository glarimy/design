package com.dir.ui;

import com.dir.app.EmployeeController;
import com.dir.app.NewEmployee;
import com.dir.app.Response;
import com.dir.app.SimpleEmployeeController;
import com.dir.domain.EmployeeRepository;
import com.dir.infra.InMemoryEmployeeRepository;

public class DirectoryConsole {
	public static void main(String[] args) {
		EmployeeRepository store = new InMemoryEmployeeRepository();
		EmployeeController dir = new SimpleEmployeeController(store);
		NewEmployee e = new NewEmployee();
		e.name = "Krishna Mohan Koyya";
		e.phone = 9731423166L;
		e.email = "krishna@glarimy.com";
		Response response = dir.add(e);
		System.out.println(response);
		response = dir.findById(1);
		System.out.println(response);
	}
}
