package com.dir.domain;

import java.util.List;

public interface EmployeeRepository {
	public Employee save(Employee employee) throws DirectoryException;

	public Employee fetch(int eid) throws NotFoundException, DirectoryException;

	public List<Employee> search(String name) throws DirectoryException;
}
