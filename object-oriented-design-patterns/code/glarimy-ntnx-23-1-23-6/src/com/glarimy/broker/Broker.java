package com.glarimy.broker;

public interface Broker {
	public void register(Handler handler);

	public void notify(Event e);
}
