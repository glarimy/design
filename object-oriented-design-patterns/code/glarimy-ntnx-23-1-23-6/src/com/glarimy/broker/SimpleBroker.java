package com.glarimy.broker;

import java.util.ArrayList;
import java.util.List;

public class SimpleBroker implements Broker {
	private List<Handler> handlers;

	public SimpleBroker() {
		handlers = new ArrayList<Handler>();
	}

	@Override
	public void register(Handler handler) {
		handlers.add(handler);
	}

	@Override
	public void notify(Event e) {
		for (Handler h : handlers)
			h.handle(e);
	}

}
