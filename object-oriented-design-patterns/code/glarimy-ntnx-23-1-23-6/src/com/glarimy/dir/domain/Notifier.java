package com.glarimy.dir.domain;

import com.glarimy.broker.Event;
import com.glarimy.broker.Handler;

public class Notifier implements Handler {

	@Override
	public void handle(Event event) {
		System.out.println("SMS: " + event);
	}
}
