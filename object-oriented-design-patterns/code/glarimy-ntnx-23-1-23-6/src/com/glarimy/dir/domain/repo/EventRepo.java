package com.glarimy.dir.domain.repo;

import com.glarimy.dir.domain.DirectoryException;

public interface EventRepo {
	public void create(String type, Object body) throws DirectoryException;
}
