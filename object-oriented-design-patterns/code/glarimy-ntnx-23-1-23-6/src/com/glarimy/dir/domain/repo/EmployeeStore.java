package com.glarimy.dir.domain.repo;

import java.util.List;

import com.glarimy.dir.domain.DirectoryException;
import com.glarimy.dir.domain.entity.Employee;

public interface EmployeeStore {
	public Employee save(Employee e) throws DirectoryException;
	public Employee fetch(int eid) throws DirectoryException;
	public List<Employee> fetch(String name) throws DirectoryException;
}
