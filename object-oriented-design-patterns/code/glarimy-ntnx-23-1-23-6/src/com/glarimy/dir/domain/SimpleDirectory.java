package com.glarimy.dir.domain;

import java.util.List;

import com.glarimy.dir.domain.entity.Employee;
import com.glarimy.dir.domain.repo.EmployeeStore;
import com.glarimy.dir.domain.repo.EventRepo;

public class SimpleDirectory implements Directory {
	private EmployeeStore empRepo;
	private EventRepo evtRepo;

	public SimpleDirectory(EmployeeStore storage, EventRepo evtRepo) {
		this.empRepo = storage;
		this.evtRepo = evtRepo;
	}

	@Override
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException {
		Employee employee = empRepo.save(e);
		evtRepo.create("directory.register", employee);
		return employee;
	}

	@Override
	public Employee find(int eid) throws EmployeeNotFoundException, DirectoryException {
		Employee e = empRepo.fetch(eid);
		if (e != null) {
			evtRepo.create("directory.find", e);
			return e;
		}
		evtRepo.create("directory.error", "employee not found");
		throw new EmployeeNotFoundException();
	}

	@Override
	public List<Employee> search(String name) throws DirectoryException {
		List<Employee> list = empRepo.fetch(name);
		evtRepo.create("directory.search", list);
		return list;

	}
}
