package com.glarimy.dir.domain;

import java.util.List;

import com.glarimy.dir.domain.entity.Employee;

public interface Directory {
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException;

	public Employee find(int eid) throws EmployeeNotFoundException, DirectoryException;

	public List<Employee> search(String name) throws DirectoryException;
}
