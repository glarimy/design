package com.glarimy.dir.domain;

import com.glarimy.broker.Event;
import com.glarimy.broker.Handler;

public class Logger implements Handler {

	@Override
	public void handle(Event event) {
		System.out.println("Logger: " + event);
	}
}
