package com.glarimy.dir.domain.vo;

import java.util.Objects;

import com.glarimy.dir.domain.ValidationException;

public class Phone {
	private long value;

	public Phone(long number) throws ValidationException {
		if (number < 1)
			throw new ValidationException("Invalid phone number");
		this.value = number;
	}

	public long getNumber() {
		return value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Phone other = (Phone) obj;
		return value == other.value;
	}

	@Override
	public String toString() {
		return "PhoneNumber [value=" + value;
	}

}
