package com.glarimy.dir.domain.entity;

import com.glarimy.dir.domain.vo.Email;
import com.glarimy.dir.domain.vo.Name;
import com.glarimy.dir.domain.vo.Phone;

public class Employee {
	private int eid;
	private Name name;
	private Phone phone;
	private Email email;

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Employee [eid=" + eid + ", name=" + name + ", phone=" + phone + ", email=" + email + "]";
	}

}
