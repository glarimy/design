package com.glarimy.dir.domain.vo;

import java.util.Objects;
import java.util.regex.Pattern;

import com.glarimy.dir.domain.ValidationException;

public class Email {
	private String value;
	private final String regex = "^[A-Za-z0-9+_.-]+@(.+)$";

	public Email(String value) throws ValidationException {
		if (Pattern.matches(regex, value))
			this.value = value;
		else
			throw new ValidationException("Invalid email");
	}

	public String getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Email other = (Email) obj;
		return Objects.equals(value, other.value);
	}

	@Override
	public String toString() {
		return "Email [value=" + value + ", regex=" + regex + "]";
	}

}
