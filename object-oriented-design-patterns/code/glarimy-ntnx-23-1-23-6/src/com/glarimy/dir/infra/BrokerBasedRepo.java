package com.glarimy.dir.infra;

import com.glarimy.broker.Broker;
import com.glarimy.broker.Event;
import com.glarimy.dir.domain.DirectoryException;
import com.glarimy.dir.domain.repo.EventRepo;

public class BrokerBasedRepo implements EventRepo {
	private Broker broker;

	public BrokerBasedRepo(Broker broker) {
		this.broker = broker;
	}

	@Override
	public void create(String type, Object body) throws DirectoryException {
		Event e = new Event.EventBuilder(type).setBody(body).build();
		broker.notify(e);
	}
}
