package com.glarimy.dir.ui;

import java.util.List;
import java.util.Properties;

import com.glarimy.dir.app.DirectoryFactory;
import com.glarimy.dir.app.EmployeeController;
import com.glarimy.dir.app.Factory;
import com.glarimy.dir.app.dto.EmployeeRecord;
import com.glarimy.dir.app.dto.Error;
import com.glarimy.dir.app.dto.NewEmployee;
import com.glarimy.dir.app.dto.Response;

public class Client {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		Properties config = new Properties();
		config.setProperty("logging", "enabled");
		config.setProperty("sms", "enabled");
		config.setProperty("storage", "cache");
		config.setProperty("broker", "local");

		Factory factory = new DirectoryFactory(config);
		EmployeeController dir = factory.getController();
		NewEmployee e = new NewEmployee();
		e.firstName = "Krishna Mohan";
		e.lastName = "Koyya";
		e.phone = 9731423166L;
		e.email = "krishna@glarimy.com";
		Response response = dir.add(e);
		if (!response.success) {
			System.out.println(((Error) response.body).message);
		} else {
			response = dir.find("Krishna");
			if (response.success) {
				List<EmployeeRecord> list = (List<EmployeeRecord>) response.body;
				for (EmployeeRecord emp : list)
					System.out.println(emp);
			}
		}
	}
}
