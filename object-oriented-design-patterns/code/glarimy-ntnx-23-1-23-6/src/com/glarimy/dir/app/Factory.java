package com.glarimy.dir.app;

import com.glarimy.dir.domain.DirectoryException;

public interface Factory {
	public EmployeeController getController() throws DirectoryException;
}
