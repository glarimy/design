package com.glarimy.dir.app.dto;

public class NewEmployee {
	public String firstName;
	public String lastName;
	public String email;
	public long phone;

	@Override
	public String toString() {
		return "NewEmployee [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", phone="
				+ phone + "]";
	}

}
