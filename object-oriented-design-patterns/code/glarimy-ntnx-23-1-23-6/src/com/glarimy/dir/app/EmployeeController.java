package com.glarimy.dir.app;

import com.glarimy.dir.app.dto.NewEmployee;
import com.glarimy.dir.app.dto.Response;

public interface EmployeeController {
	public Response add(NewEmployee e);
	public Response find(int eid);
	public Response find(String name);
}
