package com.glarimy.dir.app.dto;

public class Response {
	public boolean success;
	public Object body;

	public Response(boolean success, Object body) {
		this.success = success;
		this.body = body;
	}

	@Override
	public String toString() {
		return "Response [success=" + success + ", body=" + body + "]";
	}

}
