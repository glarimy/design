package com.glarimy.dir.app;

import java.util.Properties;

import com.glarimy.broker.Broker;
import com.glarimy.broker.SimpleBroker;
import com.glarimy.dir.domain.Directory;
import com.glarimy.dir.domain.DirectoryException;
import com.glarimy.dir.domain.Logger;
import com.glarimy.dir.domain.Notifier;
import com.glarimy.dir.domain.SimpleDirectory;
import com.glarimy.dir.domain.repo.EmployeeStore;
import com.glarimy.dir.domain.repo.EventRepo;
import com.glarimy.dir.infra.BrokerBasedRepo;
import com.glarimy.dir.infra.InMemoryStorage;

public class DirectoryFactory implements Factory {
	private EmployeeController instance;

	public DirectoryFactory(Properties config) throws DirectoryException {
		EmployeeStore empRepo;
		if (config.getProperty("storage").equals("cache"))
			empRepo = new InMemoryStorage();
		else
			throw new DirectoryException();

		Broker broker;
		if (config.getProperty("broker").equals("local"))
			broker = new SimpleBroker();
		else
			throw new DirectoryException();

		EventRepo evtRepo = new BrokerBasedRepo(broker);

		Directory service = new SimpleDirectory(empRepo, evtRepo);
		if (config.getProperty("logging").equals("enabled"))
			broker.register(new Logger());
		if (config.getProperty("sms").equals("enabled"))
			broker.register(new Notifier());

		instance = new SimpleEmployeeController(service);

	}

	@Override
	public EmployeeController getController() throws DirectoryException {
		return instance;
	}

}
