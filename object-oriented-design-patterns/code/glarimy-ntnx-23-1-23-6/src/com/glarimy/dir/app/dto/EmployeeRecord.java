package com.glarimy.dir.app.dto;

public class EmployeeRecord {
	public int eid;
	public String name;
	public String email;
	public long phone;

	@Override
	public String toString() {
		return "EmployeeRecord [eid=" + eid + ", name=" + name + ", email=" + email + ", phone=" + phone + "]";
	}

}
