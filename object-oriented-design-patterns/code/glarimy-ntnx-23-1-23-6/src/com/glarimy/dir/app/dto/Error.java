package com.glarimy.dir.app.dto;

public class Error {
	public int code;
	public String message;

	public Error(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	@Override
	public String toString() {
		return "Error [code=" + code + ", message=" + message + "]";
	}

}
