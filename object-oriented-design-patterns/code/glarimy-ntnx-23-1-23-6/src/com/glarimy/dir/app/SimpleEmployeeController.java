package com.glarimy.dir.app;

import java.util.List;
import java.util.stream.Collectors;

import com.glarimy.dir.app.dto.EmployeeRecord;
import com.glarimy.dir.app.dto.Error;
import com.glarimy.dir.app.dto.NewEmployee;
import com.glarimy.dir.app.dto.Response;
import com.glarimy.dir.domain.Directory;
import com.glarimy.dir.domain.DirectoryException;
import com.glarimy.dir.domain.EmployeeNotFoundException;
import com.glarimy.dir.domain.ValidationException;
import com.glarimy.dir.domain.entity.Employee;
import com.glarimy.dir.domain.vo.Email;
import com.glarimy.dir.domain.vo.Name;
import com.glarimy.dir.domain.vo.Phone;

public class SimpleEmployeeController implements EmployeeController {
	private Directory service;

	public SimpleEmployeeController(Directory service) {
		this.service = service;
	}

	@Override
	public Response add(NewEmployee e) {
		try {
			Name name = new Name(e.firstName, e.lastName);
			Phone phone = new Phone(e.phone);
			Email email = new Email(e.email);
			Employee employee = new Employee();
			employee.setName(name);
			employee.setEmail(email);
			employee.setPhone(phone);
			employee = service.add(employee);
			EmployeeRecord record = new EmployeeRecord();
			record.eid = employee.getEid();
			record.name = employee.getName().getFullName();
			record.phone = employee.getPhone().getNumber();
			record.email = employee.getEmail().getValue();
			return new Response(true, record);
		} catch (ValidationException ve) {
			Error error = new Error(400, ve.getMessage());
			return new Response(false, error);
		} catch (DirectoryException de) {
			Error error = new Error(500, de.getMessage());
			return new Response(false, error);
		}
	}

	@Override
	public Response find(int eid) {
		try {
			Employee employee = service.find(eid);
			EmployeeRecord record = new EmployeeRecord();
			record.eid = employee.getEid();
			record.name = employee.getName().getFullName();
			record.phone = employee.getPhone().getNumber();
			record.email = employee.getEmail().getValue();
			return new Response(true, record);
		} catch (EmployeeNotFoundException ve) {
			Error error = new Error(404, ve.getMessage());
			return new Response(false, error);
		} catch (DirectoryException de) {
			Error error = new Error(500, de.getMessage());
			return new Response(false, error);
		}
	}

	@Override
	public Response find(String name) {
		try {
			List<Employee> employees = service.search(name);
			List<EmployeeRecord> records = employees.stream().map(employee -> {
				EmployeeRecord record = new EmployeeRecord();
				record.eid = employee.getEid();
				record.name = employee.getName().getFullName();
				record.phone = employee.getPhone().getNumber();
				record.email = employee.getEmail().getValue();
				return record;
			}).collect(Collectors.toList());
			return new Response(true, records);
		} catch (DirectoryException de) {
			Error error = new Error(500, de.getMessage());
			return new Response(false, error);
		}

	}
}
