from dataclasses import dataclass
from abc import ABC, abstractmethod

# Broker Module
class Handler(ABC):
    @abstractmethod
    def on(self, msg): pass

class Broker(ABC):
    @abstractmethod
    def register(self, handler): pass

    @abstractmethod
    def notify(self, msg): pass

class SimpleBroker(Broker):
    def __init__(self):
        self.handlers = []

    def register(self, handler):
        self.handlers.append(handler)

    def notify(self, msg):
        for handler in self.handlers:
            handler.on(msg)

# Directory Module: Domain Layer
@dataclass(frozen=True)
class Email:
    value: str
    def __post_init__(self):
        if self.value == None:
            raise ValueError()

@dataclass(frozen=True)
class Phone:
    isd:int
    number: int
    def __post_init__(self):
        if self.isd < 1 and self.number < 1:
            raise ValueError()
    def get_number(self):
        return str(self.isd) + "-" + str(self.number)

@dataclass(frozen=True)
class Name:
    first_name: str
    last_name: str
    def __post_init__(self):
        if len(self.first_name) < 3 or len(self.last_name) < 3:
            raise ValueError()
    def get_fullname(self):
        return self.first_name + " " + self.last_name

@dataclass
class Employee:
    eid: int
    name: Name 
    phone: Phone
    email: Email

class Storage(ABC):
    @abstractmethod
    def create(self, e) -> Employee: pass
    
    @abstractmethod
    def read(self, id) -> Employee: pass

    @abstractmethod
    def read(self, name) -> []: pass

# Directory Module: Service Layer
class Directory(ABC):
    @abstractmethod
    def add(self, e) -> Employee: pass
    
    @abstractmethod
    def find(self, id) -> Employee: pass

    @abstractmethod
    def search(self, name) -> []: pass

class SimpleDirectory(Directory):
    def __init__(self, storage):
        self.repo: Storage = storage

    def add(self, e) -> Employee:
        e.eid = 1
        return self.repo.create(e)
    
    def find(self, id) -> Employee:
        return self.repo.read(id)

    def search(self, name) -> []:
        return self.repo.read(name)

class Auditor(Directory):
    def __init__(self, directory:Directory):
        self.target = directory

    def add(self, e) -> Employee:
        print("adding ", e)
        e = self.target.add(e)
        print("added ", e)
        return e
    
    def find(self, id) -> Employee:
        print("finding ", id)
        e = self.target.find(id)
        print("found ", e)
        return e

    def search(self, name) -> []:
        print("searching ", name)
        results = self.target.search(name)
        print("found ", results)
        return results

class Notifier(Directory):
    def __init__(self, directory:Directory, broker:Broker):
        self.target = directory
        self.broker = broker

    def add(self, e) -> Employee:
        e = self.target.add(e)
        self.broker.notify("Added " + e.name.get_fullname())
        return e
    
    def find(self, id) -> Employee:
        return self.target.find(id)

    def search(self, name) -> []:
        return self.target.search(name)

class PrintableDirectory(Directory):
    def __init__(self, directory:Directory):
        self.target = directory

    def add(self, e) -> Employee:
        return self.target.add(e)
    
    def find(self, id) -> Employee:
        return self.target.find(id)

    def search(self, name) -> []:
        return self.target.search(name)
    
    def print(self, id):
        e = self.target.find(id)
        print(e)

# Directory Module: App Layer
@dataclass
class NewEmployee:
    fname: str
    lname: str
    prefix: int
    number: int
    email: str

@dataclass
class EmployeeDetail:
    eid: int
    name: str
    phone: str
    email: str

class DirectoryController: 
    def __init__(self, directory: Directory):
        self.service: Directory = directory
    
    def register(self, dto):
        e = Employee(0, Name(dto.fname, dto.lname), Phone(dto.prefix, dto.number), Email(dto.email))
        e = self.service.add(e)
        return EmployeeDetail(e.eid, e.name.get_fullname(), e.phone.get_number(), e.email.value)

# Directory Module: Infra Layer
class InMemoryStorage(Storage):
    def __init__(self):
        self.employees = {}

    def create(self, e) -> Employee:
        self.employees[e.eid] = e
        return e

    def read(self, id) -> Employee:
        return self.employees[id]

    def read(self, name) -> []: pass

class EmailHandler(Handler):
    def on(self, msg):
        print("Mailed: ", msg)

class SMSHandler(Handler):
    def on(self, msg):
        print("Messaged: ", msg)

class Factory:
    def get(self, with_audit, with_notifications)->DirectoryController:
        broker = SimpleBroker()
        broker.register(EmailHandler())
        broker.register(SMSHandler())

        storage = InMemoryStorage()
        directory = SimpleDirectory(storage)
        if(with_audit == True):
            directory = Auditor(directory)        
        if(with_notifications == True):
            directory = Notifier(directory, broker)
        self.controller = DirectoryController(directory)
        return self.controller

# Client
with_audit = True
with_notifications = True
app = Factory().get(with_audit, with_notifications)
e: EmployeeDetail = app.register(NewEmployee("Krishna Mohan", "Koyya", 91, 9731423166, "krishna@gmailcom"))
print(e)