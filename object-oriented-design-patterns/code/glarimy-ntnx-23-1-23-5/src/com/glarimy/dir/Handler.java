package com.glarimy.dir;

public interface Handler {
	public void handle(Event event);
}
