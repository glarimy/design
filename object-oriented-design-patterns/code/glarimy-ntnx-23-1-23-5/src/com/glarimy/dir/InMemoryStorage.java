package com.glarimy.dir;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemoryStorage implements Storage {
	private Map<Integer, Employee> entries;
	private int nextId;

	public InMemoryStorage() {
		entries = new HashMap<Integer, Employee>();
		nextId = 1;
	}

	@Override
	public Employee fetch(int eid) throws DirectoryException {
		return entries.get(eid);
	}

	@Override
	public List<Employee> fetch(String name) throws DirectoryException {
		return entries.values().stream().filter(e -> e.getName().contains(name)).collect(Collectors.toList());
	}

	@Override
	public Employee save(Employee e) throws DirectoryException {
		e.setEid(nextId);
		entries.put(nextId, e);
		nextId++;
		return e;
	}
}
