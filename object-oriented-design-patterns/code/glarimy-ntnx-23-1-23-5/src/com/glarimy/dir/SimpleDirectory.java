package com.glarimy.dir;

import java.util.List;

public class SimpleDirectory implements Directory {
	private Storage storage;
	private Broker broker;

	public SimpleDirectory(Storage storage, Broker broker) {
		this.storage = storage;
		this.broker = broker;
	}

	@Override
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException {
		if (e == null || e.getName() == null || e.getEmail() == null || e.getPhone() < 1)
			throw new InvalidEmployeeException();
		Event event = new Event.EventBuilder("directory.register").setBody(e).build();
		broker.notify(event);
		return storage.save(e);
	}

	@Override
	public Employee find(int eid) throws EmployeeNotFoundException, DirectoryException {
		Employee e = storage.fetch(eid);
		if (e != null) {
			Event event = new Event.EventBuilder("directory.find").setBody(e).build();
			broker.notify(event);
			return e;
		}
		Event event = new Event.EventBuilder("directory.error").build();
		broker.notify(event);
		throw new EmployeeNotFoundException();
	}

	@Override
	public List<Employee> search(String name) throws DirectoryException {
		List<Employee> list = storage.fetch(name);
		Event event = new Event.EventBuilder("directory.search").setBody(list).build();
		broker.notify(event);
		return list;

	}
}
