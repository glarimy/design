package com.glarimy.dir;

public class Logger implements Handler {

	@Override
	public void handle(Event event) {
		System.out.println("Logger: " + event);
	}
}
