package com.glarimy.dir;

import java.util.Properties;

public class DirectoryFactory implements Factory {
	private Directory instance;

	public DirectoryFactory(Properties config) throws DirectoryException {
		Storage storage;
		if (config.getProperty("storage").equals("cache"))
			storage = new InMemoryStorage();
		else
			throw new DirectoryException();

		Broker broker;
		if (config.getProperty("broker").equals("local"))
			broker = new SimpleBroker();
		else
			throw new DirectoryException();

		instance = new SimpleDirectory(storage, broker);
		if (config.getProperty("logging").equals("enabled"))
			broker.register(new Logger());
		if (config.getProperty("sms").equals("enabled"))
			broker.register(new Notifier());

	}

	@Override
	public Directory getDirectory() throws DirectoryException {
		return instance;
	}

}
