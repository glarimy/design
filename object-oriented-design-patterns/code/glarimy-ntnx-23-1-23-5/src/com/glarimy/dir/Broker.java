package com.glarimy.dir;

public interface Broker {
	public void register(Handler handler);

	public void notify(Event e);
}
