from dataclasses import dataclass
from abc import ABC, abstractmethod

@dataclass
class Event:
    message:str
    topic:str

@dataclass
class Money:
    amount: int
    currency: str
    def __post_init__(self):
        if self.amount < 0 or self.currency == None:
            raise ValueError()

@dataclass
class Quantity:
    amount: int
    unit: str
    def __post_init__(self):
        if self.amount < 0 or self.unit == None:
            raise ValueError()

class Product:
    def __init__(self, name:str, price:Money):
        self.name = name
        self.price = price 
        self.available = 0
        self.reserved = 0
    
    def replenish(self, q: Quantity):
        self.available = self.available + q.amount

    def reserve(self, q: Quantity):
        if self.available < q.amount:
            raise ValueError()
        self.available = self.available - q.amount
        self.reserved = self.reserved + q.amount

    def release(self, q: Quantity):
        if self.reserved < q.amount:
            raise ValueError()
        self.available = self.available + q.amount
        self.reserved = self.reserved - q.amount

    def consume(self, q: Quantity):
        if self.reserved < q.amount:
            raise ValueError()
        self.reserved = self.reserved - q.amount
        print(self.reserved)

class ProductRepo(ABC):
    @abstractmethod
    def save(self, p:Product) -> Product:
        pass

    @abstractmethod
    def fetch(self, id: int) -> Product:
        pass

    @abstractmethod
    def update(self, p:Product) -> Product:
        pass

class ProductController(ABC):
    @abstractmethod
    def add(self, name: str, price: int, currency: str) -> int:
        pass

    @abstractmethod
    def replenish(self, id: int, quantity:int, units: str):
        pass

    @abstractmethod
    def reserve(self, id: int, quantity:int, units: str):
        pass

    @abstractmethod
    def release(self, id: int, quantity:int, units: str):
        pass

    @abstractmethod
    def consume(self, id: int, quantity:int, units: str):
        pass

class Handler(ABC):
    @abstractmethod
    def on(self, e:Event):
        pass

class Broker(ABC):
    @abstractmethod
    def register(self, h:Handler):
        pass
    
    @abstractmethod
    def notify(self, e:Event):
        pass

class SimpleBroker(Broker):
    def __init__(self):
        self.handlers = []

    def register(self, h:Handler):
        self.handlers.append(h)

    def notify(self, e:Event):
        for h in self.handlers:
            h.on(e)

class SMSHandler(Handler):
    def on(self, e:Event):
        print("sms", e.message)

class EmailHandler(Handler):
    def on(self, e:Event):
        print("email", e.message)

class InMemoryProductRepo(ProductRepo):
    def __init__(self):
        self.products = {}
        self.count = 0

    def save(self, p:Product) -> Product:
        self.count = self.count+1
        p.id = self.count
        self.products[self.count] = p
        return p

    def fetch(self, id: int) -> Product:
        print(self.products[id].available, self.products[id].reserved)
        return self.products[id]

    def update(self, p:Product) -> Product:
        self.products[p.id]

class SimpleProductController(ProductController):
    def __init__(self, repo: ProductRepo): 
        self.repo = repo

    def add(self, name: str, price: int, currency: str) -> int:
        try:
            value: Money = Money(price, currency)
            product: Product = Product(name, price)
            product = self.repo.save(product)
            return product.id
        except ValueError: 
            return "Operation Failed"

    def replenish(self, id: int, quantity:int, units: str):
        try:
            product:Product = self.repo.fetch(id)
            product.replenish(Quantity(quantity, units ))
            self.repo.update(product)
        except ValueError: 
            return "Operation Failed"

    def reserve(self, id: int, quantity:int, units: str):
        try:
            product:Product = self.repo.fetch(id)
            product.reserve(Quantity(quantity, units ))
            self.repo.update(product)
        except ValueError: 
            return "Operation Failed"

    def release(self, id: int, quantity:int, units: str):
        try: 
            product:Product = self.repo.fetch(id)
            product.release(Quantity(quantity, units ))
            self.repo.update(product)
        except ValueError: 
            return "Operation Failed"

    def consume(self, id: int, quantity:int, units: str):
        try:
            product:Product = self.repo.fetch(id)
            product.consume(Quantity(quantity, units ))
            self.repo.update(product)
        except ValueError: 
            return "Operation Failed"

class Notifier(ProductController):
    def __init__(self, target:ProductController, broker: Broker):
        self.target = target
        self.broker = broker

    def add(self, name: str, price: int, currency: str) -> int:
        id = self.target.add(name, price, currency)
        self.broker.notify(Event("product added", "inventory.events"))
        return id

    def replenish(self, id: int, quantity:int, units: str):
        self.target.replenish(id, quantity, units)
        self.broker.notify(Event("product replenished", "inventory.events"))

    def reserve(self, id: int, quantity:int, units: str):
        self.target.reserve(id, quantity, units)
        self.broker.notify(Event("product reserved", "inventory.events"))

    def release(self, id: int, quantity:int, units: str):
        self.target.release(id, quantity, units)
        self.broker.notify(Event("product released", "inventory.events"))

    def consume(self, id: int, quantity:int, units: str):
        self.target.consume(id, quantity, units)
        self.broker.notify(Event("product consumed", "inventory.events"))

class Factory:
    def getProductController(self):
        broker = SimpleBroker()
        broker.register(SMSHandler())
        broker.register(EmailHandler())
        repo = InMemoryProductRepo()
        controller = SimpleProductController(repo)
        proxy = Notifier(controller, broker)
        return proxy

controller: ProductController = Factory().getProductController()
id: int = controller.add("Pencil", 10, "INR")
controller.replenish(id, 10, "units")
controller.replenish(id, 10, "units")
controller.reserve(id, 5, "units")
controller.consume(id, 5, "units")