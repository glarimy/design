package com.glarimy.ws;

public class Money {
	private int amount;

	public Money(int amount) throws ValidationException {
		if (amount < 0)
			throw new ValidationException("Invalid money");
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}
}
