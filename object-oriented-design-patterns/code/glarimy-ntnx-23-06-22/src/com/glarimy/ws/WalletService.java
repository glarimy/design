package com.glarimy.ws;

public interface WalletService {
	long create(Name name) throws WalletException;

	void credit(long oid, long wid, Money amount) throws WalletException;

	void debit(long oid, long wid, Money amount) throws WalletException;

}