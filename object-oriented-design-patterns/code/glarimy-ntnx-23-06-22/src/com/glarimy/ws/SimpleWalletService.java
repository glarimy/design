package com.glarimy.ws;

import java.util.Date;

public class SimpleWalletService implements WalletService {
	private WalletStore walletRepo;
	private History history;

	public SimpleWalletService(WalletStore walletRepo, History history) {
		this.walletRepo = walletRepo;
		this.history = history;
	}

	@Override
	public long create(Name name) throws WalletException {
		Wallet wallet = new Wallet(name);
		wallet.setWid(new Date().getTime());
		wallet.credit(new Money(400));
		walletRepo.create(wallet);
		return wallet.getWid();
	}

	@Override
	public void credit(long oid, long wid, Money amount) throws WalletException {
		try {
			Wallet wallet = walletRepo.read(wid);
			wallet.credit(amount);
			walletRepo.update(wallet);
			history.save(new WalletEvent.EventBuilder("wallet.credit.status").addHeader("oid", "" + oid)
					.addHeader("success", "yes").build());
		} catch (WalletException we) {
			history.save(new WalletEvent.EventBuilder("wallet.credit.status").addHeader("oid", "" + oid)
					.addHeader("success", "no").build());
		}
	}

	@Override
	public void debit(long oid, long wid, Money amount) throws WalletException {
		try {
			Wallet wallet = walletRepo.read(wid);
			wallet.debit(amount);
			walletRepo.update(wallet);
			history.save(new WalletEvent.EventBuilder("wallet.debit.status").addHeader("oid", "" + oid)
					.addHeader("success", "yes").build());
		} catch (WalletException we) {
			we.printStackTrace();
			history.save(new WalletEvent.EventBuilder("wallet.debit.status").addHeader("oid", "" + oid)
					.addHeader("success", "no").build());
		}
	}
}