package com.glarimy.ws;

import java.util.HashMap;
import java.util.Map;

public class WalletEvent {
	private String type;
	private Map<String, String> meta;
	private Object body;

	private WalletEvent() {

	}

	public String getType() {
		return type;
	}

	public Object getHeader(String key) {
		return meta.get(key);
	}

	public Object getBody() {
		return body;
	}

	@Override
	public String toString() {
		return "Event [type=" + type + ", meta=" + meta + ", body=" + body + "]";
	}

	public static class EventBuilder {
		private String type;
		private Map<String, String> headers;
		private Object payload;

		public EventBuilder(String type) {
			headers = new HashMap<String, String>();
			this.type = type;
		}

		public EventBuilder addHeader(String key, String value) {
			headers.put(key, value);
			return this;
		}

		public EventBuilder setBody(Object o) {
			this.payload = o;
			return this;
		}

		public WalletEvent build() {
			WalletEvent e = new WalletEvent();
			e.meta = headers;
			e.type = type;
			e.body = payload;
			return e;
		}
	}
}