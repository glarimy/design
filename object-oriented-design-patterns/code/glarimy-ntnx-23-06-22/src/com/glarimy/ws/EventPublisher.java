package com.glarimy.ws;

import com.glarimy.broker.Broker;
import com.glarimy.broker.Event;

public class EventPublisher implements History {

	private Broker broker;

	public EventPublisher(Broker broker) {
		this.broker = broker;
	}

	@Override
	public void save(WalletEvent e) {
		Event event = new Event.EventBuilder(e.getType()).addHeader("oid", (String) e.getHeader("oid"))
				.addHeader("success", "yes").build();
		broker.notify(event);
	}
}