package com.glarimy.ws;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class InMemoryWalletStore implements WalletStore {
	private Map<Long, Wallet> wallets;

	public InMemoryWalletStore() {
		wallets = new HashMap<Long, Wallet>();
	}

	@Override
	public Wallet create(Wallet wallet) throws WalletException {
		long wid = new Date().getTime();
		wallet.setWid(wid);
		wallets.put(wid, wallet);
		return wallet;
	}

	@Override
	public Wallet read(long wid) throws WalletException {
		if (wallets.containsKey(wid))
			return wallets.get(wid);
		throw new NotFoundException();
	}

	@Override
	public Wallet update(Wallet wallet) throws NotFoundException, WalletException {
		if (wallets.containsKey(wallet.getWid())) {
			wallets.put(wallet.getWid(), wallet);
			return wallet;
		}
		throw new NotFoundException();
	}
}