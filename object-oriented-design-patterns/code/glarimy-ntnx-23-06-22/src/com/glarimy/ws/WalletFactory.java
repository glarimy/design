package com.glarimy.ws;

import com.glarimy.broker.Broker;
import com.glarimy.broker.BrokerFactory;

public class WalletFactory {
	private static WalletStore store = new InMemoryWalletStore();
	private static Broker broker = BrokerFactory.get();
	private static History history = new EventPublisher(broker);
	private static WalletService service = new SimpleWalletService(store, history);

	public static WalletService get() {
		return service;
	}
}
