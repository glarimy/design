package com.glarimy.ws;

public interface WalletStore {
	public Wallet create(Wallet wallet) throws WalletException;

	public Wallet update(Wallet wallet) throws NotFoundException, WalletException;

	public Wallet read(long wid) throws NotFoundException, WalletException;
}
