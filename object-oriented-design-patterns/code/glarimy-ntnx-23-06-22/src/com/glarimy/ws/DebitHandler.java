package com.glarimy.ws;

import com.glarimy.broker.Event;
import com.glarimy.broker.Handler;

public class DebitHandler implements Handler {
	private WalletService service;

	public DebitHandler() {
		service = WalletFactory.get();
	}

	@Override
	public String getTopic() {
		return "wallet.debit.request";
	}

	@Override
	public void handle(Event event) {
		Long oid = Long.parseLong(event.getMeta().get("oid"));
		Long wid = Long.parseLong(event.getMeta().get("wid"));
		Money amount = new Money(Integer.parseInt(event.getMeta().get("amount")));
		service.debit(oid, wid, amount);
	}
}
