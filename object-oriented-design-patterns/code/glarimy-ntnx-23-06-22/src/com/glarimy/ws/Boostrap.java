package com.glarimy.ws;

import com.glarimy.broker.Broker;
import com.glarimy.broker.BrokerFactory;
import com.glarimy.broker.Event;
import com.glarimy.broker.Handler;

public class Boostrap {
	public static void main(String[] args) {
		WalletService service = WalletFactory.get();
		long wid = service.create(new Name("Krishna", "Koyya"));
		System.out.println(wid);
		Broker broker = BrokerFactory.get();
		broker.subscribe(new DebitHandler());
		/////
		broker.subscribe(new Handler() {

			@Override
			public void handle(Event event) {
				System.out.println(event.getType() + ":" + event.getMeta());
			}

			@Override
			public String getTopic() {
				return "wallet.debit.status";
			}
		});
		broker.notify(new Event.EventBuilder("wallet.debit.request").addHeader("oid", "123").addHeader("amount", "10000")
				.addHeader("wid", "" + wid).build());
	}
}
