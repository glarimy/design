package com.glarimy.ws;

public class Wallet {
	private long wid;
	private Money balance;
	private Name name;

	public Wallet(Name name) {
		this.name = name;
		this.balance = new Money(0);
	}

	public long getWid() {
		return wid;
	}

	public void setWid(long wid) {
		this.wid = wid;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public Money getBalance() {
		return balance;
	}

	public Money credit(Money amount) throws WalletException {
		if (amount.getAmount() % 100 != 0)
			throw new TransactionException("amount must be in multiple of 100s");
		if (balance.getAmount() + amount.getAmount() > 5000)
			throw new TransactionException("crossing max balance");
		balance = new Money(balance.getAmount() + amount.getAmount());
		return balance;
	}

	public Money debit(Money amount) throws WalletException {
		if (balance.getAmount() - amount.getAmount() < 100)
			throw new TransactionException("crossing min balance");
		balance = new Money(balance.getAmount() - amount.getAmount());
		return balance;
	}
}