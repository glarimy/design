package com.glarimy.broker;

public class BrokerFactory {
	private static Broker broker = new SimpleBroker();

	public static Broker get() {
		return broker;
	}
}
