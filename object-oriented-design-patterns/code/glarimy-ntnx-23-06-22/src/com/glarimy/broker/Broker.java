package com.glarimy.broker;

public interface Broker {
	public void subscribe(Handler handler);

	public void notify(Event e);
}
