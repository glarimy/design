package com.glarimy.dir;

public class DirectoryFactory implements Factory {
	private Directory instance;

	public DirectoryFactory() {
		Storage storage = new InMemoryStorage();
		instance = new SimpleDirectory(storage);
	}

	@Override
	public Directory getDirectory() throws DirectoryException {
		return instance;
	}

}
