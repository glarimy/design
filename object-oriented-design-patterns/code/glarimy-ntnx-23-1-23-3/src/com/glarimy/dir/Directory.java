package com.glarimy.dir;

import java.util.List;

public interface Directory {
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException;

	public Employee find(int eid) throws EmployeeNotFoundException, DirectoryException;

	public List<Employee> search(String name) throws DirectoryException;
}
