package com.wallet.domain;

public interface WalletStore {
	public Wallet create(Wallet wallet) throws WalletException;

	public Wallet update(Wallet wallet) throws NotFoundException, WalletException;

	public Wallet read(Phone phone) throws NotFoundException, WalletException;
}
