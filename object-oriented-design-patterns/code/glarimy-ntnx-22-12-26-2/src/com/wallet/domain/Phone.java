package com.wallet.domain;

public class Phone {
	private int isd;
	private long number;

	public Phone(int isd, long number) throws ValidationException {
		if (isd < 1 || number < 1)
			throw new ValidationException();
		this.isd = isd;
		this.number = number;
	}

	public int getIsd() {
		return isd;
	}

	public long getNumber() {
		return number;
	}

}
