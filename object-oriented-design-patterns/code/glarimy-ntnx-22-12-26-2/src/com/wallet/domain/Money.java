package com.wallet.domain;

public class Money {
	private int amount;
	private String currency;

	public Money(int amount, String currency) {
		super();
		this.amount = amount;
		this.currency = currency;
	}

	public int getAmount() {
		return amount;
	}

	public String getCurrency() {
		return currency;
	}

}
