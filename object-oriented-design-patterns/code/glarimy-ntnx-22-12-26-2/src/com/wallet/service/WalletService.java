package com.wallet.service;

import com.wallet.domain.Money;
import com.wallet.domain.Phone;
import com.wallet.domain.Transaction;

public interface WalletService {
	public Transaction credit(Phone phone, Money amount);

	public Transaction debit(Phone phone, Money amount);
}
