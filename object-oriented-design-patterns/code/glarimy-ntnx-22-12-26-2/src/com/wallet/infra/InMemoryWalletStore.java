package com.wallet.infra;

import java.util.HashMap;
import java.util.Map;

import com.wallet.domain.NotFoundException;
import com.wallet.domain.Phone;
import com.wallet.domain.Wallet;
import com.wallet.domain.WalletException;
import com.wallet.domain.WalletStore;

public class InMemoryWalletStore implements WalletStore {
	private Map<Phone, Wallet> entries;
	private static InMemoryWalletStore INSTANCE;

	private InMemoryWalletStore() {
		this.entries = new HashMap<Phone, Wallet>();
	}

	public static synchronized InMemoryWalletStore getInstance() {
		if (INSTANCE == null)
			INSTANCE = new InMemoryWalletStore();
		return INSTANCE;
	}

	@Override
	public Wallet create(Wallet wallet) throws WalletException {
		this.entries.put(wallet.getPhone(), wallet);
		return wallet;
	}

	@Override
	public Wallet update(Wallet wallet) throws NotFoundException, WalletException {
		if (this.entries.containsKey(wallet.getPhone())) {
			this.entries.put(wallet.getPhone(), wallet);
			return wallet;
		}
		throw new NotFoundException();
	}

	@Override
	public Wallet read(Phone phone) throws NotFoundException, WalletException {
		if (this.entries.containsKey(phone))
			return this.entries.get(phone);
		throw new NotFoundException();
	}
}
