package com.wallet.app;

public class Tx extends Response {
	public int txid;
	public int amount;
	public int balance;
	public String currency;
	public String date;
	public long phone;

	@Override
	public String toString() {
		return "Tx [txid=" + txid + ", amount=" + amount + ", balance=" + balance + ", currency=" + currency + ", date="
				+ date + ", phone=" + phone + ", success=" + success + "]";
	}

}
