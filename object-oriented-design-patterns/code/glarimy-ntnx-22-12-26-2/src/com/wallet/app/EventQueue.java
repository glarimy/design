package com.wallet.app;

import com.broker.Event;

public interface EventQueue {

	public void put(Event event);

}
