package com.wallet.app;

import java.util.Date;

import com.broker.Event;
import com.broker.Handler;

public class AuditHandler implements Handler {
	@SuppressWarnings("deprecation")
	@Override
	public void handle(Event e) {
		if (e.getTopic().equals("wallet.registration.succeeded")) {
			Customer customer = (Customer) e.getPayload();
			System.out.println(new Date().toGMTString() + "- " + "Registered a new customer: " + customer.isd + "-"
					+ customer.phone);
		}
		if (e.getTopic().equals("wallet.registration.failed")) {
			Customer customer = (Customer) e.getPayload();
			System.out.println(new Date().toGMTString() + "- " + "Registration failed for customer: " + customer.isd
					+ "-" + customer.phone);
		}
		if (e.getTopic().startsWith("wallet.transaction.")) {
			Tx tx = (Tx) e.getPayload();
			System.out.println(new Date().toGMTString() + "- " + tx);
		}
	}

}
