package com.glarimy.dir;

public class DirectoryFactory implements Factory {

	@Override
	public Directory getDirectory() throws DirectoryException {
		return new SimpleDirectory();
	}

}
