package com.glarimy.dir;

import java.util.List;

public class Client {
	public static void main(String[] args) throws Exception {
		Factory factory = new DirectoryFactory();
		Directory dir = factory.getDirectory();
		Employee e = new Employee();
		e.setName("Krishna Mohan Koyya");
		e.setPhone(9731423166L);
		e.setEmail("krishna@glarimy.com");
		e = dir.add(e);
		List<Employee> list = dir.search("Krishna");
		for (Employee emp : list)
			System.out.println(emp);
	}
}
