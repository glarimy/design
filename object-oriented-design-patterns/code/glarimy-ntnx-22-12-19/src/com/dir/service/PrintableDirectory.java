package com.dir.service;

import com.dir.api.Directory;
import com.dir.api.DirectoryException;
import com.dir.api.Employee;
import com.dir.api.EmployeeNotFoundException;
import com.dir.api.InvalidEmployeeException;

public class PrintableDirectory implements Directory {
	private Directory target;

	public PrintableDirectory(Directory dir) {
		this.target = dir;
	}

	@Override
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException {
		return target.add(e);
	}

	@Override
	public Employee find(int id) throws EmployeeNotFoundException, DirectoryException {
		return target.find(id);
	}

	public void print(int id) throws EmployeeNotFoundException, DirectoryException {
		Employee e = target.find(id);
		System.out.println(e.getName());
	}
}
