package com.dir.service;

import com.dir.api.Directory;
import com.dir.api.DirectoryException;
import com.dir.api.Employee;
import com.dir.api.EmployeeNotFoundException;
import com.dir.api.InvalidEmployeeException;

public class ValidatableDirectory implements Directory {
	private Directory target;

	public ValidatableDirectory(Directory dir) {
		this.target = dir;
	}

	@Override
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException {
		if (e == null || e.getName() == null || e.getName().trim().length() < 3 || e.getPhone() < 1)
			throw new InvalidEmployeeException();
		return target.add(e);
	}

	@Override
	public Employee find(int id) throws EmployeeNotFoundException, DirectoryException {
		return target.find(id);
	}
}
