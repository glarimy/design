package com.dir.api;

public class Employee {
	private int id;
	private String name;
	private String email;
	private long phone;

	private Employee(String name, long phone) {
		this.name = name;
		this.phone = phone;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public long getPhone() {
		return phone;
	}

	public static class EmployeeBuilder {
		private int id;
		private String name;
		private String email;
		private long phone;

		public EmployeeBuilder(String name, long phone) {
			this.name = name;
			this.phone = phone;
		}
		
		public EmployeeBuilder addId(int id) {
			this.id = id;
			return this;
		}
		
		public EmployeeBuilder addEmail(String email) {
			this.email = email;
			return this;
		}
		
		public Employee build() {
			Employee e = new Employee(name, phone);
			e.email = email;
			e.id = id;
			return e;
		}
	}
}