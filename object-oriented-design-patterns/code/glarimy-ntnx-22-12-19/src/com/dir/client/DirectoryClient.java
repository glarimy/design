package com.dir.client;

import com.dir.api.Directory;
import com.dir.api.DirectoryException;
import com.dir.api.Employee;
import com.dir.api.EmployeeNotFoundException;
import com.dir.api.Factory;
import com.dir.api.GenericFactory;
import com.dir.api.InvalidEmployeeException;
import com.dir.service.PrintableDirectory;

public class DirectoryClient {
	public static void main(String[] args) {
		Factory factory = new GenericFactory();
		Directory dir = (Directory) factory.getObject("dir");
		Employee e = new Employee.EmployeeBuilder("Krishna", 9731423166L).addEmail("krishna@glarimy.com").build();
		try {
			Employee employee = dir.add(e);
			Employee result = dir.find(employee.getId());
			//System.out.println("Found:" + result.getName());
			PrintableDirectory pdir = new PrintableDirectory(dir);
			pdir.print(result.getId());
		} catch (InvalidEmployeeException iee) {
			System.out.println("Invalid employee");
		} catch (EmployeeNotFoundException enfe) {
			System.out.println("Employee not found");
		} catch (DirectoryException e1) {
			System.out.println("Internal Error");
		}
	}
}
