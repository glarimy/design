0. Coding against interfaces
1. Factory
2. Factory Method
3. Singleton factory
4. Multiton factory
5. Polymorphic factory
6. DI
7. Pool
8. Config
9. Reflection
10.IoC
11.Framework