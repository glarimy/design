package com.glarimy.patterns.factory9;

public class Client {
	public static void main(String[] args) throws Exception {
		Factory factory = new ComponentFactory("config9.properties");
		Component comp = factory.getComponent("comp");
		comp.service();
	}
}
