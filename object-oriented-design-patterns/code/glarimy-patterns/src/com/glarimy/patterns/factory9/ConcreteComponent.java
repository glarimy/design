package com.glarimy.patterns.factory9;

public class ConcreteComponent implements Component {
	@Override
	public void service() {
		System.out.println("concrete service rendered");
	}
}
