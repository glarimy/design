package com.glarimy.patterns.factory9;

public interface Component {
	public void service();
}
