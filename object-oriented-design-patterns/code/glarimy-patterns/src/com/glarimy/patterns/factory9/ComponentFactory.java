package com.glarimy.patterns.factory9;

import java.io.FileReader;
import java.util.Properties;

public class ComponentFactory implements Factory {
	private Properties config;

	public ComponentFactory(String path) throws Exception {
		config = new Properties();
		config.load(new FileReader(path));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Component getComponent(String type) throws Exception {
		if (!config.containsKey(type))
			throw new RuntimeException();
		Class claz = Class.forName(config.getProperty(type));
		return (Component) claz.getDeclaredConstructor().newInstance();
	}
}
