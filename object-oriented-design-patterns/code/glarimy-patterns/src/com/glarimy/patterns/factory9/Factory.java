package com.glarimy.patterns.factory9;

public interface Factory {
	public Component getComponent(String type) throws Exception;
}
