package com.glarimy.patterns.factory11;

public class SomeLibrary implements Library {
	@Override
	public void function() {
		System.out.println("Library function finished");
	}
}
