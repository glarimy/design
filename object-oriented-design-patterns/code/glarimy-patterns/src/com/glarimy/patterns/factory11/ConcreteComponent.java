package com.glarimy.patterns.factory11;

public class ConcreteComponent implements Component {
	@Refers(key = "dependency")
	private Dependency dependency;

	public ConcreteComponent(Dependency dependency) {
		this.dependency = dependency;
	}

	@Override
	public void service() {
		dependency.compute();
		System.out.println("concrete service rendered");
	}
}
