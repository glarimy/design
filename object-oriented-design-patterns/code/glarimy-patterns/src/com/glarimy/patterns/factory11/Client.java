package com.glarimy.patterns.factory11;

public class Client {
	public static void main(String[] args) throws Exception {
		Factory factory = new ComponentFactory("config11.properties");
		Component comp = (Component) factory.getObject("comp");
		comp.service();
	}
}
