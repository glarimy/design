package com.glarimy.patterns.factory11;

import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.Properties;

public class ComponentFactory implements Factory {
	private Properties config;

	public ComponentFactory(String path) throws Exception {
		config = new Properties();
		config.load(new FileReader(path));
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Object getObject(String type) throws Exception {
		Object component = null;
		if (!config.containsKey(type))
			throw new RuntimeException();
		Class claz = Class.forName(config.getProperty(type));
		Field[] fields = claz.getDeclaredFields();
		if(fields.length == 1) {
			Class fieldType = fields[0].getType();
			String key = fields[0].getDeclaredAnnotation(Refers.class).key();
			Object dependency = getObject(key);
			component = claz.getDeclaredConstructor(fieldType).newInstance(dependency);
		}else {
			component = claz.getDeclaredConstructor().newInstance();
		}
		return component;
	}
}
