package com.glarimy.patterns.factory11;

public class ConcreteDependency implements Dependency {
	@Refers(key = "library")
	private Library library;

	public ConcreteDependency(Library library) {
		this.library = library;
	}

	@Override
	public void compute() {
		library.function();
		System.out.println("Dependent computation completed");
	}
}
