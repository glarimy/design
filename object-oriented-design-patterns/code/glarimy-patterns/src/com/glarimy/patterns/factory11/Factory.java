package com.glarimy.patterns.factory11;

public interface Factory {
	public Object getObject(String type) throws Exception;
}
