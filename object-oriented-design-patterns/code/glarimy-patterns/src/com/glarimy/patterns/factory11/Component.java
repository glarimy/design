package com.glarimy.patterns.factory11;

public interface Component {
	public void service();
}
