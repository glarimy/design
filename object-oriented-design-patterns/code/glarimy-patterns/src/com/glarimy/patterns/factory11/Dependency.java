package com.glarimy.patterns.factory11;

public interface Dependency {
	public void compute();
}
