package com.glarimy.patterns.factory4;

import java.util.HashMap;
import java.util.Map;

public class ComponentFactory implements Factory {
	private Map<String, Component> instances;

	public ComponentFactory() {
		instances = new HashMap<String, Component>();
	}

	@Override
	public Component getComponent(String client) {
		if (!instances.containsKey(client))
			instances.put(client, new ConcreteComponent());
		return instances.get(client);
	}
}
