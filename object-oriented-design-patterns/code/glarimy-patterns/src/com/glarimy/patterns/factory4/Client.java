package com.glarimy.patterns.factory4;

public class Client {
	public static void main(String[] args) {
		Factory factory = new ComponentFactory();
		Component comp = factory.getComponent("local");
		comp.service();
	}
}
