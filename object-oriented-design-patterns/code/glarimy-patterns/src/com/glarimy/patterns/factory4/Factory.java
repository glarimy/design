package com.glarimy.patterns.factory4;

public interface Factory {
	public Component getComponent(String client);
}
