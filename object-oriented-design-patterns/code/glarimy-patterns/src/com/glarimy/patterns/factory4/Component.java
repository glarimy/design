package com.glarimy.patterns.factory4;

public interface Component {
	public void service();
}
