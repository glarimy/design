package com.glarimy.patterns.factory6;

public interface Component {
	public void service();
}
