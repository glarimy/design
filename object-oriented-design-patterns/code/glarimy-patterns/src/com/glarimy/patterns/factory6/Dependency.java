package com.glarimy.patterns.factory6;

public interface Dependency {
	public void compute();
}
