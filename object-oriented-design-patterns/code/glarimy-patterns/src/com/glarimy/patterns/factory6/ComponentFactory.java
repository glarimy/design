package com.glarimy.patterns.factory6;

public class ComponentFactory implements Factory {
	@Override
	public Component getComponent() throws Exception {
		return new ConcreteComponent(new ConcreteDependency());
	}
}
