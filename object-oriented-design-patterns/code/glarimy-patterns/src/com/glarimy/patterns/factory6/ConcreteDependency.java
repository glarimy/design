package com.glarimy.patterns.factory6;

public class ConcreteDependency implements Dependency {
	@Override
	public void compute() {
		System.out.println("Dependent computation completed");
	}
}
