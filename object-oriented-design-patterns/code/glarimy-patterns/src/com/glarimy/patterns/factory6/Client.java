package com.glarimy.patterns.factory6;

public class Client {
	public static void main(String[] args) throws Exception {
		Factory factory = new ComponentFactory();
		Component comp = factory.getComponent();
		comp.service();
	}
}
