package com.glarimy.patterns.factory6;

public class ConcreteComponent implements Component {
	private Dependency dependency;

	public ConcreteComponent(Dependency dependency) {
		this.dependency = dependency;
	}

	@Override
	public void service() {
		dependency.compute();
		System.out.println("concrete service rendered");
	}
}
