package com.glarimy.patterns.factory6;

public interface Factory {
	public Component getComponent() throws Exception;
}
