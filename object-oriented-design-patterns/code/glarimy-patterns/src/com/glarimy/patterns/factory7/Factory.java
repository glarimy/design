package com.glarimy.patterns.factory7;

public interface Factory {
	public Component getComponent();
}
