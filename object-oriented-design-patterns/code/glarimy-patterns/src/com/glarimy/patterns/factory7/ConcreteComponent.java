package com.glarimy.patterns.factory7;

public class ConcreteComponent implements Component {
	private boolean free;

	public ConcreteComponent() {
		free = true;
	}

	@Override
	public void aquire() {
		free = false;
	}

	@Override
	public boolean isFree() {
		return free;
	}

	@Override
	public void release() {
		free = true;
	}

	@Override
	public void service() {
		System.out.println("concrete service rendered");
	}
}
