package com.glarimy.patterns.factory7;

public interface Component {
	public void aquire();
	public void release();
	public boolean isFree();
	public void service();
}
