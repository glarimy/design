package com.glarimy.patterns.factory7;

public class Client {
	public static void main(String[] args){
		Factory factory = new ComponentFactory(2);
		Component comp = factory.getComponent();
		comp.service();
		comp = factory.getComponent();
		comp.service();
		comp.release();
		comp = factory.getComponent();
		comp.service();
		comp = factory.getComponent();
		comp.service();
	}
}
