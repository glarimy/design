package com.glarimy.patterns.factory7;

import java.util.ArrayList;
import java.util.List;

public class ComponentFactory implements Factory {
	private List<Component> pool;

	public ComponentFactory(int size) {
		pool = new ArrayList<Component>();
		for (int i = 0; i < size; i++)
			pool.add(new ConcreteComponent());
	}

	@Override
	public Component getComponent() {
		for(Component c: pool)
			if(c.isFree()) {
				c.aquire();
				return c;
			}
		throw new RuntimeException();
	}
}
