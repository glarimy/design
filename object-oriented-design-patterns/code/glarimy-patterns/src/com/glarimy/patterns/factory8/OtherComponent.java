package com.glarimy.patterns.factory8;

public class OtherComponent implements Component {
	@Override
	public void service() {
		System.out.println("other service rendered");
	}
}
