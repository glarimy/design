package com.glarimy.patterns.factory8;

import java.util.Properties;

public class Client {
	public static void main(String[] args) throws Exception {
		Properties config = new Properties();
		config.put("comp", "com.glarimy.patterns.factory9.ConcreteComponent");
		Factory factory = new ComponentFactory(config);
		Component comp = factory.getComponent("comp");
		comp.service();
	}
}
