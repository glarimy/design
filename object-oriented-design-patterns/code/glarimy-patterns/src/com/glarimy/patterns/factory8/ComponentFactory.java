package com.glarimy.patterns.factory8;

import java.util.Properties;

public class ComponentFactory implements Factory {
	private Properties config;
	public ComponentFactory(Properties config) {
		this.config = config;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Component getComponent(String type) throws Exception {
		if(!config.containsKey(type))
			throw new RuntimeException();
		Class claz = Class.forName(config.getProperty(type));
		return (Component) claz.getDeclaredConstructor().newInstance();
	}
}
