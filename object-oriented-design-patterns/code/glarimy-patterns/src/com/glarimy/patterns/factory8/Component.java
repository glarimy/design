package com.glarimy.patterns.factory8;

public interface Component {
	public void service();
}
