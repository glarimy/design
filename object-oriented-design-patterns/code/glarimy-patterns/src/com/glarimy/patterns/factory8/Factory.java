package com.glarimy.patterns.factory8;

public interface Factory {
	public Component getComponent(String type) throws Exception;
}
