package com.glarimy.patterns.factory8;

public class ConcreteComponent implements Component {
	@Override
	public void service() {
		System.out.println("concrete service rendered");
	}
}
