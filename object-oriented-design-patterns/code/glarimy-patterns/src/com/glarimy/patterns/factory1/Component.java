package com.glarimy.patterns.factory1;

public interface Component {
	public void service();
}
