package com.glarimy.patterns.factory1;

public class Factory {
	public static Component getComponent() {
		return new ConcreteComponent();
	}
}
