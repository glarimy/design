package com.glarimy.patterns.factory1;

public class ConcreteComponent implements Component {
	@Override
	public void service() {
		System.out.println("service rendered");
	}
}
