package com.glarimy.patterns.factory1;

public class Client {
	public static void main(String[] args) {
		Component comp = Factory.getComponent();
		comp.service();
	}
}
