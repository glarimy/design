package com.glarimy.patterns.factory2;

public interface Component {
	public void service();
}
