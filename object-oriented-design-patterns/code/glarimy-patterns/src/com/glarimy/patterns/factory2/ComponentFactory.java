package com.glarimy.patterns.factory2;

public class ComponentFactory implements Factory {
	@Override
	public Component getComponent() {
		return new ConcreteComponent();
	}
}
