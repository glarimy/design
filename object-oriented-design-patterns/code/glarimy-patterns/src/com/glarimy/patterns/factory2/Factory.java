package com.glarimy.patterns.factory2;

public interface Factory {
	public Component getComponent();
}
