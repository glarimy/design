package com.glarimy.patterns.factory2;

public class Client {
	public static void main(String[] args) {
		Factory factory = new ComponentFactory();
		Component comp = factory.getComponent();
		comp.service();
	}
}
