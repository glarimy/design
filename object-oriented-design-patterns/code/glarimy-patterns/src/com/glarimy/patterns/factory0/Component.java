package com.glarimy.patterns.factory0;

public interface Component {
	public void service();
}
