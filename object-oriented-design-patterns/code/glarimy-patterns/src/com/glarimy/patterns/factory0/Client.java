package com.glarimy.patterns.factory0;

public class Client {
	public static void main(String[] args) {
		Component comp = new ConcreteComponent();
		comp.service();
	}
}
