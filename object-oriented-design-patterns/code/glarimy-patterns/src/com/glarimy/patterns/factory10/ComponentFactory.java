package com.glarimy.patterns.factory10;

import java.io.FileReader;
import java.util.Properties;

public class ComponentFactory implements Factory {
	private Properties config;

	public ComponentFactory(String path) throws Exception {
		config = new Properties();
		config.load(new FileReader(path));
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Component getComponent(String type) throws Exception {
		if (!config.containsKey(type))
			throw new RuntimeException();
		Class claz = Class.forName(config.getProperty(type));
		Refers refers = (Refers) claz.getAnnotation(Refers.class);
		Dependency dependency = (Dependency) Class.forName(config.getProperty(refers.key())).getDeclaredConstructor()
				.newInstance();
		return (Component) claz.getDeclaredConstructor(Dependency.class).newInstance(dependency);
	}
}
