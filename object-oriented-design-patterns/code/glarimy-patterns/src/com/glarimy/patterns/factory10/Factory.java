package com.glarimy.patterns.factory10;

public interface Factory {
	public Component getComponent(String type) throws Exception;
}
