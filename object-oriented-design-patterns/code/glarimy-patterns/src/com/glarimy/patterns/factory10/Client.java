package com.glarimy.patterns.factory10;

public class Client {
	public static void main(String[] args) throws Exception {
		Factory factory = new ComponentFactory("config10.properties");
		Component comp = factory.getComponent("comp");
		comp.service();
	}
}
