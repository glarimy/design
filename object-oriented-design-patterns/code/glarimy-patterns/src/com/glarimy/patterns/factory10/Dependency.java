package com.glarimy.patterns.factory10;

public interface Dependency {
	public void compute();
}
