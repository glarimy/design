package com.glarimy.patterns.factory10;

public interface Component {
	public void service();
}
