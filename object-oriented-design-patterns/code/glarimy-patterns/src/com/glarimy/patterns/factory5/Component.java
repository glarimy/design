package com.glarimy.patterns.factory5;

public interface Component {
	public void service();
}
