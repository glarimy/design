package com.glarimy.patterns.factory5;

public class Client {
	public static void main(String[] args) {
		Factory factory = new ComponentFactory();
		Component comp = factory.getComponent("concrete");
		comp.service();
		comp = factory.getComponent("other");
		comp.service();
	}
}
