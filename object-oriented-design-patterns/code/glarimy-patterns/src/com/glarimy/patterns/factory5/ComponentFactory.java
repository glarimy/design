package com.glarimy.patterns.factory5;

public class ComponentFactory implements Factory {
	@Override
	public Component getComponent(String type) {
		if (type.equalsIgnoreCase("concrete"))
			return new ConcreteComponent();
		if (type.equalsIgnoreCase("other"))
			return new OtherComponent();
		throw new RuntimeException();
	}
}
