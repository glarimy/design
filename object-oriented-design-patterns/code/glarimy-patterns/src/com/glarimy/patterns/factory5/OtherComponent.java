package com.glarimy.patterns.factory5;

public class OtherComponent implements Component {
	@Override
	public void service() {
		System.out.println("other service rendered");
	}
}
