package com.glarimy.patterns.factory5;

public class ConcreteComponent implements Component {
	@Override
	public void service() {
		System.out.println("concrete service rendered");
	}
}
