package com.glarimy.patterns.factory5;

public interface Factory {
	public Component getComponent(String type);
}
