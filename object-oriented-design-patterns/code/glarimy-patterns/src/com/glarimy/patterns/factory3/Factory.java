package com.glarimy.patterns.factory3;

public interface Factory {
	public Component getComponent();
}
