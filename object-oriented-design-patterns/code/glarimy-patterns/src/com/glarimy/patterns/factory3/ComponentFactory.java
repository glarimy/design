package com.glarimy.patterns.factory3;

public class ComponentFactory implements Factory {
	private Component instance;

	public ComponentFactory() {
		instance = new ConcreteComponent();
	}

	@Override
	public Component getComponent() {
		return instance;
	}
}
