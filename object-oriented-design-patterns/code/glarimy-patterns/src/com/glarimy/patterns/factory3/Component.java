package com.glarimy.patterns.factory3;

public interface Component {
	public void service();
}
