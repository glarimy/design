from dataclasses import dataclass
from abc import ABC, abstractmethod
# domain 
@dataclass(frozen=True)
class Email:
    value: str
    def __post_init__(self):
        if self.value == None:
            raise ValueError()

@dataclass(frozen=True)
class Phone:
    isd:int
    number: int
    def __post_init__(self):
        if self.isd < 1 and self.number < 1:
            raise ValueError()
    def get_number(self):
        return str(self.isd) + "-" + str(self.number)

@dataclass(frozen=True)
class Name:
    first_name: str
    last_name: str
    def __post_init__(self):
        if len(self.first_name) < 3 or len(self.last_name) < 3:
            raise ValueError()
    def get_fullname(self):
        return self.first_name + " " + self.last_name

@dataclass
class Employee:
    eid: int
    name: Name 
    phone: Phone
    email: Email

class Storage(ABC):
    @abstractmethod
    def create(self, e) -> Employee: pass
    
    @abstractmethod
    def read(self, id) -> Employee: pass

    @abstractmethod
    def read(self, name) -> []: pass

# infra
class InMemoryStorage(Storage):
    def __init__(self):
        self.employees = {}

    def create(self, e) -> Employee:
        self.employees[e.eid] = e
        return e

    def read(self, id) -> Employee:
        return self.employees[id]

    def read(self, name) -> []: pass

# domain service

class Directory(ABC):
    @abstractmethod
    def add(self, e) -> Employee: pass
    
    @abstractmethod
    def find(self, id) -> Employee: pass

    @abstractmethod
    def search(self, name) -> []: pass

class SimpleDirectory(Directory):
    def __init__(self, storage: Storage):
        self.repo = storage

    def add(self, e) -> Employee:
        e.eid = 1
        return self.repo.create(e)
    
    def find(self, id) -> Employee:
        return self.repo.read(id)

    def search(self, name) -> []:
        return self.repo.read(name)
# app service 
@dataclass
class NewEmployee:
    fname: str
    lname: str
    prefix: int
    number: int
    email: str

@dataclass
class EmployeeDetail:
    eid: int
    name: str
    phone: str
    email: str

class DirectoryController: 
    def __init__(self, service:Directory):
        self.service = service
    
    def register(self, dto):
        e = Employee(0, Name(dto.fname, dto.lname), Phone(dto.prefix, dto.number), Email(dto.email))
        e = self.service.add(e)
        return EmployeeDetail(e.eid, e.name.get_fullname(), e.phone.get_number(), e.email.value)
# client
dir = DirectoryController(SimpleDirectory(InMemoryStorage()))
e = dir.register(NewEmployee("Krishna Mohan", "Koyya", 91, 9731423166, "krishna@gmailcom"))
print(e)
