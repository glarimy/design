package com.glarimy.decorator;

public class PrintableCalculator implements ICalculator {
	private ICalculator target;

	public PrintableCalculator(ICalculator target) {
		this.target = target;
	}

	@Override
	public int add(int first, int second) {
		return target.add(first, second);
	}

	@Override
	public int mutipley(int first, int second) {
		return target.mutipley(first, second);
	}

	public void addAndPrint(int first, int second) {
		int sum = target.add(first, second);
		System.out.println(first + "+" + second + "=" + sum);
	}

	public void multiplyAndPrint(int first, int second) {
		int product = target.mutipley(first, second);
		System.out.println(first + "x" + second + "=" + product);
	}
}
