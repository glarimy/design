package com.glarimy.decorator;

import java.util.HashMap;
import java.util.Map;

public class CalculatorFactory implements ICalculatorFactory {
	private Map<Integer, ICalculator> instances = new HashMap<Integer, ICalculator>();

	@Override
	public ICalculator get(int clientId) {
		if(instances.containsKey(clientId))
			return instances.get(clientId);
		else
		{
			ICalculator instance = new CalculatorImpl();
			instances.put(clientId, instance);
			return instance;
		}
	}
}
