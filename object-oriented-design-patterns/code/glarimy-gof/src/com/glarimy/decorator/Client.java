package com.glarimy.decorator;

public class Client {
	public static void main(String[] args) {
		ICalculatorFactory factory = new CalculatorFactory();
		ICalculator calc = factory.get(1);
		PrintableCalculator pc = new PrintableCalculator(calc);
		pc.addAndPrint(1, 5);
	}
}
