package com.glarimy.decorator;

public interface ICalculatorFactory {
	public ICalculator get(int clientId);
}
