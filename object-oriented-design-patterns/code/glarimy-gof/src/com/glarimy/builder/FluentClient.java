package com.glarimy.builder;

public class FluentClient {
	public static void main(String[] args) {
		FluentHttpResponse response = new FluentHttpResponse.ResponseBuilder(200).addBody("Found")
				.addHeader("Content-Type", "JSON").build();
		System.out.println(response.getHeader("Content-Type"));
	}
}
