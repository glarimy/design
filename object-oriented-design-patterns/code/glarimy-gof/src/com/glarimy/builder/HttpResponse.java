package com.glarimy.builder;

import java.util.HashMap;
import java.util.Map;

public class HttpResponse {
	private int status;
	private String body;
	private Map<String, String> headers;

	private HttpResponse(int status) {
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public String getBody() {
		return body;
	}

	public String getHeader(String key) {
		return headers.get(key);
	}

	public static class ResponseBuilder {
		private int status;
		private String body;
		private Map<String, String> headers;

		public ResponseBuilder(int status) {
			this.status = status;
			this.headers = new HashMap<String, String>();
		}

		public void addBody(String body) {
			this.body = body;
		}

		public void addHeader(String key, String value) {
			this.headers.put(key, value);
		}

		public HttpResponse build() {
			HttpResponse response = new HttpResponse(status);
			if (body != null)
				response.body = body;
			response.headers = headers;
			return response;
		}
	}
}
