package com.glarimy.builder;

public class BuilderClient {
	public static void main(String[] args) {
		HttpResponse.ResponseBuilder builder = new HttpResponse.ResponseBuilder(200);
		builder.addBody("Found");
		builder.addHeader("Content-Type", "JSON");
		HttpResponse resp = builder.build();
		System.out.println(resp.getHeader("Content-Type"));
	}
}
