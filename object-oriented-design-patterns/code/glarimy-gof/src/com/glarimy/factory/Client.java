package com.glarimy.factory;

public class Client {
	public static void main(String[] args) {
		ICalculatorFactory factory = new CalculatorFactory();
		ICalculator calc = factory.get(1);
		System.out.println(calc.add(1, 4));
	}
}
