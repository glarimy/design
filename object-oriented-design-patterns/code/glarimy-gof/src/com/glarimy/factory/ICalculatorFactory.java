package com.glarimy.factory;

public interface ICalculatorFactory {
	public ICalculator get(int clientId);
}
