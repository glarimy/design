package com.glarimy.proxy;

public class Client {
	public static void main(String[] args) {
		ICalculatorFactory factory = new CalculatorFactory();
		ICalculator calc = factory.get(1, false);
		PrintableCalculator pc = new PrintableCalculator(calc);
		pc.addAndPrint(1, 5);
	}
}
