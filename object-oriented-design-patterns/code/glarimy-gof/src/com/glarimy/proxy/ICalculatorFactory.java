package com.glarimy.proxy;

public interface ICalculatorFactory {
	public ICalculator get(int clientId, boolean logging);
}
