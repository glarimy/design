package com.glarimy.proxy;

import java.util.HashMap;
import java.util.Map;

public class CalculatorFactory implements ICalculatorFactory {
	private Map<Integer, ICalculator> instances = new HashMap<Integer, ICalculator>();

	@Override
	public ICalculator get(int clientId, boolean logging) {
		ICalculator o;
		if (instances.containsKey(clientId))
			o = instances.get(clientId);
		else {
			ICalculator instance = new CalculatorImpl();
			instances.put(clientId, instance);
			o = instance;
		}
		if (logging)
			return new Logger(o);
		return o;
	}
}
