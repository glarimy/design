package com.glarimy.proxy;

public class Logger implements ICalculator {
	private ICalculator target;

	public Logger(ICalculator target) {
		this.target = target;
	}

	@Override
	public int add(int first, int second) {
		System.out.println("calling add");
		return target.add(first, second);
	}

	@Override
	public int mutipley(int first, int second) {
		System.out.println("calling multiply");
		return target.mutipley(first, second);
	}

}
