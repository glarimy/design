package com.glarimy.ws.domain;

public class Wallet {
	private PhoneNumber phone;
	private Money balance;
	private Name name;

	public Wallet(PhoneNumber phone, Name name, Money balance) {
		this.phone = phone;
		this.name = name;
		this.balance = balance;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public PhoneNumber getPhoneNumber() {
		return phone;
	}

	public Money getBalance() {
		return balance;
	}

	public Money credit(Money amount) throws WalletException {
		if (amount.getAmount() % 100 != 0)
			throw new TransactionException("amount must be in multiple of 100s");
		if (balance.getAmount() + amount.getAmount() > 5000)
			throw new TransactionException("crossing max balance");
		balance = new Money(balance.getAmount() + amount.getAmount(), amount.getCurrency());
		return balance;
	}

	public Money debit(Money amount) throws WalletException {
		if (balance.getAmount() - amount.getAmount() < 100)
			throw new TransactionException("crossing min balance");
		balance = new Money(balance.getAmount() - amount.getAmount(), amount.getCurrency());
		return balance;
	}
}