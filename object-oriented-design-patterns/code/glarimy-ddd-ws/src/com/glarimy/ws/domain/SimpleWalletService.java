package com.glarimy.ws.domain;

import java.util.Optional;

public class SimpleWalletService implements WalletService {
	private WalletStore walletRepo;
	private TransactionStore txRepo;

	public SimpleWalletService(WalletStore walletRepo, TransactionStore txRepo) {
		this.walletRepo = walletRepo;
		this.txRepo = txRepo;
	}

	@Override
	public Transaction credit(PhoneNumber phone, Money amount) throws WalletException {
		Transaction tx = null;
		Optional<Wallet> owallet = walletRepo.read(phone);
		if (owallet.isEmpty())
			throw new NotFoundException("Wallet not found");
		Wallet wallet = owallet.get();
		try {
			Money balance = wallet.credit(amount);
			walletRepo.update(wallet);
			tx = new Transaction(phone, amount, true, true, balance);
		} catch (WalletException e) {
			tx = new Transaction(phone, amount, true, false, wallet.getBalance());
		}
		tx = txRepo.create(tx);
		return tx;
	}

	@Override
	public Transaction debit(PhoneNumber phone, Money amount) throws WalletException {
		Transaction tx = null;
		Optional<Wallet> owallet = walletRepo.read(phone);
		if (owallet.isEmpty())
			throw new WalletException();
		Wallet wallet = owallet.get();
		try {
			Money balance = wallet.debit(amount);
			walletRepo.update(wallet);
			tx = new Transaction(phone, amount, true, true, balance);
		} catch (WalletException e) {
			tx = new Transaction(phone, amount, true, false, wallet.getBalance());
		}
		tx = txRepo.create(tx);
		return tx;
	}
}