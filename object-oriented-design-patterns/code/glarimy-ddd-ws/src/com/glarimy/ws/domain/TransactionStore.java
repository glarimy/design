package com.glarimy.ws.domain;

import java.util.List;
import java.util.Optional;

public interface TransactionStore {
	public Transaction create(Transaction tx) throws TransactionException;
	public List<Transaction> read(PhoneNumber phone) throws TransactionException;
	public Optional<Transaction> read(int txid) throws TransactionException;
}
