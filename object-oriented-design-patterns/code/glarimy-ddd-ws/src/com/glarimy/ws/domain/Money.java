package com.glarimy.ws.domain;

import java.util.Objects;

public class Money {
	private int amount;
	private String currency;

	public Money(int amount, String currency) throws ValidationException {
		if (amount < 0 || currency == null)
			throw new ValidationException("Invalid money");
		if (currency.trim().equalsIgnoreCase("INR") == false)
			throw new ValidationException("Currency not supported");
		this.currency = currency;
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public String getCurrency() {
		return currency;
	}

	public boolean isZero() {
		if (amount == 0)
			return true;
		return false;
	}

	public String format() {
		return currency + " " + amount;
	}

	@Override
	public int hashCode() {
		return Objects.hash(amount, currency);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Money other = (Money) obj;
		return amount == other.amount && Objects.equals(currency, other.currency);
	}

	@Override
	public String toString() {
		return "Money [amount=" + amount + ", currency=" + currency + "]";
	}

}
