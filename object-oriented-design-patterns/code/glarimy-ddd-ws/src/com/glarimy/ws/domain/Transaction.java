package com.glarimy.ws.domain;

import java.util.Date;

public class Transaction {
	private int txid;
	private PhoneNumber wid;
	private Money amount;
	private boolean credit;
	private boolean success;
	private Money balance;
	private Date date;

	public Transaction(PhoneNumber wid, Money amount, boolean credit, boolean success, Money balance) {
		this.wid = wid;
		this.amount = amount;
		this.credit = credit;
		this.success = success;
		this.balance = balance;
		this.date = new Date();
	}

	public int getTxid() {
		return txid;
	}

	public void setTxid(int txid) {
		this.txid = txid;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public Money getBalance() {
		return balance;
	}

	public void setBalance(Money balance) {
		this.balance = balance;
	}

	public boolean isCredit() {
		return credit;
	}

	public void setCredit(boolean credit) {
		this.credit = credit;
	}

	public PhoneNumber getWid() {
		return wid;
	}

	public void setWid(PhoneNumber wid) {
		this.wid = wid;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
