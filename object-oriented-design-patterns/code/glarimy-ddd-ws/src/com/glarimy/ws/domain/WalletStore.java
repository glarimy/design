package com.glarimy.ws.domain;

import java.util.Optional;

public interface WalletStore {
	public Wallet create(Wallet wallet) throws WalletException;

	public Wallet update(Wallet wallet) throws NotFoundException, WalletException;

	public Optional<Wallet> read(PhoneNumber phone) throws WalletException;
}
