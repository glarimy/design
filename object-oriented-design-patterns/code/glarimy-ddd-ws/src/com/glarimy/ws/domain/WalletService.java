package com.glarimy.ws.domain;

public interface WalletService {

	Transaction credit(PhoneNumber phone, Money amount) throws WalletException;

	Transaction debit(PhoneNumber phone, Money amount) throws WalletException;

}