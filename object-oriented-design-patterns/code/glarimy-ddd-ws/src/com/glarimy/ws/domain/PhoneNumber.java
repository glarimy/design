package com.glarimy.ws.domain;

import java.util.Objects;

public class PhoneNumber {
	private int isd;
	private long number;

	public PhoneNumber(int isd, long number) throws ValidationException {
		if (isd < 1 || number < 1)
			throw new ValidationException("Invalid phone number");
		this.isd = isd;
		this.number = number;
	}

	public int getIsd() {
		return isd;
	}

	public long getNumber() {
		return number;
	}

	public String format() {
		return "+" + isd + "-" + number;
	}

	@Override
	public int hashCode() {
		return Objects.hash(isd, number);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhoneNumber other = (PhoneNumber) obj;
		return isd == other.isd && number == other.number;
	}

	@Override
	public String toString() {
		return "PhoneNumber [isd=" + isd + ", number=" + number + "]";
	}

}
