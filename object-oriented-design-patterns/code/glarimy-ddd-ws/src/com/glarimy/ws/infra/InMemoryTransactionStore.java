package com.glarimy.ws.infra;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.glarimy.ws.domain.PhoneNumber;
import com.glarimy.ws.domain.Transaction;
import com.glarimy.ws.domain.TransactionException;
import com.glarimy.ws.domain.TransactionStore;

public class InMemoryTransactionStore implements TransactionStore {
	private Map<Integer, Transaction> txs;
	private int index;

	public InMemoryTransactionStore() {
		txs = new HashMap<Integer, Transaction>();
	}

	@Override
	public Transaction create(Transaction tx) throws TransactionException {
		tx.setTxid(++index);
		tx.setDate(new Date());
		txs.put(tx.getTxid(), tx);
		return tx;
	}

	@Override
	public Optional<Transaction> read(int txid) throws TransactionException {
		if (txs.containsKey(txid))
			return Optional.of(txs.get(txid));
		return Optional.empty();
	}

	@Override
	public List<Transaction> read(PhoneNumber phone) throws TransactionException {
		List<Transaction> results = new ArrayList<Transaction>();
		for (Transaction tx : txs.values())
			if (tx.getWid().equals(phone))
				results.add(tx);
		return results;
	}
}
