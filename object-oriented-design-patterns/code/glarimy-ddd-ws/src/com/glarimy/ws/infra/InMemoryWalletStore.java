package com.glarimy.ws.infra;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.glarimy.ws.domain.NotFoundException;
import com.glarimy.ws.domain.PhoneNumber;
import com.glarimy.ws.domain.Wallet;
import com.glarimy.ws.domain.WalletException;
import com.glarimy.ws.domain.WalletStore;

public class InMemoryWalletStore implements WalletStore {
	private Map<PhoneNumber, Wallet> wallets;

	public InMemoryWalletStore() {
		wallets = new HashMap<PhoneNumber, Wallet>();
	}

	@Override
	public Wallet create(Wallet wallet) throws WalletException {
		wallets.put(wallet.getPhoneNumber(), wallet);
		return wallet;
	}

	@Override
	public Optional<Wallet> read(PhoneNumber phone) throws WalletException {
		if (wallets.containsKey(phone))
			return Optional.of(wallets.get(phone));
		return Optional.empty();
	}

	@Override
	public Wallet update(Wallet wallet) throws NotFoundException, WalletException {
		if (wallets.containsKey(wallet.getPhoneNumber())) {
			wallets.put(wallet.getPhoneNumber(), wallet);
			return wallet;
		}
		throw new NotFoundException();
	}
}