package com.glarimy.ws.ui;

import com.glarimy.ws.app.Account;
import com.glarimy.ws.app.Customer;
import com.glarimy.ws.app.Error;
import com.glarimy.ws.app.Request;
import com.glarimy.ws.app.Response;
import com.glarimy.ws.app.SimpleWalletController;
import com.glarimy.ws.app.WalletController;
import com.glarimy.ws.domain.SimpleWalletService;
import com.glarimy.ws.domain.TransactionStore;
import com.glarimy.ws.domain.WalletService;
import com.glarimy.ws.domain.WalletStore;
import com.glarimy.ws.infra.InMemoryTransactionStore;
import com.glarimy.ws.infra.InMemoryWalletStore;

public class WalletConsole {
	public static void main(String[] args) {
		WalletStore ws = new InMemoryWalletStore();
		TransactionStore ts = new InMemoryTransactionStore();
		WalletService service = new SimpleWalletService(ws, ts);
		WalletController wc = new SimpleWalletController(service, ws, ts);
		Customer customer = new Customer("Krishna Mohan", "Koyya", 91, 9731423166L, "INR", 500);
		Response response = wc.register(customer);
		if (response.error) {
			Error error = (Error) response.body;
			System.out.println(error);
		} else {
			Account account = (Account) response.body;
			System.out.println(account);

			Request request = new Request(91, 9731423166L, true, 102, "INR");
			response = wc.process(request);
			System.out.println(response.body);

			request = new Request(91, 9731423166L, true, 300, "INR");
			response = wc.process(request);
			System.out.println(response.body);

			request = new Request(91, 9731423166L, false, 100, "INR");
			response = wc.process(request);
			System.out.println(response.body);

			System.out.println(wc.fetchRecord(2));
			System.out.println(wc.fetchAccount(91, 9731423166L));
			System.out.println(wc.fetchHistory(91, 9731423166L));
		}
	}
}
