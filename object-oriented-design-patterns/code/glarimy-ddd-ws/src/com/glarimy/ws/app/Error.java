package com.glarimy.ws.app;

public class Error {
	public int code;
	public String message;
	public Object body;

	public Error(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	@Override
	public String toString() {
		return "Error [code=" + code + ", message=" + message + ", body=" + body + "]";
	}

}
