package com.glarimy.ws.app;

public class Account {
	public String phone;
	public String name;
	public String balance;

	public Account(String phone, String name, String balance) {
		super();
		this.phone = phone;
		this.name = name;
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Account [phone=" + phone + ", name=" + name + ", balance=" + balance + "]";
	}

}
