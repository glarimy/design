package com.glarimy.ws.app;

public interface WalletController {
	public Response register(Customer customer);
	public Response process(Request request);
	public Response fetchAccount(int isd, long number);
	public Response fetchRecord(int txid);
	public Response fetchHistory(int isd, long number);

}