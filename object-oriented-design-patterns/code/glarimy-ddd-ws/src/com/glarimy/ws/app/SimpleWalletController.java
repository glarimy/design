package com.glarimy.ws.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.glarimy.ws.domain.Money;
import com.glarimy.ws.domain.Name;
import com.glarimy.ws.domain.NotFoundException;
import com.glarimy.ws.domain.PhoneNumber;
import com.glarimy.ws.domain.Transaction;
import com.glarimy.ws.domain.TransactionStore;
import com.glarimy.ws.domain.ValidationException;
import com.glarimy.ws.domain.Wallet;
import com.glarimy.ws.domain.WalletException;
import com.glarimy.ws.domain.WalletService;
import com.glarimy.ws.domain.WalletStore;

public class SimpleWalletController implements WalletController {
	private WalletService service;
	private WalletStore walletStore;
	private TransactionStore txStore;

	public SimpleWalletController(WalletService service, WalletStore ws, TransactionStore ts) {
		this.service = service;
		this.walletStore = ws;
		this.txStore = ts;
	}

	@Override
	public Response register(Customer customer) {
		try {
			Name name = new Name(customer.firstName, customer.lastName);
			PhoneNumber phone = new PhoneNumber(customer.isd, customer.phone);
			Money money = new Money(customer.deposit, customer.currency);
			Wallet wallet = new Wallet(phone, name, money);
			wallet = walletStore.create(wallet);
			Account account = new Account(wallet.getPhoneNumber().format(), wallet.getName().getFullName(),
					wallet.getBalance().format());
			return new Response(false, account);
		} catch (ValidationException ve) {
			Error error = new Error(400, ve.getMessage());
			return new Response(true, error);
		} catch (WalletException we) {
			Error error = new Error(500, we.getMessage());
			return new Response(true, error);
		}
	}

	@Override
	@SuppressWarnings("deprecation")
	public Response process(Request request) {
		try {
			PhoneNumber phone = new PhoneNumber(request.isd, request.number);
			Money money = new Money(request.amount, request.currency);
			Transaction tx = null;
			if (request.credit) {
				tx = service.credit(phone, money);
			} else {
				tx = service.debit(phone, money);
			}
			Record record = new Record(tx.getTxid(), tx.getWid().format(), tx.getAmount().format(),
					request.credit ? "Credit" : "Debit", tx.getBalance().format(), tx.getDate().toGMTString(),
					tx.isSuccess());
			return new Response(false, record);
		} catch (ValidationException ve) {
			Error error = new Error(400, ve.getMessage());
			return new Response(true, error);
		} catch (NotFoundException nfe) {
			Error error = new Error(404, nfe.getMessage());
			return new Response(true, error);
		} catch (WalletException we) {
			Error error = new Error(500, we.getMessage());
			return new Response(true, error);
		}
	}

	@Override
	public Response fetchAccount(int isd, long number) {
		try {
			PhoneNumber phone = new PhoneNumber(isd, number);
			Optional<Wallet> owallet = walletStore.read(phone);
			if (owallet.isEmpty()) {
				Error error = new Error(404, "No such account");
				return new Response(true, error);
			}
			Wallet wallet = owallet.get();
			Account account = new Account(wallet.getPhoneNumber().format(), wallet.getName().getFullName(),
					wallet.getBalance().format());
			return new Response(false, account);
		} catch (ValidationException ve) {
			Error error = new Error(400, ve.getMessage());
			return new Response(true, error);
		} catch (WalletException we) {
			Error error = new Error(500, we.getMessage());
			return new Response(true, error);
		}
	}

	@Override
	@SuppressWarnings("deprecation")
	public Response fetchRecord(int txid) {
		try {
			Optional<Transaction> otx = txStore.read(txid);
			if (otx.isEmpty()) {
				Error error = new Error(404, "No such record");
				return new Response(true, error);
			}
			Transaction tx = otx.get();
			Record record = new Record(tx.getTxid(), tx.getWid().format(), tx.getAmount().format(), tx.isCredit() ? "Credit" : "Debit",
					tx.getBalance().format(), tx.getDate().toGMTString(), tx.isSuccess());
			return new Response(false, record);
		} catch (ValidationException ve) {
			Error error = new Error(400, ve.getMessage());
			return new Response(true, error);
		} catch (WalletException we) {
			Error error = new Error(500, we.getMessage());
			return new Response(true, error);
		}
	}

	@Override
	@SuppressWarnings("deprecation")
	public Response fetchHistory(int isd, long number) {
		try {
			PhoneNumber phone = new PhoneNumber(isd, number);
			Optional<Wallet> owallet = walletStore.read(phone);
			if (owallet.isEmpty()) {
				Error error = new Error(404, "No such account");
				return new Response(true, error);
			}
			List<Transaction> txs = txStore.read(phone);
			List<Record> records = new ArrayList<Record>();
			for (Transaction tx : txs) {
				Record record = new Record(tx.getTxid(), tx.getWid().format(), tx.getAmount().format(), tx.isCredit() ? "Credit" : "Debit",
						tx.getBalance().format(), tx.getDate().toGMTString(), tx.isSuccess());
				records.add(record);
			}
			return new Response(false, records);
		} catch (ValidationException ve) {
			Error error = new Error(400, ve.getMessage());
			return new Response(true, error);
		} catch (WalletException we) {
			Error error = new Error(500, we.getMessage());
			return new Response(true, error);
		}
	}
}
