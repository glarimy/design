package com.glarimy.ws.app;

public class Response {
	public boolean error;
	public Object body;

	public Response(boolean error, Object body) {
		super();
		this.error = error;
		this.body = body;
	}

	@Override
	public String toString() {
		return "Response [error=" + error + ", body=" + body + "]";
	}

}
