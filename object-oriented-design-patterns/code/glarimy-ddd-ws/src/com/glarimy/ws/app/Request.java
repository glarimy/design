package com.glarimy.ws.app;

public class Request {
	public int isd;
	public long number;
	public boolean credit;
	public int amount;
	public String currency;

	public Request(int isd, long number, boolean credit, int amount, String currency) {
		super();
		this.isd = isd;
		this.number = number;
		this.credit = credit;
		this.amount = amount;
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "Request [isd=" + isd + ", number=" + number + ", credit=" + credit + ", amount=" + amount
				+ ", currency=" + currency + "]";
	}

}
