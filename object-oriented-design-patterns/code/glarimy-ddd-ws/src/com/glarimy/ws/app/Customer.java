package com.glarimy.ws.app;

public class Customer {
	public String firstName;
	public String lastName;
	public int isd;
	public long phone;
	public String currency;
	public int deposit;

	public Customer(String firstName, String lastName, int isd, long phone, String currency, int deposit) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.isd = isd;
		this.phone = phone;
		this.currency = currency;
		this.deposit = deposit;
	}

	@Override
	public String toString() {
		return "Customer [firstName=" + firstName + ", lastName=" + lastName + ", isd=" + isd + ", phone=" + phone
				+ ", currency=" + currency + ", deposit=" + deposit + "]";
	}

}
