package com.glarimy.ws.app;

public class Record {
	public int txid;
	public String phone;
	public String amount;
	public String type;
	public String balance;
	public String date;
	public boolean success;

	public Record(int txid, String phone, String amount, String type, String balance, String date, boolean success) {
		super();
		this.txid = txid;
		this.phone = phone;
		this.amount = amount;
		this.type = type;
		this.balance = balance;
		this.date = date;
		this.success = success;
	}

	@Override
	public String toString() {
		return "Record [txid=" + txid + ", phone=" + phone + ", amount=" + amount + ", type=" + type + ", balance="
				+ balance + ", date=" + date + ", success=" + success + "]";
	}

}
