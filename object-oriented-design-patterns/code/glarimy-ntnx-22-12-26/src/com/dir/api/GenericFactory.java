package com.dir.api;

import java.io.FileReader;
import java.util.Properties;

public class GenericFactory implements Factory {

	@SuppressWarnings({ "rawtypes", "deprecation" })
	@Override
	public Object getObject(String key) throws RuntimeException {
		try {
			Properties config = new Properties();
			config.load(new FileReader("config.properties"));
			String name = config.getProperty(key);
			Class claz = Class.forName(name);
			Object o = claz.newInstance();
			return o;
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

}
