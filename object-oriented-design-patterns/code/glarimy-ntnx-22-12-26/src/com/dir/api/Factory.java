package com.dir.api;

public interface Factory {

	public Object getObject(String key) throws RuntimeException;

}
