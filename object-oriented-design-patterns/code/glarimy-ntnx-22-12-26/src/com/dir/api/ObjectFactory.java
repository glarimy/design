package com.dir.api;

import com.dir.service.InMemoryStoage;
import com.dir.service.SimpleDirectory;
import com.dir.service.Storage;
import com.dir.service.ValidatableDirectory;

public class ObjectFactory implements Factory {
	private Storage storage;
	private Directory directory;

	public ObjectFactory() {
		storage = new InMemoryStoage();
		directory = new SimpleDirectory(storage);
	}

	@Override
	public Object getObject(String key) throws RuntimeException {
		if (key.equalsIgnoreCase("dir")) {
			return new ValidatableDirectory(directory);
		}
		if (key.equalsIgnoreCase("storage"))
			return storage;
		throw new RuntimeException();
	}

}
