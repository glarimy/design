package com.dir.api;

public interface Directory {
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException;

	public Employee find(int id) throws EmployeeNotFoundException, DirectoryException;
}
