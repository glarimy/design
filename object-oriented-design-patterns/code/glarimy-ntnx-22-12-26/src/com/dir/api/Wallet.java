package com.dir.api;

public class Wallet {
	private PhoneNumber phone;
	private Money balance;
	
	public Wallet(PhoneNumber phone) {
		this.phone = phone;
	}

	public PhoneNumber getPhone() {
		return phone;
	}

	public Money getBalance() {
		return balance;
	}

	public Money credit(Money money) throws WalletException {
		if(money.getAmount() % 100)
			throw new WalletException();
		if(balance.getAmount() + money.getAmount() > 5000)
			throw new WalletException();
		balance = new Balance(balance.getAmount() + money.getAmount());
		return balance;
	}
	
	public Money debit(Money money) throws WalletException {
		if(money.getAmount() % 100)
			throw new WalletException();
		if(balance.getAmount() - money.getAmount() < 100)
			throw new WalletException();
		balance = new Balance(balance.getAmount() - money.getAmount());
		return balance;
	}
}