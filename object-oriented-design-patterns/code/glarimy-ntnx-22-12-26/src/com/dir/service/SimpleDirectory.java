package com.dir.service;

import com.dir.api.Directory;
import com.dir.api.DirectoryException;
import com.dir.api.Employee;
import com.dir.api.EmployeeNotFoundException;
import com.dir.api.InvalidEmployeeException;

public class SimpleDirectory implements Directory {
	private Storage storage;

	public SimpleDirectory(Storage storage) {
		this.storage = storage;
	}

	@Override
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException {
		int id = 1;
		Employee entry = new Employee.EmployeeBuilder(e.getName(), e.getPhone()).addEmail(e.getEmail()).addId(id)
				.build();
		return storage.create(entry);
	}

	@Override
	public Employee find(int id) throws EmployeeNotFoundException, DirectoryException {
		Employee e = storage.read(id);
		if (e == null)
			throw new EmployeeNotFoundException();
		return e;
	}
}
