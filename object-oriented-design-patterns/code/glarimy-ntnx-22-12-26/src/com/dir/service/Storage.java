package com.dir.service;

import com.dir.api.Employee;

public interface Storage {
	public Employee create(Employee e);

	public Employee read(int eid);
}
