package com.dir.service;

import java.util.HashMap;
import java.util.Map;

import com.dir.api.Employee;

public class InMemoryStoage implements Storage {
	private Map<Integer, Employee> entries;

	public InMemoryStoage() {
		this.entries = new HashMap<Integer, Employee>();
	}

	@Override
	public Employee create(Employee e) {
		this.entries.put(e.getId(), e);
		return e;
	}

	@Override
	public Employee read(int eid) {
		Employee e = this.entries.get(eid);
		return e;
	}
}
