package com.dir.ui;

import java.util.HashMap;
import java.util.Map;

import com.dir.app.DefaultFactory;
import com.dir.app.EmployeeController;
import com.dir.app.Factory;
import com.dir.app.NewEmployee;
import com.dir.app.Response;

public class DirectoryConsole {
	public static void main(String[] args) {
		Map<String, String> config = new HashMap<String, String>();
		config.put("db", "cache");
		Factory factory = new DefaultFactory();
		EmployeeController dir = factory.get(config);
		NewEmployee e = new NewEmployee();
		e.name = "Krishna Mohan Koyya";
		e.phone = 9731423166L;
		e.email = "krishna@glarimy.com";
		Response response = dir.add(e);
		System.out.println(response);
		response = dir.findById(1);
		System.out.println(response);
	}
}
