package com.dir.app;

public interface EmployeeController {
	public Response add(NewEmployee e);

	public Response findById(int eid);

	public Response findByName(String name);
}
