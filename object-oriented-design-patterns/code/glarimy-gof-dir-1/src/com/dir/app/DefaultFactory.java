package com.dir.app;

import java.util.Map;

import com.dir.domain.EmployeeRepository;
import com.dir.infra.InMemoryEmployeeRepository;

public class DefaultFactory implements Factory {
	private EmployeeRepository repo;

	public DefaultFactory() {
		// eager and singleton
		repo = new InMemoryEmployeeRepository();
	}

	@Override
	public EmployeeController get(Map<String, String> config) {
		// strategy
		if (config.get("db").equals("cache"))
			// dependency injection
			return new SimpleEmployeeController(repo);
		throw new RuntimeException("Initialization failed");
	}
}
