package com.dir.domain;

import java.util.Objects;

public class Phone {
	private long number;

	public Phone(long number) throws ValidationException {
		if (number < 1)
			throw new ValidationException();
		this.number = number;
	}

	public long getNumber() {
		return number;
	}

	@Override
	public int hashCode() {
		return Objects.hash(number);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Phone other = (Phone) obj;
		return number == other.number;
	}

	@Override
	public String toString() {
		return "Phone [number=" + number + "]";
	}

}
