package com.dir.domain;

public class Email {
	private String id;

	public Email(String id) throws ValidationException{
		// validated ... may be by using regex
		this.id = id;
	}

	public String getId() {
		return id;
	}

}
