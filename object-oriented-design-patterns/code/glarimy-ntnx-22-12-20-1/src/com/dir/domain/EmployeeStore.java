package com.dir.domain;

public interface EmployeeStore {
	public Employee create(Employee employee) throws DirectoryException;

	public Employee find(int eid) throws NotFoundException, DirectoryException;
}
