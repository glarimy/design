package com.dir.app;

import com.dir.domain.DirectoryException;
import com.dir.domain.Email;
import com.dir.domain.Employee;
import com.dir.domain.EmployeeStore;
import com.dir.domain.Name;
import com.dir.domain.NotFoundException;
import com.dir.domain.PhoneNumber;
import com.dir.domain.ValidationException;

public class DirectoryController implements Directory {
	private EmployeeStore repo;

	public DirectoryController(EmployeeStore store) {
		this.repo = store;
	}

	@Override
	public Response add(NewEmployee e) {
		try {
			Name name = new Name(e.firstName, e.lastName);
			PhoneNumber phone = new PhoneNumber(e.isd, e.phone);
			Email email = new Email(e.email);
			Employee employee = new Employee(name);
			employee.setEmail(email);
			employee.setPhone(phone);
			employee = repo.create(employee);
			EmployeeRecord record = new EmployeeRecord();
			record.eid = employee.getEid();
			record.email = employee.getEmail().getId();
			record.phone = employee.getPhone().getIsd() + "-" + employee.getPhone().getNumber();
			record.name = employee.getName().getFirstName() + " " + employee.getName().getLastName();
			record.success = true;
			return record;
		} catch (ValidationException ve) {
			Error error = new Error();
			error.success = false;
			error.code = 400;
			error.message = "validation error";
			return error;
		} catch (DirectoryException de) {
			Error error = new Error();
			error.success = false;
			error.code = 500;
			error.message = "internal error";
			return error;
		}
	}

	@Override
	public Response find(int eid) {
		try {
			Employee employee = repo.find(eid);
			EmployeeRecord record = new EmployeeRecord();
			record.eid = employee.getEid();
			record.email = employee.getEmail().getId();
			record.phone = employee.getPhone().getIsd() + "-" + employee.getPhone().getNumber();
			record.name = employee.getName().getFirstName() + " " + employee.getName().getLastName();
			record.success = true;
			return record;
		} catch (NotFoundException nfe) {
			Error error = new Error();
			error.success = false;
			error.code = 404;
			error.message = "no such employee";
			return error;
		} catch (DirectoryException de) {
			Error error = new Error();
			error.success = false;
			error.code = 500;
			error.message = "internal error";
			return error;
		}
	}
}
