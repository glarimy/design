package com.dir.app;

public class Error extends Response {
	public int code;
	public String message;

	@Override
	public String toString() {
		return "Error [code=" + code + ", message=" + message + ", success=" + success + "]";
	}

}
