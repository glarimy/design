package com.dir.app;

public interface Directory {
	public Response add(NewEmployee e);
	public Response find(int eid);
}
