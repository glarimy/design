package com.dir.app;

public class EmployeeRecord extends Response {
	public String name;
	public String phone;
	public String email;
	public int eid;

	@Override
	public String toString() {
		return "EmployeeRecord [name=" + name + ", phone=" + phone + ", email=" + email + ", eid=" + eid + ", success="
				+ success + "]";
	}

}
