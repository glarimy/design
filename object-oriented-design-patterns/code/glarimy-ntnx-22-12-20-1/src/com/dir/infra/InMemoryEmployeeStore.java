package com.dir.infra;

import java.util.HashMap;
import java.util.Map;

import com.dir.domain.DirectoryException;
import com.dir.domain.Employee;
import com.dir.domain.EmployeeStore;
import com.dir.domain.NotFoundException;

public class InMemoryEmployeeStore implements EmployeeStore {
	private Map<Integer, Employee> entries;
	private static InMemoryEmployeeStore INSTANCE;

	private InMemoryEmployeeStore() {
		this.entries = new HashMap<Integer, Employee>();
	}

	public static synchronized InMemoryEmployeeStore getInstance() {
		if (INSTANCE == null)
			INSTANCE = new InMemoryEmployeeStore();
		return INSTANCE;
	}

	@Override
	public Employee create(Employee employee) throws DirectoryException {
		employee.setEid(this.entries.size() + 1);
		this.entries.put(employee.getEid(), employee);
		return employee;
	}

	@Override
	public Employee find(int eid) throws DirectoryException {
		if (this.entries.containsKey(eid))
			return this.entries.get(eid);
		throw new NotFoundException();
	}
}
