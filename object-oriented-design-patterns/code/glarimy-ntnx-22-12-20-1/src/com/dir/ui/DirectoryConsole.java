package com.dir.ui;

import com.dir.app.Directory;
import com.dir.app.DirectoryController;
import com.dir.app.EmployeeRecord;
import com.dir.app.NewEmployee;
import com.dir.app.Response;
import com.dir.app.Error;
import com.dir.domain.EmployeeStore;
import com.dir.infra.InMemoryEmployeeStore;

public class DirectoryConsole {
	public static void main(String[] args) {
		EmployeeStore store = InMemoryEmployeeStore.getInstance();
		Directory dir = new DirectoryController(store);
		NewEmployee e = new NewEmployee();
		e.firstName = "Krishna Mohan";
		e.lastName = "Koyya";
		e.isd = 91;
		e.phone = 9731423166L;
		e.email = "krishna@glarimy.com";
		Response response = dir.add(e);
		if (response.success)
			System.out.println((EmployeeRecord) response);
		else
			System.out.println((Error) response);
		response = dir.find(1);
		if (response.success)
			System.out.println((EmployeeRecord) response);
		else
			System.out.println((Error) response);
	}
}
