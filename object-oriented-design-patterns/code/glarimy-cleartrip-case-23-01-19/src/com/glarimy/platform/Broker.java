package com.glarimy.platform;

public interface Broker {
	public void subscribe(Handler notifier);

	public void notify(Event e);
}
