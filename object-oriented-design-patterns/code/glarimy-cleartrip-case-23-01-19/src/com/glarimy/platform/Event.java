package com.glarimy.platform;

import java.util.HashMap;
import java.util.Map;

public class Event {
	private String type;
	private Map<String, String> headers;
	private Object payload;

	private Event() {

	}

	public String getType() {
		return type;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public Object getPayload() {
		return payload;
	}

	public static class EventBuilder {
		private String type;
		private Map<String, String> headers;
		private Object payload;

		public EventBuilder(String type) {
			headers = new HashMap<String, String>();
			this.type = type;
		}

		public EventBuilder addHeader(String key, String value) {
			headers.put(key, value);
			return this;
		}

		public EventBuilder setBody(Object o) {
			this.payload = o;
			return this;
		}

		public Event build() {
			Event e = new Event();
			e.headers = headers;
			e.type = type;
			e.payload = payload;
			return e;
		}
	}
}
