package com.glarimy.platform;

public interface Handler {
	public void on(Event event);
}
