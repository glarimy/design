package com.glarimy.platform;

import java.util.ArrayList;
import java.util.List;

public class SimpleBroker implements Broker {
	private List<Handler> handlers;
	private static SimpleBroker INSTANCE;

	private SimpleBroker() {
		handlers = new ArrayList<Handler>();
	}
	
	public static synchronized SimpleBroker getInstance() {
		if(INSTANCE == null)
			INSTANCE = new SimpleBroker();
		return INSTANCE;
	}

	@Override
	public void notify(Event e) {
		for (Handler h : handlers)
			h.on(e);
	}

	@Override
	public void subscribe(Handler handler) {
		handlers.add(handler);
	}
}
