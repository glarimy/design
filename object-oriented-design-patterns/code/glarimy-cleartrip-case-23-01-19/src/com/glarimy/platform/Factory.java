package com.glarimy.platform;

public interface Factory {
	public Object get(String key);
}
