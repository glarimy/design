package com.glarimy.ws.ui;

import com.glarimy.platform.Broker;
import com.glarimy.platform.Factory;
import com.glarimy.platform.SimpleBroker;
import com.glarimy.ws.app.SimpleWalletController;
import com.glarimy.ws.app.WalletController;
import com.glarimy.ws.db.InMemoryTransactionStore;
import com.glarimy.ws.db.InMemoryWalletStore;
import com.glarimy.ws.domain.TransactionStore;
import com.glarimy.ws.domain.WalletStore;
import com.glarimy.ws.domain.service.SimpleWalletService;
import com.glarimy.ws.domain.service.WalletService;

public class ObjectFactory implements Factory {
	@Override
	public Object get(String key) {
		if (key == "controller") {
			WalletStore ws = InMemoryWalletStore.getInstance();
			TransactionStore ts = InMemoryTransactionStore.getInstance();
			WalletService service = new SimpleWalletService(ws, ts);
			Broker broker = (Broker) get("broker");
			WalletController wc = new SimpleWalletController(service, ws, ts, broker);
			return wc;
		}
		if (key == "broker") {
			return SimpleBroker.getInstance();
		}
		throw new RuntimeException();
	}
}
