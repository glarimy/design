package com.glarimy.ws.ui;

import com.glarimy.platform.Broker;
import com.glarimy.platform.Factory;
import com.glarimy.ws.app.WalletController;
import com.glarimy.ws.app.dto.Request;

public class WalletConsole {
	public static void main(String[] args) {
		Factory factory = new ObjectFactory();

		Broker broker = (Broker) factory.get("broker");
		broker.subscribe(new SmsNotifier());
		broker.subscribe(new EmailNotifier());

		WalletController wc = (WalletController) factory.get("controller");
		wc.register("Krishna Mohan", "Koyya", 9731423166L, "krishna@glarimy.com");

		Request req = new Request();
		req.amount = 100;
		req.phone = 9731423166L;
		req.type = "topup";

		wc.transact(req);
	}
}
