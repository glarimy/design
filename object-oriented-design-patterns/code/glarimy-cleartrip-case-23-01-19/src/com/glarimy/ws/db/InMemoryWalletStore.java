package com.glarimy.ws.db;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.glarimy.ws.domain.WalletStore;
import com.glarimy.ws.domain.entities.Wallet;
import com.glarimy.ws.domain.exceptions.NotFoundException;
import com.glarimy.ws.domain.exceptions.WalletException;
import com.glarimy.ws.domain.vo.PhoneNumber;

public class InMemoryWalletStore implements WalletStore {
	private Map<PhoneNumber, Wallet> wallets;

	private static InMemoryWalletStore INSTANCE;

	public static synchronized InMemoryWalletStore getInstance() {
		if (INSTANCE == null)
			INSTANCE = new InMemoryWalletStore();
		return INSTANCE;
	}

	private InMemoryWalletStore() {
		wallets = new HashMap<PhoneNumber, Wallet>();
	}

	@Override
	public Wallet save(Wallet wallet) throws WalletException {
		wallets.put(wallet.getPhone(), wallet);
		return wallet;
	}

	@Override
	public Optional<Wallet> fetch(PhoneNumber phone) throws WalletException {
		if (wallets.containsKey(phone))
			return Optional.of(wallets.get(phone));
		return Optional.empty();
	}

	@Override
	public Wallet update(Wallet wallet) throws NotFoundException, WalletException {
		if (wallets.containsKey(wallet.getPhone())) {
			wallets.put(wallet.getPhone(), wallet);
			return wallet;
		}
		throw new NotFoundException();
	}
}