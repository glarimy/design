package com.glarimy.ws.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.glarimy.ws.domain.TransactionStore;
import com.glarimy.ws.domain.entities.Transaction;
import com.glarimy.ws.domain.exceptions.TransactionException;
import com.glarimy.ws.domain.vo.PhoneNumber;

public class InMemoryTransactionStore implements TransactionStore {
	private Map<Integer, Transaction> txs;
	private int index;
	private static InMemoryTransactionStore INSTANCE;

	private InMemoryTransactionStore() {
		txs = new HashMap<Integer, Transaction>();
	}
	
	public static synchronized InMemoryTransactionStore getInstance() {
		if(INSTANCE == null)
			INSTANCE = new InMemoryTransactionStore();
		return INSTANCE;
	}

	@Override
	public Transaction save(Transaction tx) throws TransactionException {
		tx.setTxid(++index);
		txs.put(tx.getTxid(), tx);
		return tx;
	}

	@Override
	public Optional<Transaction> fetch(int txid) throws TransactionException {
		if (txs.containsKey(txid))
			return Optional.of(txs.get(txid));
		return Optional.empty();
	}

	@Override
	public List<Transaction> fetch(PhoneNumber phone) throws TransactionException {
		List<Transaction> results = new ArrayList<Transaction>();
		for (Transaction tx : txs.values())
			if (tx.getWid().equals(phone))
				results.add(tx);
		return results;
	}
}
