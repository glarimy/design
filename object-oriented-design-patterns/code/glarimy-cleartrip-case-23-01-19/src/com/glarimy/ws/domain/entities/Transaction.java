package com.glarimy.ws.domain.entities;

import com.glarimy.ws.domain.vo.PhoneNumber;

public class Transaction {
	private int txid;
	private PhoneNumber wid;
	private double amount;
	private String type;
	private double balance;

	public Transaction(PhoneNumber wid, double amount, String type, double balance) {
		this.wid = wid;
		this.amount = amount;
		this.type = type;
		this.balance = balance;
	}

	public int getTxid() {
		return txid;
	}

	public void setTxid(int txid) {
		this.txid = txid;
	}

	public PhoneNumber getWid() {
		return wid;
	}

	public void setWid(PhoneNumber wid) {
		this.wid = wid;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}


}
