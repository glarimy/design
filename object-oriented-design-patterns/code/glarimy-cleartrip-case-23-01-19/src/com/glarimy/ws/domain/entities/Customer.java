package com.glarimy.ws.domain.entities;

import com.glarimy.ws.domain.vo.Email;
import com.glarimy.ws.domain.vo.Name;
import com.glarimy.ws.domain.vo.PhoneNumber;

public class Customer {
	private PhoneNumber phone;
	private Name name;
	private Email email;

	public Customer(PhoneNumber phone, Name name, Email email) {
		this.phone = phone;
		this.name = name;
		this.email = email;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public PhoneNumber getPhoneNumber() {
		return phone;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

}