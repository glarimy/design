package com.glarimy.ws.domain.service;

import com.glarimy.ws.domain.entities.Transaction;
import com.glarimy.ws.domain.exceptions.WalletException;
import com.glarimy.ws.domain.vo.PhoneNumber;

public interface WalletService {

	Transaction topup(PhoneNumber phone, double amount) throws WalletException;

	Transaction withdraw(PhoneNumber phone, double amount) throws WalletException;

}