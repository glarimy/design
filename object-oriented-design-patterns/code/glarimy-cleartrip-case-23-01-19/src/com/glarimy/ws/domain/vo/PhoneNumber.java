package com.glarimy.ws.domain.vo;

import java.util.Objects;

import com.glarimy.ws.domain.exceptions.ValidationException;

public class PhoneNumber {
	private long value;

	public PhoneNumber(long number) throws ValidationException {
		if (number < 1)
			throw new ValidationException("Invalid phone number");
		this.value = number;
	}

	public long getNumber() {
		return value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhoneNumber other = (PhoneNumber) obj;
		return value == other.value;
	}

	@Override
	public String toString() {
		return "PhoneNumber [value=" + value;
	}

}
