package com.glarimy.ws.domain;

import java.util.List;
import java.util.Optional;

import com.glarimy.ws.domain.entities.Transaction;
import com.glarimy.ws.domain.exceptions.TransactionException;
import com.glarimy.ws.domain.vo.PhoneNumber;

public interface TransactionStore {
	public Transaction save(Transaction tx) throws TransactionException;
	public List<Transaction> fetch(PhoneNumber wid) throws TransactionException;
	public Optional<Transaction> fetch(int txid) throws TransactionException;
}
