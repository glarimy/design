package com.glarimy.ws.domain.vo;

import com.glarimy.ws.domain.exceptions.ValidationException;

public class Email {
	private String value;

	public Email(String value) {
		if (value == null)
			throw new ValidationException("Invalid email");
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
