package com.glarimy.ws.domain.service;

import java.util.Optional;

import com.glarimy.ws.domain.TransactionStore;
import com.glarimy.ws.domain.WalletStore;
import com.glarimy.ws.domain.entities.Transaction;
import com.glarimy.ws.domain.entities.Wallet;
import com.glarimy.ws.domain.exceptions.NotFoundException;
import com.glarimy.ws.domain.exceptions.WalletException;
import com.glarimy.ws.domain.vo.PhoneNumber;

public class SimpleWalletService implements WalletService {
	private WalletStore walletRepo;
	private TransactionStore txRepo;

	public SimpleWalletService(WalletStore walletRepo, TransactionStore txRepo) {
		this.walletRepo = walletRepo;
		this.txRepo = txRepo;
	}

	@Override
	public Transaction topup(PhoneNumber phone, double amount) throws WalletException {
		Transaction tx = null;
		Optional<Wallet> owallet = walletRepo.fetch(phone);
		if (owallet.isEmpty())
			throw new NotFoundException("Wallet not found");
		Wallet wallet = owallet.get();
		try {
			double balance = wallet.topup(amount);
			walletRepo.update(wallet);
			tx = new Transaction(phone, amount, "topup", balance);
		} catch (WalletException e) {
			tx = new Transaction(phone, amount, "failed-topup", wallet.getBalance());
		}
		tx = txRepo.save(tx);
		return tx;
	}

	@Override
	public Transaction withdraw(PhoneNumber phone, double amount) throws WalletException {
		Transaction tx = null;
		Optional<Wallet> owallet = walletRepo.fetch(phone);
		if (owallet.isEmpty())
			throw new WalletException();
		Wallet wallet = owallet.get();
		try {
			double balance = wallet.withdraw(amount);
			walletRepo.update(wallet);
			tx = new Transaction(phone, amount, "withdraw", balance);
		} catch (WalletException e) {
			tx = new Transaction(phone, amount, "failed-withdraw", wallet.getBalance());
		}
		tx = txRepo.save(tx);
		return tx;
	}
}