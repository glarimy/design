package com.glarimy.ws.domain.entities;

import com.glarimy.ws.domain.exceptions.TransactionException;
import com.glarimy.ws.domain.exceptions.WalletException;
import com.glarimy.ws.domain.vo.PhoneNumber;

public class Wallet {
	private PhoneNumber phone;
	private double balance;
	private Customer customer;

	public Wallet(PhoneNumber phone, Customer customer) {
		this.phone = phone;
		this.customer = customer;
	}

	public PhoneNumber getPhone() {
		return phone;
	}

	public double getBalance() {
		return balance;
	}

	public Customer getCustomer() {
		return customer;
	}

	public double topup(double amount) throws WalletException {
		if (amount % 100 != 0)
			throw new TransactionException("amount must be in multiple of 100s");
		if (balance + amount > 5000)
			throw new TransactionException("crossing max balance");
		balance = balance + amount;
		return balance;
	}

	public double withdraw(double amount) throws WalletException {
		if (balance - amount < 100)
			throw new TransactionException("crossing min balance");
		balance = balance - amount;
		return balance;
	}
}