package com.glarimy.ws.domain;

import java.util.Optional;

import com.glarimy.ws.domain.entities.Wallet;
import com.glarimy.ws.domain.exceptions.NotFoundException;
import com.glarimy.ws.domain.exceptions.WalletException;
import com.glarimy.ws.domain.vo.PhoneNumber;

public interface WalletStore {
	public Wallet save(Wallet wallet) throws WalletException;

	public Wallet update(Wallet wallet) throws NotFoundException, WalletException;

	public Optional<Wallet> fetch(PhoneNumber phone) throws WalletException;
}
