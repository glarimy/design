package com.glarimy.ws.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.glarimy.platform.Broker;
import com.glarimy.platform.Event;
import com.glarimy.ws.app.dto.Error;
import com.glarimy.ws.app.dto.Request;
import com.glarimy.ws.app.dto.Record;
import com.glarimy.ws.app.dto.Response;
import com.glarimy.ws.domain.TransactionStore;
import com.glarimy.ws.domain.WalletStore;
import com.glarimy.ws.domain.entities.Customer;
import com.glarimy.ws.domain.entities.Transaction;
import com.glarimy.ws.domain.entities.Wallet;
import com.glarimy.ws.domain.exceptions.NotFoundException;
import com.glarimy.ws.domain.exceptions.ValidationException;
import com.glarimy.ws.domain.exceptions.WalletException;
import com.glarimy.ws.domain.service.WalletService;
import com.glarimy.ws.domain.vo.Email;
import com.glarimy.ws.domain.vo.Name;
import com.glarimy.ws.domain.vo.PhoneNumber;

public class SimpleWalletController implements WalletController {
	private WalletService service;
	private WalletStore walletStore;
	private TransactionStore txStore;
	private Broker broker;

	public SimpleWalletController(WalletService service, WalletStore ws, TransactionStore ts, Broker broker) {
		this.service = service;
		this.walletStore = ws;
		this.txStore = ts;
		this.broker = broker;
	}

	@Override
	public Response register(String firstName, String lastName, long phone, String email) {
		try {
			Name name = new Name(firstName, lastName);
			PhoneNumber phoneNumber = new PhoneNumber(phone);
			Email mail = new Email(email);
			Customer customer = new Customer(phoneNumber, name, mail);
			Wallet wallet = new Wallet(phoneNumber, customer);
			wallet = walletStore.save(wallet);
			return new Response(true, phone);
		} catch (ValidationException ve) {
			Error error = new Error(400, ve.getMessage());
			return new Response(false, error);
		} catch (WalletException we) {
			Error error = new Error(500, we.getMessage());
			return new Response(false, error);
		}
	}

	@Override
	public Response transact(Request request) {
		try {
			PhoneNumber phone = new PhoneNumber(request.phone);
			Transaction tx = null;
			if (request.type.equalsIgnoreCase("topup")) {
				tx = service.topup(phone, request.amount);
			} else {
				tx = service.withdraw(phone, request.amount);
			}
			Event e = new Event.EventBuilder("wallet").setBody(tx).build();
			broker.notify(e);
			Record record = new Record(tx.getTxid(), tx.getWid().getNumber(), tx.getAmount(), request.type,
					tx.getBalance());
			return new Response(false, record);
		} catch (ValidationException ve) {
			Error error = new Error(400, ve.getMessage());
			return new Response(true, error);
		} catch (NotFoundException nfe) {
			Error error = new Error(404, nfe.getMessage());
			return new Response(true, error);
		} catch (WalletException we) {
			Error error = new Error(500, we.getMessage());
			return new Response(true, error);
		}
	}

	@Override
	public Response fetchRecord(int txid) {
		try {
			Optional<Transaction> otx = txStore.fetch(txid);
			if (otx.isEmpty()) {
				Error error = new Error(404, "No such record");
				return new Response(true, error);
			}
			Transaction tx = otx.get();
			Record record = new Record(tx.getTxid(), tx.getWid().getNumber(), tx.getAmount(), tx.getType(),
					tx.getBalance());
			return new Response(false, record);
		} catch (ValidationException ve) {
			Error error = new Error(400, ve.getMessage());
			return new Response(true, error);
		} catch (WalletException we) {
			Error error = new Error(500, we.getMessage());
			return new Response(true, error);
		}
	}

	@Override
	public Response fetchHistory(long number) {
		try {
			PhoneNumber phone = new PhoneNumber(number);
			Optional<Wallet> owallet = walletStore.fetch(phone);
			if (owallet.isEmpty()) {
				Error error = new Error(404, "No such account");
				return new Response(true, error);
			}
			List<Transaction> txs = txStore.fetch(phone);
			List<Record> records = new ArrayList<Record>();
			for (Transaction tx : txs) {
				Record record = new Record(tx.getTxid(), tx.getWid().getNumber(), tx.getAmount(), tx.getType(),
						tx.getBalance());
				records.add(record);
			}
			return new Response(false, records);
		} catch (ValidationException ve) {
			Error error = new Error(400, ve.getMessage());
			return new Response(true, error);
		} catch (WalletException we) {
			Error error = new Error(500, we.getMessage());
			return new Response(true, error);
		}
	}
}
