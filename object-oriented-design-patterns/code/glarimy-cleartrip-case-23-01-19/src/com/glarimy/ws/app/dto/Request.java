package com.glarimy.ws.app.dto;

public class Request {
	public long phone;
	public String type;
	public double amount;

	@Override
	public String toString() {
		return "Request [phone=" + phone + ", type=" + type + ", amount=" + amount + "]";
	}

}
