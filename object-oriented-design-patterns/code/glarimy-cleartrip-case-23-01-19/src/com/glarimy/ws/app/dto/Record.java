package com.glarimy.ws.app.dto;

public class Record {
	public int txid;
	public long phone;
	public double amount;
	public String type;
	public double balance;

	public Record(int txid, long phone, double amount, String type, double balance) {
		super();
		this.txid = txid;
		this.phone = phone;
		this.amount = amount;
		this.type = type;
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Record [txid=" + txid + ", phone=" + phone + ", amount=" + amount + ", type=" + type + ", balance="
				+ balance + "]";
	}

}
