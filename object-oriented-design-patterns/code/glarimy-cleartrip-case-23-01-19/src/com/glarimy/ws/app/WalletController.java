package com.glarimy.ws.app;

import com.glarimy.ws.app.dto.Request;
import com.glarimy.ws.app.dto.Response;

public interface WalletController {
	public Response register(String firstName, String lastName, long phone, String email);
	public Response transact(Request request);
	public Response fetchRecord(int txid);
	public Response fetchHistory(long phone);

}