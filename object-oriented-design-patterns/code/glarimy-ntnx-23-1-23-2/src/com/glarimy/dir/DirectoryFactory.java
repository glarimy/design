package com.glarimy.dir;

public class DirectoryFactory implements Factory {
	private Directory instance;

	public DirectoryFactory() {
		instance = new SimpleDirectory();
	}

	@Override
	public Directory getDirectory() throws DirectoryException {
		return instance;
	}

}
