package com.glarimy.dir;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SimpleDirectory implements Directory {
	private Map<Integer, Employee> entries;
	private int nextId;

	public SimpleDirectory() {
		entries = new HashMap<Integer, Employee>();
		nextId = 1;
	}

	@Override
	public Employee add(Employee e) throws InvalidEmployeeException, DirectoryException {
		if (e == null || e.getName() == null || e.getEmail() == null || e.getPhone() < 1)
			throw new InvalidEmployeeException();
		e.setEid(nextId);
		entries.put(nextId, e);
		nextId++;
		return e;
	}

	@Override
	public Employee find(int eid) throws EmployeeNotFoundException, DirectoryException {
		if (entries.containsKey(eid))
			return entries.get(eid);
		throw new EmployeeNotFoundException();
	}

	@Override
	public List<Employee> search(String name) throws DirectoryException {
		return entries.values().stream().filter(e -> e.getName().contains(name)).collect(Collectors.toList());
	}
}
