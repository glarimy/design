package com.glarimy.dir;

public interface Factory {
	public Directory getDirectory() throws DirectoryException;
}
