from dataclasses import dataclass
from abc import ABC, abstractmethod

@dataclass
class Employee:
    eid: int
    name: str 
    phone: int
    email: str

class Directory(ABC):
    @abstractmethod
    def add(self, e) -> Employee: pass
    
    @abstractmethod
    def find(self, id) -> Employee: pass

    @abstractmethod
    def search(self, name) -> []: pass

class SimpleDirectory(Directory):
    def __init__(self):
        self.employees = {}

    def add(self, e) -> Employee:
        e.eid = 1
        self.employees[e.eid] = e
        return e
    
    def find(self, id) -> Employee:
        return self.employees[id]

    def search(self, name) -> []:
        pass

dir = SimpleDirectory()
e = dir.add(Employee(0, "Krishna Mohan Koyya", 9731423166, "krishna@gmailcom"))
print(e)
