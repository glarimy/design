package com.dir.app;

import java.util.HashMap;
import java.util.Map;

public class Event {
	private String type;
	private Object body;
	private Map<String, Object> headers;

	private Event(String type, Object body, Map<String, Object> headers) {
		this.type = type;
		this.body = body;
		this.headers = headers;
	}

	public String getType() {
		return type;
	}

	public Object getBody() {
		return body;
	}

	public Object getHeader(String header) {
		return headers.get(header);
	}

	public static class EventBuilder {
		private String type;
		private Map<String, Object> headers;
		private Object body;

		public EventBuilder(String type) {
			this.type = type;
			headers = new HashMap<String, Object>();
		}

		public EventBuilder addHeader(String header, Object value) {
			headers.put(header, value);
			return this;
		}

		public EventBuilder setBody(Object o) {
			this.body = o;
			return this;
		}

		public Event build() {
			Event event = new Event(type, body, headers);
			return event;
		}
	}

}
