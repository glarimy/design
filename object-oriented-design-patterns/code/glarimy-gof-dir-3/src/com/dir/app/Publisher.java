package com.dir.app;

import java.util.Date;

public class Publisher implements EmployeeController {
	private EmployeeController target;

	public Publisher(EmployeeController target) {
		this.target = target;
	}

	@Override
	public Response add(NewEmployee e) {
//		Event.EventBuilder builder = new Event.EventBuilder("employee.add");
//		builder.addHeader("timestamp", new Date());
//		builder.addHeader("severity", 1);
//		builder.setBody(e);
//		Event event = builder.build();

		Event event = new Event.EventBuilder("employee.add").setBody(e).addHeader("timestamp", new Date())
				.addHeader("severity", "1").build();
		System.out.println("publishing " + event);

		Response response = target.add(e);
		if (response.success)
			System.out.println(new Date() + "Event: add completed");
		else
			System.out.println(new Date() + "Event: add failed");
		return response;
	}

	@Override
	public Response findById(int eid) {
		System.out.println(new Date() + "Event: find by id attempted");
		Response response = target.findById(eid);
		if (response.success)
			System.out.println(new Date() + "Event: find by id completed");
		else
			System.out.println(new Date() + "Event: find by id failed");
		return response;
	}

	@Override
	public Response findByName(String name) {
		System.out.println(new Date() + "Event: find by name attempted");
		Response response = target.findByName(name);
		if (response.success)
			System.out.println(new Date() + "Event: find by name completed");
		else
			System.out.println(new Date() + "Event: find by name failed");
		return response;
	}
}
