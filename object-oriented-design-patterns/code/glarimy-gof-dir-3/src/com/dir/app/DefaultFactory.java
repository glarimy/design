package com.dir.app;

import java.util.Map;

import com.dir.domain.EmployeeRepository;
import com.dir.infra.InMemoryEmployeeRepository;

public class DefaultFactory implements Factory {
	private EmployeeRepository repo;

	public DefaultFactory() {
		// eager and singleton
		repo = new InMemoryEmployeeRepository();
	}

	@Override
	public EmployeeController get(Map<String, String> config) {
		EmployeeController target;
		// strategy
		if (config.get("db").equals("cache"))
			// dependency injection
			target = new SimpleEmployeeController(repo);
		else
			throw new RuntimeException("Initialization failed");
		
		// chain of responsibilities
		if (config.get("logging").equals("enabled"))
			target = new Logger(target);
		
		if (config.get("notifications").equals("enabled"))
			target = new Publisher(target);
		
		return target;
	}
}
