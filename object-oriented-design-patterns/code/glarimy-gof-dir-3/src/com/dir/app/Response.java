package com.dir.app;

public class Response {
	public boolean success;
	public Object body;

	@Override
	public String toString() {
		return "Response [success=" + success + ", body=" + body + "]";
	}

}
