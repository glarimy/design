package com.dir.ui;

import java.util.HashMap;
import java.util.Map;

import com.dir.app.DefaultFactory;
import com.dir.app.EmployeeController;
import com.dir.app.Factory;
import com.dir.app.Monitor;
import com.dir.app.NewEmployee;
import com.dir.app.Response;

public class DirectoryConsole {
	public static void main(String[] args) {
		Map<String, String> config = new HashMap<String, String>();
		config.put("db", "cache");
		config.put("logging", "disabled");
		config.put("notifications", "disabled");

		Factory factory = new DefaultFactory();
		EmployeeController dir = factory.get(config);

		// add a monitor to the controller
		Monitor monitor = new Monitor(dir);

		NewEmployee e = new NewEmployee();
		e.name = "Krishna Mohan Koyya";
		e.phone = 9731423166L;
		e.email = "krishna@glarimy.com";
		Response response = monitor.add(e);
		System.out.println(response);
		response = monitor.findById(1);
		System.out.println(response);

		monitor.doSomethingElse();
	}
}
