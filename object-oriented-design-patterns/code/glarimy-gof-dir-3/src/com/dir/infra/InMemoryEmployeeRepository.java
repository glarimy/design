package com.dir.infra;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.dir.domain.DirectoryException;
import com.dir.domain.Employee;
import com.dir.domain.EmployeeRepository;
import com.dir.domain.NotFoundException;

public class InMemoryEmployeeRepository implements EmployeeRepository {
	private Map<Integer, Employee> entries;

	public InMemoryEmployeeRepository() {
		this.entries = new HashMap<Integer, Employee>();
	}

	@Override
	public Employee save(Employee employee) throws DirectoryException {
		employee.setEid(this.entries.size() + 1);
		this.entries.put(employee.getEid(), employee);
		return employee;
	}

	@Override
	public Employee fetch(int eid) throws NotFoundException, DirectoryException {
		if (this.entries.containsKey(eid))
			return this.entries.get(eid);
		throw new NotFoundException();
	}

	@Override
	public List<Employee> search(String name) throws DirectoryException {
		return entries.values().stream()
				.filter(e -> e.getName().getFirstName().contains(name) || e.getName().getLastName().contains(name))
				.collect(Collectors.toList());
	}
}
