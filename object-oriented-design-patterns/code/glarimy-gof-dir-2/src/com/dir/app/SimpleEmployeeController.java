package com.dir.app;

import java.util.ArrayList;
import java.util.List;

import com.dir.domain.DirectoryException;
import com.dir.domain.Email;
import com.dir.domain.Employee;
import com.dir.domain.EmployeeRepository;
import com.dir.domain.Name;
import com.dir.domain.NotFoundException;
import com.dir.domain.Phone;
import com.dir.domain.ValidationException;

public class SimpleEmployeeController implements EmployeeController {
	private EmployeeRepository repo;

	public SimpleEmployeeController(EmployeeRepository store) {
		this.repo = store;
	}

	@Override
	public Response add(NewEmployee e) {
		try {
			int index = e.name.lastIndexOf(" ");
			String firstName = e.name.substring(0, index);
			String lastName = e.name.substring(index);
			Name name = new Name(firstName, lastName);
			Phone phone = new Phone(e.phone);
			Email email = new Email(e.email);
			Employee employee = new Employee(name);
			employee.setEmail(email);
			employee.setPhone(phone);
			employee = repo.save(employee);
			EmployeeRecord record = new EmployeeRecord();
			record.eid = employee.getEid();
			record.email = employee.getEmail().getValue();
			record.phone = employee.getPhone().getNumber();
			record.name = employee.getName().getFirstName() + " " + employee.getName().getLastName();
			Response response = new Response();
			response.success = true;
			response.body = record;
			return response;
		} catch (ValidationException ve) {
			Error error = new Error();
			error.code = 400;
			error.message = "validation error";
			Response response = new Response();
			response.success = false;
			response.body = error;
			return response;
		} catch (DirectoryException de) {
			Error error = new Error();
			error.code = 500;
			error.message = "internal error";
			Response response = new Response();
			response.success = false;
			response.body = error;
			return response;
		}
	}

	@Override
	public Response findById(int eid) {
		try {
			Employee employee = repo.fetch(eid);
			EmployeeRecord record = new EmployeeRecord();
			record.eid = employee.getEid();
			record.email = employee.getEmail().getValue();
			record.phone = employee.getPhone().getNumber();
			record.name = employee.getName().getFirstName() + " " + employee.getName().getLastName();
			Response response = new Response();
			response.success = true;
			response.body = record;
			return response;
		} catch (NotFoundException nfe) {
			Error error = new Error();
			error.code = 404;
			error.message = "no such employee";
			Response response = new Response();
			response.success = false;
			response.body = error;
			return response;
		} catch (DirectoryException de) {
			Error error = new Error();
			error.code = 500;
			error.message = "internal error";
			Response response = new Response();
			response.success = false;
			response.body = error;
			return response;
		}
	}

	@Override
	public Response findByName(String name) {
		try {
			List<Employee> employees = repo.search(name);
			List<EmployeeRecord> records = new ArrayList<EmployeeRecord>();
			for (Employee employee : employees) {
				EmployeeRecord record = new EmployeeRecord();
				record.eid = employee.getEid();
				record.email = employee.getEmail().getValue();
				record.phone = employee.getPhone().getNumber();
				record.name = employee.getName().getFirstName() + " " + employee.getName().getLastName();
				records.add(record);
			}
			Response response = new Response();
			response.success = true;
			response.body = records;
			return response;
		} catch (DirectoryException de) {
			Error error = new Error();
			error.code = 500;
			error.message = "internal error";
			Response response = new Response();
			response.success = false;
			response.body = error;
			return response;
		}
	}
}
