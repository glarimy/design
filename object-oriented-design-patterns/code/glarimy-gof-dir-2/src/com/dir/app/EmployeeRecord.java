package com.dir.app;

public class EmployeeRecord {
	public String name;
	public long phone;
	public String email;
	public int eid;

	@Override
	public String toString() {
		return "EmployeeRecord [name=" + name + ", phone=" + phone + ", email=" + email + ", eid=" + eid + "]";
	}

}
