package com.dir.app;

import java.util.Map;

public interface Factory {
	public EmployeeController get(Map<String, String> config);
}
