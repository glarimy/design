package com.wallet.infra;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.wallet.domain.NotFoundException;
import com.wallet.domain.Phone;
import com.wallet.domain.Transaction;
import com.wallet.domain.TransactionException;
import com.wallet.domain.TransactionStore;

public class InMemoryTransactionStore implements TransactionStore {
	private Map<Integer, Transaction> entries;
	private static InMemoryTransactionStore INSTANCE;

	private InMemoryTransactionStore() {
		this.entries = new HashMap<Integer, Transaction>();
	}

	public static synchronized InMemoryTransactionStore getInstance() {
		if (INSTANCE == null)
			INSTANCE = new InMemoryTransactionStore();
		return INSTANCE;
	}

	@Override
	public Transaction create(Transaction tx) throws TransactionException {
		tx.setTxid(entries.size() + 1);
		entries.put(tx.getTxid(), tx);
		return tx;
	}

	@Override
	public List<Transaction> read(Phone phone) throws NotFoundException, TransactionException {
		return entries.values().stream().filter(tx -> tx.getWid().getNumber() == phone.getNumber())
				.collect(Collectors.toList());
	}

	@Override
	public Transaction read(int txid) throws NotFoundException, TransactionException {
		if (this.entries.containsKey(txid))
			return this.entries.get(txid);
		throw new NotFoundException();
	}

}
