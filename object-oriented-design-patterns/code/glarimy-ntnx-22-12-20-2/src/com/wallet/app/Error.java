package com.wallet.app;

public class Error extends Response {
	public int code;
	public String message;
	public Object data;

	@Override
	public String toString() {
		return "Error [code=" + code + ", message=" + message + ", data=" + data + "]";
	}

}
