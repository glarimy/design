package com.wallet.app;

import java.util.ArrayList;
import java.util.List;

import com.wallet.domain.Money;
import com.wallet.domain.Name;
import com.wallet.domain.NotFoundException;
import com.wallet.domain.Phone;
import com.wallet.domain.Transaction;
import com.wallet.domain.TransactionException;
import com.wallet.domain.TransactionStore;
import com.wallet.domain.ValidationException;
import com.wallet.domain.Wallet;
import com.wallet.domain.WalletException;
import com.wallet.domain.WalletStore;
import com.wallet.service.WalletService;

public class SimpleWalletController implements WalletController {
	private WalletStore walletRepo;
	private TransactionStore txRepo;
	private WalletService service;

	public SimpleWalletController(WalletStore walletRepo, TransactionStore txRepo, WalletService service) {
		this.walletRepo = walletRepo;
		this.txRepo = txRepo;
		this.service = service;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Response history(int isd, long wid) {
		try {
			Phone phone = new Phone(isd, wid);
			List<Transaction> list = txRepo.read(phone);
			List<Tx> results = new ArrayList<Tx>();
			for (Transaction t : list) {
				Tx tx = new Tx();
				tx.amount = t.getAmount().getAmount();
				tx.currency = t.getAmount().getCurrency();
				tx.date = t.getDate().toGMTString();
				tx.txid = t.getTxid();
				tx.phone = t.getWid().getNumber();
				results.add(tx);
			}
			History history = new History();
			history.success = true;
			history.list = results;
			return history;
		} catch (NotFoundException nfe) {
			Error error = new Error();
			error.success = false;
			error.code = 404;
			error.message = "no such record";
			return error;
		} catch (WalletException de) {
			Error error = new Error();
			error.success = false;
			error.code = 500;
			error.message = "internal error";
			return error;
		}
	}

	@Override
	public Response register(Customer cust) {
		try {
			Name name = new Name(cust.firstName, cust.lastName);
			Phone phone = new Phone(cust.isd, cust.phone);
			Wallet wallet = new Wallet(phone, name);
			wallet = walletRepo.create(wallet);
			Response response = new Response();
			response.success = true;
			return response;
		} catch (ValidationException ve) {
			Error error = new Error();
			error.success = false;
			error.code = 400;
			error.message = "validation error";
			return error;
		} catch (WalletException de) {
			Error error = new Error();
			error.success = false;
			error.code = 500;
			error.message = "internal error";
			return error;
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public Response transact(Request req) {
		try {
			Transaction t = null;
			if (req.credit)
				t = service.credit(new Phone(req.isd, req.number), new Money(req.amount, req.currency));
			else
				t = service.debit(new Phone(req.isd, req.number), new Money(req.amount, req.currency));
			if(t.isSuccess()) {
				Tx tx = new Tx();
				tx.amount = t.getAmount().getAmount();
				tx.currency = t.getAmount().getCurrency();
				tx.date = t.getDate().toGMTString();
				tx.txid = t.getTxid();
				tx.phone = t.getWid().getNumber();
				tx.success = true;
				return tx;				
			}else {
				Error error = new Error();
				error.success = false;
				error.code = 400;
				error.message = "validation error";
				return error;				
			}
		} catch (ValidationException e) {
			Error error = new Error();
			error.success = false;
			error.code = 400;
			error.message = "validation error";
			return error;
		}
	}
}
