package com.wallet.app;

public interface WalletController {
	public Response register(Customer cust);

	public Response transact(Request req);

	public Response history(int isd, long wid);
}
