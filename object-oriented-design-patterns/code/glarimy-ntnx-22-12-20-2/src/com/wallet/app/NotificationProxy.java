package com.wallet.app;

import com.broker.Event;

public class NotificationProxy implements WalletController {
	private WalletController target;
	private EventQueue queue;

	public NotificationProxy(WalletController target, EventQueue queue) {
		super();
		this.target = target;
		this.queue = queue;
	}

	@Override
	public Response history(int isd, long wid) {
		return target.history(isd, wid);
	}

	@Override
	public Response register(Customer cust) {
		Response response = target.register(cust);
		if (response.success)
			queue.put(new Event.EventBuilder("wallet.registration.succeeded").addPayload(cust).build());
		else
			queue.put(new Event.EventBuilder("wallet.registration.failed").addPayload(cust).build());
		return response;
	}

	@Override
	public Response transact(Request req) {
		Response response = target.transact(req);
		if (response.success)
			queue.put(new Event.EventBuilder("wallet.transaction.succeeded").addPayload(response).build());
		else
			queue.put(new Event.EventBuilder("wallet.transaction.failed").addPayload(response).build());
		return response;
	}
}
