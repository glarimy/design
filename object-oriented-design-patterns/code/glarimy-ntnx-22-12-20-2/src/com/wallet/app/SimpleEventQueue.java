package com.wallet.app;

import com.broker.Broker;
import com.broker.Event;

public class SimpleEventQueue implements EventQueue {
	private Broker broker;

	public SimpleEventQueue(Broker broker) {
		this.broker = broker;
	}

	@Override
	public void put(Event event) {
		broker.notify(event);
	}

}
