package com.wallet.domain;

import java.util.Date;

public class Transaction {
	private int txid;
	private Date date;
	private Money amount;
	private boolean credit;
	private Phone wid;
	private boolean success;

	public Transaction(Money amount, boolean credit, Phone wid, boolean success) {
		super();
		this.amount = amount;
		this.credit = credit;
		this.wid = wid;
		this.success = success;
		this.date = new Date();
	}

	public int getTxid() {
		return txid;
	}

	public void setTxid(int txid) {
		this.txid = txid;
	}

	public Date getDate() {
		return date;
	}

	public Money getAmount() {
		return amount;
	}

	public boolean isCredit() {
		return credit;
	}

	public Phone getWid() {
		return wid;
	}

	public boolean isSuccess() {
		return success;
	}

}
