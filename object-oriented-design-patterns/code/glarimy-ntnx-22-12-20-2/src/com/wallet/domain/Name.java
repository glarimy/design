package com.wallet.domain;

public class Name {
	private String firstName;
	private String lastName;

	public Name(String firstName, String lastName) throws ValidationException {
		if (firstName == null || lastName == null || firstName.trim().length() < 3 || lastName.trim().length() < 1)
			throw new ValidationException();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

}
