package com.wallet.domain;

import java.util.List;

public interface TransactionStore {
	public Transaction create(Transaction tx) throws TransactionException;

	public List<Transaction> read(Phone phone) throws NotFoundException, TransactionException;

	public Transaction read(int txid) throws NotFoundException, TransactionException;
}
