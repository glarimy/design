package com.wallet.domain;

public class Wallet {
	private Phone phone;
	private Money balance;
	private Name name;

	public Wallet(Phone phone, Name name) {
		super();
		this.name = name;
	}

	public Name getName() {
		return name;
	}

	public void setName(Name name) {
		this.name = name;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public Money getBalance() {
		return balance;
	}

	public void setBalance(Money balance) {
		this.balance = balance;
	}

	public Money credit(Money amount) throws TransactionException {
		if (amount.getAmount() % 100 != 0)
			throw new TransactionException("amount must be in multiple of 100s");
		if (balance.getAmount() + amount.getAmount() <= 5000) {
			balance = new Money(balance.getAmount() + amount.getAmount(), amount.getCurrency());
			return balance;
		}
		throw new TransactionException("crossing max balance");
	}

	public Money debit(Money amount) throws TransactionException {
		if (balance.getAmount() - amount.getAmount() >= 100) {
			balance = new Money(balance.getAmount() - amount.getAmount(), amount.getCurrency());
			return balance;
		}
		throw new TransactionException("crossing max balance");
	}

}
