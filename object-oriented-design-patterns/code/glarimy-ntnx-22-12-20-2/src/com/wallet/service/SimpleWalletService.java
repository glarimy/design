package com.wallet.service;

import com.wallet.domain.Money;
import com.wallet.domain.Phone;
import com.wallet.domain.Transaction;
import com.wallet.domain.TransactionException;
import com.wallet.domain.TransactionStore;
import com.wallet.domain.Wallet;
import com.wallet.domain.WalletStore;

public class SimpleWalletService implements WalletService {
	private WalletStore walletRepo;
	private TransactionStore txRepo;

	public SimpleWalletService(WalletStore walletRepo, TransactionStore txRepo) {
		this.walletRepo = walletRepo;
		this.txRepo = txRepo;
	}

	@Override
	public Transaction credit(Phone phone, Money amount) {
		Transaction tx = null;
		try {
			Wallet wallet = walletRepo.read(phone);
			wallet.credit(amount);
			tx = new Transaction(amount, true, phone, true);
			tx = txRepo.create(tx);
			walletRepo.update(wallet);
		} catch (Exception e) {
			tx = new Transaction(amount, true, phone, false);
			try {
				tx = txRepo.create(tx);
			} catch (TransactionException te) {
			}
		}
		return tx;
	}

	@Override
	public Transaction debit(Phone phone, Money amount) {
		Transaction tx = null;
		try {
			Wallet wallet = walletRepo.read(phone);
			wallet.debit(amount);
			tx = new Transaction(amount, false, phone, true);
			tx = txRepo.create(tx);
			walletRepo.update(wallet);
		} catch (Exception e) {
			tx = new Transaction(amount, false, phone, false);
			try {
				tx = txRepo.create(tx);
			} catch (TransactionException te) {
			}
		}
		return tx;
	}

}
