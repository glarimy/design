package com.broker;

public interface Handler {

	public void handle(Event e);
}
