package com.broker;

import java.util.HashMap;
import java.util.Map;

public class Event {
	private String topic;
	private Object payload;
	private Map<String, Object> headers;

	private Event() {

	}

	public String getTopic() {
		return topic;
	}

	public Object getPayload() {
		return payload;
	}

	public Map<String, Object> getHeaders() {
		return headers;
	}

	public static class EventBuilder {
		private String topic;
		private Object payload;
		private Map<String, Object> headers;

		public EventBuilder(String topic) {
			this.topic = topic;
			headers = new HashMap<String, Object>();
		}

		public EventBuilder addPayload(Object o) {
			this.payload = o;
			return this;
		}

		public EventBuilder addHeader(String header, Object Value) {
			headers.put(header, Value);
			return this;
		}

		public Event build() {
			Event event = new Event();
			event.topic = this.topic;
			event.payload = this.payload;
			event.headers = this.headers;
			return event;
		}
	}
}
