

public class Broker {
	public String role = "follower";

	public void lead() {
		System.out.println("Leader");
	}

	public void follow() {
		System.out.println("Follow");
	}
}
