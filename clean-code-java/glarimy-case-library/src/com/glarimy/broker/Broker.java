package com.glarimy.broker;

public interface Broker {
	public void publish(Message message);

	public long subscribe(String type, Handler handler);

	public void unsubscribe(long id);
}
