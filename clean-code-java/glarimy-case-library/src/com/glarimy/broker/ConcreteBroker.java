package com.glarimy.broker;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ConcreteBroker implements Broker {
	private static ConcreteBroker INSTANCE;
	private Map<String, Map<Long, Handler>> handlers;

	private ConcreteBroker() {
		handlers = new HashMap<String, Map<Long, Handler>>();
	}

	public static ConcreteBroker getInstance() {
		if (INSTANCE == null)
			INSTANCE = new ConcreteBroker();
		return INSTANCE;
	}

	@Override
	public void publish(Message message) {
		Executor threads = Executors.newFixedThreadPool(5);
		for (Handler handler : handlers.get(message.getType()).values()) {
			threads.execute(() -> {
				handler.handle(message);
			});
		}
	}

	@Override
	public long subscribe(String type, Handler handler) {
		if (!handlers.containsKey(type))
			handlers.put(type, new HashMap<Long, Handler>());
		long id = new Date().getTime();
		handlers.get(type).put(id, handler);
		return id;
	}

	@Override
	public void unsubscribe(long id) {
		for(Map<Long, Handler> handler: handlers.values())
			if(handler.containsKey(id)) {
				handler.remove(id);
				break;
			}
	}
}
