package in.koyya.ui;

import in.koyya.dir.api.Directory;
import in.koyya.dir.api.DirectoryFactory;
import in.koyya.dir.api.Employee;
import in.koyya.dir.api.Factory;
import in.koyya.dir.api.PrintableDirectory;
import in.koyya.dir.api.SimplePrintableDirectory;
import in.koyya.dir.api.exceptions.EmployeeNotFoundException;
import in.koyya.dir.api.exceptions.InvalidEmployeeException;

public class Client {
    public static void main(String[] args) {
        Factory factory = new DirectoryFactory();
        Directory directory = (Directory) factory.getDirectory();
        Employee employee = new Employee("Krishna Mohan Koyya", 9731423166L,
                "krishnamohan@koyya.in");
        try {
            employee = directory.add(employee);
        } catch (InvalidEmployeeException iee) {
            System.out.println("Employee is failed to be added. Supply valid name");
        }

        try {
            PrintableDirectory pdir = new SimplePrintableDirectory(directory);
            pdir.print(1);
        }catch(EmployeeNotFoundException enfe){
            System.out.println("Employee not found");
        }
    }
}
