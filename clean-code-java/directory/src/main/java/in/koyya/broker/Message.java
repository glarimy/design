package in.koyya.broker;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Message {
    private String topic;
    private Object body;
    private Date createAt;
    private Map<String, String> headers;

    private Message(){
        createAt = new Date();
        headers = new HashMap<>();
    }

    public String getTopic() {
        return topic;
    }

    public Object getBody() {
        return body;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public String toString() {
        return "Message [topic=" + topic + ", body=" + body + ", createAt=" + createAt + ", headers=" + headers + "]";
    }

    public static class Builder {
        private String topic;
        private Object body;
        private Map<String, String> headers;

        public Builder(String topic){
            this.topic = topic;
            this.headers = new HashMap<>();
        }

        public Builder addBody(Object body){
            this.body = body;
            return this;
        }

        public Builder addHeader(String name, String value){
            this.headers.put(name, value);
            return this;
        }

        public Message build(){
            Message message = new Message();
            message.topic = topic;
            message.body = body;
            message.headers.putAll(headers);
            return message;
        }
    }

}
