package in.koyya.broker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class SimpleBroker implements Broker {
    private Map<String, List<Consumer<Message>>> handlers;
    private ExecutorService service;

    public SimpleBroker(){
        handlers = new HashMap<>();
        service = Executors.newFixedThreadPool(5);
    }

    @Override
    public void register(Consumer<Message> handler, String topic) {
        if(!handlers.containsKey(topic))
            handlers.put(topic, new ArrayList<>());
        handlers.get(topic).add(handler);
    }

    @Override
    public void send(Message message) {
        List<Consumer<Message>> handlers = this.handlers.get(message.getTopic());
        for(Consumer<Message> handler: handlers)
            service.submit(()->handler.accept(message));
    }
}