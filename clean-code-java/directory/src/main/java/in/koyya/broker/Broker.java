package in.koyya.broker;

import java.util.function.Consumer;

public interface Broker {
    public void register(Consumer<Message> handler, String topic);
    public void send(Message message);
}
