package in.koyya.dir.service;

import java.util.List;
import java.util.Optional;

import in.koyya.dir.api.Directory;
import in.koyya.dir.api.Employee;
import in.koyya.dir.api.exceptions.EmployeeNotFoundException;
import in.koyya.dir.api.exceptions.InvalidEmployeeException;

public class SimpleDirectory implements Directory {
    private int index;
    private Storage storage;

    public SimpleDirectory(Storage storage) {
        this.storage = storage;
    }

    @Override
    public Employee add(Employee emp) throws InvalidEmployeeException {
        if (emp.getName() == null || emp.getName().trim().length() < 3)
            throw new InvalidEmployeeException();
        index++;
        emp.setId(index);
        return storage.save(emp);
    }

    @Override
    public Optional<Employee> find(int eid) {
        return storage.read(eid);
    }

    @Override
    public List<Employee> search(String name) {
        return storage.read(name);
    }
}
