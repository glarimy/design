package in.koyya.dir.service;

import java.util.List;
import java.util.Optional;

import in.koyya.dir.api.Employee;

public interface Storage {
    public Employee save(Employee emp);
    public Optional<Employee> read(int eid);
    public List<Employee> read(String name);
}
