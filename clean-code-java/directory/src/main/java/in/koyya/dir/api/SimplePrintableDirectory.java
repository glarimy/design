package in.koyya.dir.api;

import java.util.List;
import java.util.Optional;

import in.koyya.dir.api.exceptions.EmployeeNotFoundException;
import in.koyya.dir.api.exceptions.InvalidEmployeeException;

public class SimplePrintableDirectory implements PrintableDirectory {
    private Directory target;

    public SimplePrintableDirectory(Directory target){
        this.target = target;
    }
    @Override
    public Employee add(Employee emp) throws InvalidEmployeeException {
        return target.add(emp);
    }

    @Override
    public Optional<Employee>  find(int eid)  {
        return target.find(eid);
    }

    @Override
    public List<Employee> search(String name) {
        return target.search(name);
    }

    @Override
    public void print(int eid) throws EmployeeNotFoundException {
        Optional<Employee>  emp = target.find(eid);
        if(emp.isPresent())
            System.out.println(emp.get());
    }
}
