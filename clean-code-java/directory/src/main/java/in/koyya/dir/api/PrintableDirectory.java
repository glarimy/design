package in.koyya.dir.api;

import in.koyya.dir.api.exceptions.EmployeeNotFoundException;

public interface PrintableDirectory extends Directory {
    public void print(int eid) throws EmployeeNotFoundException;
}
