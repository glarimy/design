package in.koyya.dir.api;

import java.util.List;
import java.util.Optional;

import in.koyya.dir.api.exceptions.EmployeeNotFoundException;
import in.koyya.dir.api.exceptions.InvalidEmployeeException;

/**
 * Directory maintains a list of employees.
 */
public interface Directory {
    /**
     * Adds a new employee into the system and assigns a unique employee ID to it.
     * 
     * @param emp with name, phone and email
     * @return employee object with generated employee id
     * @throws InvalidEmployeeException if employee name is less than three
     *                                  characters
     */
    public Employee add(Employee emp) throws InvalidEmployeeException;

    /**
     * Finds an employee with the given employee ID
     * 
     * @param eid a valid employee ID
     * @return the employee with the given employee ID, if available
     */
    public Optional<Employee> find(int eid);

    /**
     * Searches for all employees with name that consists of supplied name part.
     * 
     * @param name the part of the name by which employees are to be searched for
     * @return List of zero to any number of employees with the matching name.
     */
    public List<Employee> search(String name);
}
