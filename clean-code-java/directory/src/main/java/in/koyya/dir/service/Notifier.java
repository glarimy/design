package in.koyya.dir.service;

import java.util.List;
import java.util.Optional;

import in.koyya.broker.Broker;
import in.koyya.broker.Message;
import in.koyya.dir.api.Directory;
import in.koyya.dir.api.Employee;
import in.koyya.dir.api.exceptions.EmployeeNotFoundException;
import in.koyya.dir.api.exceptions.InvalidEmployeeException;

public class Notifier implements Directory {
    private Directory target;
    private Broker broker;

    public Notifier(Directory target, Broker broker) {
        this.target = target;
        this.broker = broker;
    }

    @Override
    public Employee add(Employee emp) throws InvalidEmployeeException {
        Employee e = target.add(emp);
        Message message = new Message.Builder("in.koyya.dir.add").addBody(emp).build();
        broker.send(message);
        return e;
    }

    @Override
    public Optional<Employee>  find(int eid)  {
        Optional<Employee>  e = target.find(eid);
        return e;
    }

    @Override
    public List<Employee> search(String name) {
        List<Employee> results = target.search(name);
        return results;
    }
}
