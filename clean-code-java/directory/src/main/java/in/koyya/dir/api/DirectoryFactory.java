package in.koyya.dir.api;

import java.io.FileReader;
import java.util.Properties;

import in.koyya.broker.Broker;
import in.koyya.broker.SimpleBroker;
import in.koyya.dir.service.Auditor;
import in.koyya.dir.service.InMemoryStorage;
import in.koyya.dir.service.Notifier;
import in.koyya.dir.service.SimpleDirectory;
import in.koyya.dir.service.Storage;

public class DirectoryFactory implements Factory {
    private Properties config;
    private boolean withAudit;
    private boolean withNotifications;

    public DirectoryFactory() throws RuntimeException {
        try {
            config = new Properties();
            config.load(new FileReader("config.properties"));
            withNotifications = Boolean.parseBoolean(config.getProperty("withNotifications"));
            withAudit = Boolean.parseBoolean(config.getProperty("withAudit"));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public Object getDirectory() {
        Storage storage = new InMemoryStorage();
        Directory dir = new SimpleDirectory(storage);
        if (withAudit)
            dir = new Auditor(dir);
        if (withNotifications) {
            Broker broker = new SimpleBroker();
            broker.register(message->System.out.println("Email: " + message), "in.koyya.dir.add");
            broker.register(message->System.out.println("SMS: " + message), "in.koyya.dir.add");
            dir = new Notifier(dir, broker);
        }
        return dir;
    }
}