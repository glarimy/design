package in.koyya.dir.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import in.koyya.dir.api.Directory;
import in.koyya.dir.api.Employee;
import in.koyya.dir.api.exceptions.EmployeeNotFoundException;
import in.koyya.dir.api.exceptions.InvalidEmployeeException;

public class Auditor implements Directory {
    private Directory target;

    public Auditor(Directory target) {
        this.target = target;
    }

    @Override
    public Employee add(Employee emp) throws InvalidEmployeeException {
        System.out.println(new Date() + " - Auditor: adding new employee");
        try {
            Employee e = target.add(emp);
            System.out.println(new Date() + " - Auditor: added new employee with ID: " + e.getId());
            return e;
        } catch (InvalidEmployeeException ex) {
            System.out.println(new Date() + " - Auditor: failed to add new employee");
            throw ex;
        }
    }

    @Override
    public Optional<Employee> find(int eid) {
        System.out.println(new Date() + " - Auditor: find employee with ID: " + eid);
        Optional<Employee> e = target.find(eid);
        if (e.isPresent())
            System.out.println(new Date() + " - Auditor: found employee with ID: " + eid);
        else
            System.out.println(new Date() + " - Auditor: faild to find found employee with ID: " + eid);
        return e;
    }

    @Override
    public List<Employee> search(String name) {
        System.out.println(new Date() + " - Auditor: searching employees with name: " + name);
        List<Employee> results = target.search(name);
        System.out.println(new Date() + " - Auditor: found employees with name: " + name);
        return results;
    }
}
