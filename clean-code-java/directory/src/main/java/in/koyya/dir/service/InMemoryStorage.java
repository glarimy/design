package in.koyya.dir.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import in.koyya.dir.api.Employee;

public class InMemoryStorage implements Storage {
    private Map<Integer, Employee> entries;

    public InMemoryStorage(){
        entries = new HashMap<>();
    }

    @Override
    public Employee save(Employee emp) {
        entries.put(emp.getId(), emp);
        return emp;
    }

    @Override
    public Optional<Employee> read(int eid) {
        Employee e = entries.get(eid);
        if(e == null)
            return Optional.empty();
        return Optional.of(e);
    }

    @Override
    public List<Employee> read(String name) {
        return entries.values()
                .stream()
                .filter(e -> e.getName().toUpperCase().contains(name.toUpperCase()))
                .collect(Collectors.toList());
    }
}
