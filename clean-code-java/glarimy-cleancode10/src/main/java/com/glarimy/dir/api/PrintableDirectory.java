package com.glarimy.dir.api;

import java.util.List;
import java.util.function.Predicate;

import com.glarimy.dir.api.exceptions.DirectoryException;
import com.glarimy.dir.api.exceptions.DuplicateUserException;
import com.glarimy.dir.api.exceptions.InvalidUserException;
import com.glarimy.dir.api.exceptions.UserNotFoundException;
import com.glarimy.dir.domain.User;

public class PrintableDirectory implements Directory {
	private Directory target;

	public PrintableDirectory(Directory target) {
		this.target = target;
	}

	@Override
	public User add(User user) throws DuplicateUserException, InvalidUserException, DirectoryException {
		return target.add(user);
	}

	@Override
	public User findByPhone(long phone) throws UserNotFoundException, DirectoryException {
		return target.findByPhone(phone);
	}

	@Override
	public List<User> searchByName(String name) throws DirectoryException {
		return target.searchByName(name);
	}

	@Override
	public List<User> searchBy(Predicate<User> condition) throws DirectoryException {
		return target.searchBy(condition);
	}

	public void printByPhone(long phone) throws UserNotFoundException, DirectoryException {
		User user = target.findByPhone(phone);
		System.out.println("Found: " + user.getName());
	}

}
