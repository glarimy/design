package com.glarimy.dir.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.glarimy.dir.domain.User;

public class InMemoryStorage implements Storage {
	private Map<Long, User> users;
	private static InMemoryStorage INSTANCE;

	private InMemoryStorage() {
		users = new HashMap<Long, User>();
	}

	public static synchronized InMemoryStorage getInstance() {
		if (INSTANCE == null)
			INSTANCE = new InMemoryStorage();
		return INSTANCE;
	}

	@Override
	public User save(User user) {
		users.put(user.getPhone(), user);
		return user;
	}

	@Override
	public User read(long id) {
		return users.get(id);
	}

	@Override
	public List<User> read(String name) {
		return users.values().stream().filter(u -> u.getName().equalsIgnoreCase(name)).collect(Collectors.toList());
	}

	@Override
	public List<User> read() {
		return new ArrayList<User>(users.values());
	}

}
