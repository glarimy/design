package com.glarimy.dir.service;

@Aspect
public class Logger {
    @Before("execution(* com.glarimy.dir.service.*.*(..))")
    public void printBeforeMessage(){
        debug("entering into the method");
    }

    @AfterReturning("execution(* com.glarimy.dir.service.*.*(..))")
    public void printBeforeMessage(){
        debug("existing the method");
    }
    
}
