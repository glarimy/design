package com.glarimy.dir.service;

import java.util.List;
import java.util.function.Predicate;

import com.glarimy.dir.api.Directory;
import com.glarimy.dir.api.exceptions.DirectoryException;
import com.glarimy.dir.api.exceptions.DuplicateUserException;
import com.glarimy.dir.api.exceptions.InvalidUserException;
import com.glarimy.dir.api.exceptions.UserNotFoundException;
import com.glarimy.dir.domain.User;

public class Validator implements Directory {
	private Directory target;

	public Validator(Directory target) {
		this.target = target;
	}

	@Override
	public User add(User user) throws DuplicateUserException, InvalidUserException, DirectoryException {
		if (user == null || user.getPhone() < 0)
			throw new InvalidUserException();
		User result = target.add(user);
		return result;
	}

	@Override
	public User findByPhone(long phone) throws UserNotFoundException, DirectoryException {
		return target.findByPhone(phone);
	}

	@Override
	public List<User> searchByName(String name) throws DirectoryException {
		return target.searchByName(name);
	}

	@Override
	public List<User> searchBy(Predicate<User> condition) throws DirectoryException {
		return target.searchBy(condition);
	}

}
