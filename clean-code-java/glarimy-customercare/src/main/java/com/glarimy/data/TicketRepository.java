package com.glarimy.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.glarimy.domain.Customer;
import com.glarimy.domain.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {
	public List<Ticket> findByCustomer(Customer customer);
}
