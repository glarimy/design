package com.glarimy.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.glarimy.app.Issue;
import com.glarimy.data.CustomerRepository;
import com.glarimy.data.EmployeeRepository;
import com.glarimy.data.TicketRepository;
import com.glarimy.domain.Customer;
import com.glarimy.domain.Employee;
import com.glarimy.domain.NotFoundException;
import com.glarimy.domain.Ticket;

@Service
@EnableTransactionManagement
public class SimpleTicketService implements TicketService {
	@Autowired
	private TicketRepository ticketRepo;
	@Autowired
	private EmployeeRepository empRepo;
	@Autowired
	private CustomerRepository custRepo;

	@Override
	public Ticket raise(Issue issue) {
		Optional<Customer> result = custRepo.findById(issue.getPhone());
		if (result.isPresent()) {
			Ticket ticket = new Ticket();
			ticket.setIssue(issue.getDescription());
			ticket.setSeverity(issue.getSeverity());
			ticket.setPriority(issue.getPriority());
			ticket.setStatus("New");
			ticket.setCreationTime(new Date());
			ticket.setCustomer(result.get());
			return ticketRepo.save(ticket);
		}
		throw new NotFoundException();
	}

	@Override
	public Ticket findTicket(int tid) {
		Optional<Ticket> result = ticketRepo.findById(tid);
		if (result.isPresent())
			return result.get();
		throw new NotFoundException();
	}

	@Override
	public List<Ticket> findCustomerTickets(long phone) {
		Optional<Customer> result = custRepo.findById(phone);
		if (result.isPresent()) {
			return ticketRepo.findByCustomer(result.get());
		}
		throw new NotFoundException();

	}

	@Override
	@Transactional
	public Ticket assignOwner(int tid, int eid) {
		Optional<Ticket> result = ticketRepo.findById(tid);
		if (result.isPresent()) {
			Ticket ticket = result.get();
			Optional<Employee> record = empRepo.findById(eid);
			if (record.isPresent()) {
				Employee employe = record.get();
				ticket.assign(employe);
				return ticket;
			}
			throw new NotFoundException();
		}
		throw new NotFoundException();
	}

	@Override
	@Transactional
	public Ticket close(int tid, String note) {
		Optional<Ticket> result = ticketRepo.findById(tid);
		if (result.isPresent()) {
			Ticket ticket = result.get();
			ticket.close(note);
			return ticket;
		}
		throw new NotFoundException();

	}

	@Override
	@Transactional
	public Ticket accept(int tid, String note) {
		Optional<Ticket> result = ticketRepo.findById(tid);
		if (result.isPresent()) {
			Ticket ticket = result.get();
			ticket.accept(note);
			return ticket;
		}
		throw new NotFoundException();
	}

}