package com.glarimy.service;

import java.util.List;

import com.glarimy.app.Issue;
import com.glarimy.domain.Ticket;

public interface TicketService {
	public Ticket raise(Issue issue);

	public Ticket findTicket(int tid);

	public List<Ticket> findCustomerTickets(long phone);

	public Ticket assignOwner(int tid, int eid);

	public Ticket accept(int tid, String note);

	public Ticket close(int tid, String note);
}
