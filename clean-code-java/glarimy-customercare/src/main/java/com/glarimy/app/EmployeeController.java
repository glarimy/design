package com.glarimy.app;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.glarimy.data.EmployeeRepository;
import com.glarimy.domain.Employee;

@RestController
public class EmployeeController {
	private Logger logger = LoggerFactory.getLogger(EmployeeController.class);
	@Autowired
	private EmployeeRepository repo;

	@PostMapping("/employee")
	public ResponseEntity<Employee> register(@RequestBody Employee employee) {
		logger.debug("Registering " + employee);
		employee = repo.save(employee);
		logger.debug("Registered " + employee);
		return ResponseEntity.status(HttpStatus.CREATED).body(employee);
	}

	@GetMapping("/employee/{phone}")
	public ResponseEntity<Employee> find(@PathVariable("id") int id) {
		logger.debug("Finding employee with phone " + id);
		Optional<Employee> result = repo.findById(id);
		if (result.isPresent()) {
			Employee employee = result.get();
			logger.debug("Found " + employee);
			return ResponseEntity.status(HttpStatus.OK).body(result.get());
		}
		logger.debug("Employee with phone " + id + " is not found");
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
}