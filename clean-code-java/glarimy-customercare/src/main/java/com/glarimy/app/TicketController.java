package com.glarimy.app;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.glarimy.domain.NotFoundException;
import com.glarimy.domain.Ticket;
import com.glarimy.service.TicketService;

@RestController
public class TicketController {
	@Autowired
	private TicketService service;

	@PostMapping("/ticket")
	public ResponseEntity<Ticket> raise(@RequestBody Issue issue) {
		try {
			Ticket ticket = service.raise(issue);
			return ResponseEntity.status(HttpStatus.CREATED).body(ticket);
		} catch (NotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@GetMapping("/ticket/{tid}")
	public ResponseEntity<Ticket> findTicket(@PathVariable("tid") int tid) {
		try {
			Ticket ticket = service.findTicket(tid);
			return ResponseEntity.status(HttpStatus.OK).body(ticket);
		} catch (NotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@GetMapping("/ticket")
	public ResponseEntity<List<Ticket>> findCustomerTickets(@RequestParam("phone") long phone) {
		try {
			List<Ticket> tickets = service.findCustomerTickets(phone);
			return ResponseEntity.status(HttpStatus.OK).body(tickets);
		} catch (NotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@PostMapping("/ticket/{tid}/owner/{eid}")
	public ResponseEntity<Ticket> assignOwner(@PathVariable("tid") int tid, @PathVariable("eid") int eid) {
		try {
			Ticket ticket = service.assignOwner(tid, eid);
			return ResponseEntity.status(HttpStatus.OK).body(ticket);
		} catch (NotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@PostMapping("/ticket/{tid}/status")
	public ResponseEntity<Ticket> update(@PathVariable("tid") int tid, @RequestBody StatusRequest request) {
		Ticket ticket;
		try {
			if (request.getStatus().equals("Closed")) {
				ticket = service.close(tid, request.getNote());
				return ResponseEntity.status(HttpStatus.OK).body(ticket);
			} else if (request.getStatus().equals("Accepted")) {
				ticket = service.accept(tid, request.getNote());
				return ResponseEntity.status(HttpStatus.OK).body(ticket);
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
		} catch (NotFoundException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
}
