package com.glarimy.app;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.glarimy.data.CustomerRepository;
import com.glarimy.domain.Customer;

@RestController
public class CustomerController {
	private Logger logger = LoggerFactory.getLogger(CustomerController.class);
	@Autowired
	private CustomerRepository repo;

	@PostMapping("/customer")
	public ResponseEntity<Customer> register(@RequestBody Customer customer) {
		logger.debug("Registering " + customer);
		customer = repo.save(customer);
		logger.debug("Registered " + customer);
		return ResponseEntity.status(HttpStatus.CREATED).body(customer);
	}

	@GetMapping("/customer/{phone}")
	public ResponseEntity<Customer> find(@PathVariable("phone") long phone) {
		logger.debug("Finding customer with phone " + phone);
		Optional<Customer> result = repo.findById(phone);
		if (result.isPresent()) {
			Customer customer = result.get();
			logger.debug("Found " + customer);
			return ResponseEntity.status(HttpStatus.OK).body(result.get());
		}
		logger.debug("Customer with phone " + phone + " is not found");
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
}