package com.glarimy.app;

public class StatusRequest {
	private int tid;
	private String note;
	private String status;

	public StatusRequest() {

	}

	public StatusRequest(int tid, String note, String status) {
		this.tid = tid;
		this.note = note;
		this.status = status;
	}

	public int getTid() {
		return tid;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}