package com.glarimy.app;

public class Issue {
	private long phone;
	private String description;
	private int severity;
	private int priority;

	public Issue() {

	}

	public Issue(long phone, String description, int severity, int priority) {
		super();
		this.phone = phone;
		this.description = description;
		this.severity = severity;
		this.priority = priority;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

}
