package com.glarimy.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Ticket {
	@Id
	@GeneratedValue
	private int tid;
	private String issue;
	private int priority;
	private int severity;
	@ManyToOne
	private Customer customer;
	@ManyToOne
	private Employee owner;
	@OneToMany(cascade = CascadeType.ALL)
	private List<Note> updates;
	private String status;
	private Date creationTime;
	private Date lastUpdate;

	public Ticket() {

	}

	public Ticket(int tid, Date date, String status, String issue, int priority, int severity, Employee owner,
			Customer customer, List<Note> updates) {
		super();
		this.tid = tid;
		this.status = status;
		this.issue = issue;
		this.priority = priority;
		this.severity = severity;
		this.owner = owner;
		this.customer = customer;
		this.updates = updates;
		this.creationTime = new Date();
	}

	public void assign(Employee owner) {
		this.owner = owner;
		this.status = "Active";
		Date now = new Date();
		this.setLastUpdate(now);
		Note update = new Note();
		update.setNewState("Active");
		update.setNote("Assigned owner");
		update.setCreationTime(now);
		this.updates.add(update);
	}

	public void close(String note) {
		this.status = "Closed";
		Date now = new Date();
		this.setLastUpdate(now);
		Note update = new Note();
		update.setNewState("Closed");
		update.setNote(note);
		update.setCreationTime(now);
		this.updates.add(update);
	}

	public void accept(String note) {
		this.status = "Accepted";
		Date now = new Date();
		this.setLastUpdate(now);
		Note update = new Note();
		update.setNewState("Accepted");
		update.setNote(note);
		update.setCreationTime(now);
		this.updates.add(update);
	}

	public int getTid() {
		return tid;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIssue() {
		return issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public Employee getOwner() {
		return owner;
	}

	public void setOwner(Employee owner) {
		this.owner = owner;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<Note> getUpdates() {
		return updates;
	}

	public void setUpdates(List<Note> updates) {
		this.updates = updates;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public String toString() {
		return "Ticket [tid=" + tid + ", issue=" + issue + ", priority=" + priority + ", severity=" + severity
				+ ", customer=" + customer + ", owner=" + owner + ", updates=" + updates + ", status=" + status
				+ ", creationTime=" + creationTime + ", lastUpdate=" + lastUpdate + "]";
	}
}
