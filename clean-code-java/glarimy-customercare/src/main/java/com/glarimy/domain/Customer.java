package com.glarimy.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Customer {
	@Id
	private long phone;
	private String name;
	private String email;
	private String city;

	public Customer() {

	}

	public Customer(long phone, String name, String email, String city) {
		super();
		this.phone = phone;
		this.name = name;
		this.email = email;
		this.city = city;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "Customer [phone=" + phone + ", name=" + name + ", email=" + email + ", city=" + city + "]";
	}

}
