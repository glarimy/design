package com.glarimy.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Note {
	@Id
	@GeneratedValue
	private int nid;
	private String note;
	private String newState;
	private Date creationTime;

	public Note() {

	}

	public Note(String note, String newState) {
		this.note = note;
		this.newState = newState;
		this.creationTime = new Date();
	}

	public int getNid() {
		return nid;
	}

	public void setNid(int nid) {
		this.nid = nid;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getNewState() {
		return newState;
	}

	public void setNewState(String newState) {
		this.newState = newState;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public String toString() {
		return "Note [nid=" + nid + ", note=" + note + ", newState=" + newState + ", creationTime=" + creationTime
				+ "]";
	}

}
