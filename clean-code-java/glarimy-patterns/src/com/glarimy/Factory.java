package com.glarimy;

import java.util.Map;

public class Factory {
	public static Calculator get(Map<String, Object> config) {
		Calculator target = new SimpleCalculator();
		if (config.get("monitor").equals("enabled")) {
			target = new PerformanceProxy(target);
		}
		if (config.get("auditor").equals("enabled")) {
			target = new AuditProxy(target);
		}
		return target;
	}
}
