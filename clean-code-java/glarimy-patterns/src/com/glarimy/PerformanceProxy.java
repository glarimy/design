package com.glarimy;

import java.util.Date;

public class PerformanceProxy implements Calculator {
	private Calculator target;

	public PerformanceProxy(Calculator target) {
		this.target = target;
	}

	@Override
	public int add(int first, int second) {
		Date start = new Date();
		int sum = target.add(first, second);
		Date end = new Date();
		long duration = end.getTime() - start.getTime();
		System.out.println("Add Duration: " + duration + "ms");
		return sum;
	}

	@Override
	public int multiply(int first, int second) {
		Date start = new Date();
		int product = target.multiply(first, second);
		Date end = new Date();
		long duration = end.getTime() - start.getTime();
		System.out.println("Multiply Duration: " + duration + "ms");
		return product;
	}
}
