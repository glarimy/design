package com.glarimy;

public class AuditProxy implements Calculator {
	private Calculator target;

	public AuditProxy(Calculator target) {
		this.target = target;
	}

	@Override
	public int add(int first, int second) {
		System.out.println("Audit: adding " + first + " and " + second);
		int sum = target.add(first, second);
		System.out.println("Audit: added " + first + " and " + second);
		return sum;
	}

	@Override
	public int multiply(int first, int second) {
		System.out.println("Audit: mutliplying " + first + " and " + second);
		int product = target.multiply(first, second);
		System.out.println("Audit: multiplied " + first + " and " + second);
		return product;
	}
}
