package com.glarimy;

public interface Calculator {
	public int add(int first, int second);

	public int multiply(int first, int second);
}
