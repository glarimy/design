package com.glarimy;

import java.util.HashMap;
import java.util.Map;

public class Client {
	public static void main(String[] args) {
		Map<String, Object> config = new HashMap<String, Object>();
		config.put("monitor", "enable");
		config.put("auditor", "enable");
		Calculator calc = Factory.get(config);
		int sum = calc.add(1, 2);
		System.out.println("SUM: " + sum);
		int product = calc.multiply(1, 2);
		System.out.println("Product: " + product);
	}
}
