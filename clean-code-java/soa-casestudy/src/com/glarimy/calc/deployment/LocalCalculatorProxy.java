package com.glarimy.calc.deployment;

import com.glarimy.calc.api.Calculator;
import com.glarimy.calc.service.SimpleCalculator;

public class LocalCalculatorProxy implements Calculator {
	private Calculator target;

	public LocalCalculatorProxy() {
		this.target = new SimpleCalculator();
	}

	@Override
	public int add(int first, int second) {
		System.out.println("Connecting to the local calculator");
		int sum = target.add(first, second);
		System.out.println("Disconnecting from the local calculator");
		return sum;
	}

	@Override
	public int multiply(int first, int second) {
		System.out.println("Connecting to the local calculator");
		int product = target.multiply(first, second);
		System.out.println("Disconnecting from the local calculator");
		return product;

	}
}
