package com.glarimy.calc.deployment;

import com.glarimy.calc.api.Calculator;
import com.glarimy.cloud.ServiceRegistry;
import com.glarimy.cloud.SimpleServiceRegistry;

public class ServiceDeployer {
	private ServiceRegistry registry = SimpleServiceRegistry.getInstance();

	public void deploy() {
		registry.register(Calculator.class.getName(), LocalCalculatorProxy.class, true);
		registry.register(Calculator.class.getName(), RemoteCalculatorProxy.class, false);
	}

	public void undeploy() {
		registry.unregister(Calculator.class.getName(), true);
	}
}
