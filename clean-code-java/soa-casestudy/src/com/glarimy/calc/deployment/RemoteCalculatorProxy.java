package com.glarimy.calc.deployment;

import com.glarimy.calc.api.Calculator;

public class RemoteCalculatorProxy implements Calculator {

	@Override
	public int add(int first, int second) {
		System.out.println("Connecting to the remote calculator");
		int sum = first + second; // TBD: real network call
		System.out.println("Disconnecting from the remote calculator");
		return sum;
	}

	@Override
	public int multiply(int first, int second) {
		System.out.println("Connecting to the remote calculator");
		int product = first * second; // TBD: real network call
		System.out.println("Disconnecting from the remote calculator");
		return product;

	}
}
