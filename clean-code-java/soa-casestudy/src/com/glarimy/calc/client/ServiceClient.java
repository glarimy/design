package com.glarimy.calc.client;

import com.glarimy.calc.api.Calculator;
import com.glarimy.cloud.ServiceLocator;
import com.glarimy.cloud.SimpleServiceLocator;

public class ServiceClient {
	public void use() {
		ServiceLocator locator = new SimpleServiceLocator();
		Calculator calc = (Calculator) locator.lookup(Calculator.class.getName());
		int sum = calc.add(10, 20);
		int product = calc.multiply(1, 5);
		System.out.println("SUM: " + sum + " and PRODUCT: " + product);
	}
}
