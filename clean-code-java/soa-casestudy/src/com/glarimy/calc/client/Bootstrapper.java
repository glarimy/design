package com.glarimy.calc.client;

import com.glarimy.calc.deployment.ServiceDeployer;

public class Bootstrapper {
	public static void main(String[] args) {
		ServiceDeployer deployer = new ServiceDeployer();
		ServiceClient client = new ServiceClient();

		deployer.deploy(); // this happens on the provider side
		client.use();

		deployer.undeploy(); // this happens on the provider side
		client.use();
	}
}
