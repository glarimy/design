package com.glarimy.calc.service;

import com.glarimy.calc.api.Calculator;

public class SimpleCalculator implements Calculator {
	@Override
	public int add(int first, int second) {
		return first + second;
	}

	@Override
	public int multiply(int first, int second) {
		return first * second;
	}
}
