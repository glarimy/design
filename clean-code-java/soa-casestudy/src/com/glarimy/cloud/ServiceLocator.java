package com.glarimy.cloud;

public interface ServiceLocator {
	public Object lookup(String service);
}
