package com.glarimy.cloud;

public interface ServiceRegistry {
	@SuppressWarnings("rawtypes")
	public void register(String service, Class proxy, boolean local);

	public void unregister(String service, boolean local);

	@SuppressWarnings("rawtypes")
	public Class lookup(String service);

}
