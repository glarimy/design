package com.glarimy.cloud;

public class SimpleServiceLocator implements ServiceLocator {
	private ServiceRegistry registry;

	public SimpleServiceLocator() {
		registry = SimpleServiceRegistry.getInstance();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object lookup(String service) {
		Class claz = registry.lookup(service);
		try {
			return claz.newInstance();
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}
}
