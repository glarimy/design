package com.glarimy.cloud;

import java.util.HashMap;
import java.util.Map;

public class SimpleServiceRegistry implements ServiceRegistry {
	@SuppressWarnings("rawtypes")
	private Map<String, Class> locals;
	
	@SuppressWarnings("rawtypes")
	private Map<String, Class> remotes;
	
	private static SimpleServiceRegistry INSTANCE;

	@SuppressWarnings("rawtypes")
	private SimpleServiceRegistry() {
		locals = new HashMap<String, Class>();
		remotes = new HashMap<String, Class>();
	}

	public static synchronized SimpleServiceRegistry getInstance() {
		if (INSTANCE == null)
			INSTANCE = new SimpleServiceRegistry();
		return INSTANCE;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void register(String service, Class proxy, boolean local) {
		if (local)
			locals.put(service, proxy);
		else
			remotes.put(service, proxy);
	}

	@Override
	public void unregister(String service, boolean local) {
		if (local)
			locals.remove(service);
		else
			remotes.remove(service);

	}

	@SuppressWarnings("rawtypes")
	@Override
	public Class lookup(String service) {
		Class claz = locals.get(service);
		if (claz == null)
			claz = remotes.get(service);
		if (claz == null)
			throw new RuntimeException();
		return claz;
	}

}
