package temp;

public class Counter implements Runnable {
	private int timeout;

	public Counter(int timeout) {
		this.timeout = timeout;
	}

	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println(Thread.currentThread().getName() + ": " + i);
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
			}
		}
	}
}
