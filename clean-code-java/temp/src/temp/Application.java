package temp;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Application {
	public static void main(String[] args) {		
		Executor executor = Executors.newFixedThreadPool(2);
		executor.execute(new Counter(1000));
		executor.execute(new Counter(700));
		executor.execute(new Counter(200));
		executor.execute(new Counter(1200));
	}

}
