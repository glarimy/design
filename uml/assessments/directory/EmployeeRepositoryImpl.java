package assessments.directory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmployeeRepositoryImpl implements EmployeeRepository {
    private Map<Integer, Employee> employees = new HashMap<>();

    @Override
    public void save(Employee e) {
        employees.put(e.getEid(), e);
    }

    @Override
    public Employee find(int eid) {
        return employees.get(eid);
    }

    @Override
    public List<Integer> list() {
        return new ArrayList<>(employees.keySet());
    }
}
