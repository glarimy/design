package assessments.directory;

import java.util.List;

public class EmployeeDirectoryImpl implements EmployeeDirectory {
    private EmployeeRepository repo;
    
    public EmployeeDirectoryImpl(EmployeeRepository repo) {
        this.repo = repo;
    }

    @Override
    public void add(Employee e) {
        repo.save(e);
    }

    @Override
    public Employee findEmployee(int eid) {
        return repo.find(eid);
    }

    @Override
    public List<Integer> list() {
        return repo.list();
    }
    
}
