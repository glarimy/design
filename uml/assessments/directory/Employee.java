package assessments.directory;

public class Employee {
    private int eid;
    private String name;
    private Email email;

    public Employee(int eid, String name, Email email) {
        this.eid = eid;
        this.name = name;
        this.email = email;
    }
    public int getEid() {
        return eid;
    }
    public String getName() {
        return name;
    }
    public Email getEmail() {
        return email;
    }
}
