package assessments.directory;

public class Nam {
    private String first;
    private String last;

    public Nam(String first, String last) {
        // TBD: validate
        this.first = first;
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public String getLast() {
        return last;
    }

    public String getFullName() {
        return first + " " + last;
    }
}
