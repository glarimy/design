package assessments.directory;

import java.util.List;

public interface EmployeeRepository {
    public void save(Employee e);
    public List<Integer> list();
    public Employee find(int eid);
}
