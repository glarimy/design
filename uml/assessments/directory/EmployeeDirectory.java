package assessments.directory;

import java.util.List;

public interface EmployeeDirectory {
    public void add(Employee e);
    public Employee findEmployee(int eid);
    public List<Integer> list();
}
