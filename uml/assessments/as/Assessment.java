package assessments.as;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import assessments.qb.QuizQuestion;
//comment

public class Assessment {
    private int aid;
    private List<Integer> employees;
    private List<QuizQuestion> questions;
    private String subject;

    public Assessment(String subject, List<Integer> employees, List<QuizQuestion> questions) {
        this.aid = ThreadLocalRandom.current().nextInt(0, 100);
        this.subject = subject;
        this.employees = employees;
        this.questions = questions;
    }

    public int getAid() {
        return aid;
    }

    public String getSubject() {
        return subject;
    }

    public List<Integer> getEmployees() {
        return employees;
    }

    public List<QuizQuestion> getQuestions() {
        return questions;
    }  
    
    public void display(){
        System.out.println("Assessment ID: " + aid);
        System.out.println("Subject: " + subject);
        System.out.println("Candidates: " + employees);
        for(QuizQuestion q: questions){
            q.display();
        }
    }
}
