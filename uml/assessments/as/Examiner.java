package assessments.as;

import java.util.List;

import assessments.qb.Answer;

public interface Examiner {
    public int create(String subject, List<Integer> eids);
    public List<Assessment> assessments();
    public Assessment fetch(int aid);
    public int evaluate(int aid, int eid, List<Answer> answers);
    public List<Result> resultsForAssessment(int aid);
    public List<Result> resultsForEmployee(int eid);
}
