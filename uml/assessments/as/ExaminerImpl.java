package assessments.as;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import assessments.qb.Answer;
import assessments.qb.QuestionBank;
import assessments.qb.QuizQuestion;

public class ExaminerImpl implements Examiner {
    private Map<Integer, Assessment> assessments = new HashMap<>();
    private List<Result> results = new ArrayList<>();
    private QuestionBank qb;

    public ExaminerImpl(QuestionBank qb) {
        this.qb = qb;
    }

    @Override
    public int create(String subject, List<Integer> eids) {
        List<QuizQuestion> questions = this.qb.fetch(subject, 5);
        Assessment assessment = new Assessment(subject, eids, questions);
        assessments.put(assessment.getAid(), assessment);
        return assessment.getAid();
    }

    @Override
    public List<Assessment> assessments(){
        return new ArrayList<>(assessments.values());
    }

    @Override
    public Assessment fetch(int aid) {
        return assessments.get(aid);
    }

    @Override
    public int evaluate(int aid, int eid, List<Answer> answers) {
        int score = qb.evaluate(answers);
        Result result = new Result(aid, eid, score);
        results.add(result);
        return score;
    }

    @Override
    public List<Result> resultsForAssessment(int aid) {
        return results.stream().filter(result -> result.getAid() == aid).collect(Collectors.toList());
    }   

    @Override
    public List<Result> resultsForEmployee(int eid) {
        return results.stream().filter(result -> result.getEid() == eid).collect(Collectors.toList());
    }   
}