package assessments.qb;

public class QuizQuestion {
    private int qid;
    private String description;
    private String[] options;

    public QuizQuestion(int qid, String description, String[] options) {
        this.qid = qid;
        this.description = description;
        this.options = options;
    }

    public int getQid() {
        return qid;
    }

    public String getDescription() {
        return description;
    }

    public String[] getOptions() {
        return options;
    }

    public void display() {
        System.out.println("Question: " + description);
        System.out.println("A." + options[0]);
        System.out.println("B." + options[1]);
        System.out.println("C." + options[2]);
        System.out.println("D." + options[3]);
    }
}

