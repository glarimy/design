package assessments.qb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class QuestionRepositoryImpl implements QuestionRepository {
    private Map<Integer, Question> questions;
    private int index = 0;

    public QuestionRepositoryImpl() {
        this.questions = new HashMap<>();
    }

    @Override
    public void save(Question question) {
        question.setQid(index);
        questions.put(index, question);
        index++;
    }

    @Override
    public Question find(int qid) {
        return questions.get(qid);
    }

    @Override
    public List<Question> get(int count) {
        return this.questions.values().stream().filter(q -> q.getQid() < count).collect(Collectors.toList());
    }
}
