package assessments.qb;

import java.util.List;

public interface QuestionRepository {
    public void save(Question question);
    public List<Question> get(int count);
    public Question find(int qid);
}
