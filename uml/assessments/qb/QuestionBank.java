package assessments.qb;

import java.util.List;

public interface QuestionBank {
    public void add(Question question);
    public List<QuizQuestion> fetch(String subject, int count);
    public int evaluate(List<Answer> answers);
}
