package assessments.qb;

import java.util.List;
import java.util.stream.Collectors;

public class QuestionBankImpl implements QuestionBank{
    private QuestionRepository repo;
    public QuestionBankImpl(QuestionRepository repo){
        this.repo = repo;
    }

    @Override
    public void add(Question question) {
        repo.save(question);
    }

    @Override
    public List<QuizQuestion> fetch(String subject, int count) {
        List<Question> questions = repo.get(count);
        return questions.stream().map(q -> q.get()).collect(Collectors.toList());
    }
    @Override
    public int evaluate(List<Answer> answers) {
        int score = 0;
        for(Answer answer: answers){
            if(repo.find(answer.getQid()).isValid(answer.getChoice()))
                score++;
        }
        return score;
    }
}