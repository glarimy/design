package assessments;

import java.util.ArrayList;
import java.util.List;

import assessments.as.Assessment;
import assessments.as.Examiner;
import assessments.as.ExaminerImpl;
import assessments.as.Result;
import assessments.directory.Email;
import assessments.directory.Employee;
import assessments.directory.EmployeeDirectory;
import assessments.directory.EmployeeDirectoryImpl;
import assessments.directory.EmployeeRepositoryImpl;
import assessments.qb.Answer;
import assessments.qb.Question;
import assessments.qb.QuestionBank;
import assessments.qb.QuestionBankImpl;
import assessments.qb.QuestionRepositoryImpl;
import assessments.qb.QuizQuestion;

public class AssessmentSystem {
    public static void main(String[] args) {
        // Admin adds the employees
        EmployeeDirectory directory = new EmployeeDirectoryImpl(new EmployeeRepositoryImpl());
        directory.add(new Employee(1970, "Krishna", new Email("krishna@gmail.com")));
        directory.add(new Employee(1971, "Mohan", new Email("mohan@gmail.com")));
        directory.add(new Employee(1972, "Koyya", new Email("koyya@gmail.com")));

        // SME builds the question bank
        QuestionBank qb = new QuestionBankImpl(new QuestionRepositoryImpl());
        qb.add(new Question("1+1=?", new String[] { "1", "2", "3", "4" }, 2, "Maths"));
        qb.add(new Question("1+2=?", new String[] { "1", "2", "3", "4" }, 3, "Maths"));
        qb.add(new Question("1+3=?", new String[] { "1", "2", "3", "4" }, 4, "Maths"));
        qb.add(new Question("1+4=?", new String[] { "1", "2", "3", "5" }, 4, "Maths"));
        qb.add(new Question("1+5=?", new String[] { "1", "2", "3", "6" }, 4, "Maths"));
        qb.add(new Question("1+6=?", new String[] { "1", "2", "3", "7" }, 4, "Maths"));
        qb.add(new Question("1+7=?", new String[] { "1", "2", "3", "8" }, 4, "Maths"));
        qb.add(new Question("1+8=?", new String[] { "1", "2", "3", "9" }, 4, "Maths"));
        qb.add(new Question("1+9=?", new String[] { "1", "2", "3", "10" }, 4, "Maths"));

        // LnD schedules an assessment
        Examiner examiner = new ExaminerImpl(qb);
        int aid = examiner.create("Maths", directory.list());

        // Employee takes an assessment
        Assessment assessment = examiner.fetch(aid);
        System.out.println("ASSESSMENT");
        assessment.display();
        List<Answer> answers = new ArrayList<>();
        for (QuizQuestion q : assessment.getQuestions()) {
            answers.add(new Answer(q.getQid(), 4));
        }
        for (Integer eid : assessment.getEmployees()) {
            int score = examiner.evaluate(aid, eid, answers);
            System.out.println("Employee ID: " + eid + " - Score: " + score);
        }

        aid = examiner.create("Maths", directory.list());

        // Employee takes an assessment
        assessment = examiner.fetch(aid);
        System.out.println("ASSESSMENT");
        assessment.display();
        answers = new ArrayList<>();
        for (QuizQuestion q : assessment.getQuestions()) {
            answers.add(new Answer(q.getQid(), 3));
        }
        for (Integer eid : assessment.getEmployees()) {
            int score = examiner.evaluate(aid, eid, answers);
            System.out.println("Employee ID: " + eid + " - Score: " + score);
        }

        // Employee or Manager views scores of a employee
        System.out.println("REPORT FOR EMPLOYEE ID: 1972");
        List<Result> results = examiner.resultsForEmployee(1972);
        for(Result r: results){
            System.out.println("Assessment ID: " + r.getAid() + " Score: " + r.getScore());
        }

        // LnD or Manager views scores of an assessment
        System.out.println("REPORT FOR ASSESSMENT ID " + aid);
        results = examiner.resultsForAssessment(aid);
        for(Result r: results){
            System.out.println("Employee ID: " + r.getEid() + " Score: " + r.getScore());
        }

        // LnD or Manager or Employee views all assessments
        System.out.println("ALL ASSESSMENTS");
        List<Assessment> assessments = examiner.assessments();
        for (Assessment a : assessments) {
            System.out.println("Assessment ID: " + a.getAid());
            results = examiner.resultsForAssessment(aid);
            for(Result r: results){
                System.out.println("Employee ID: " + r.getEid() + " Score: " + r.getScore());
            }
        }

        // LnD or Manager or Employee views all assessments
        System.out.println("ALL EMPLOYEES");
        List<Integer> employees = directory.list();
        for (Integer e : employees) {
            System.out.println("Employee ID: " + e);
            results = examiner.resultsForEmployee(e);
            for(Result r: results){
                System.out.println("Assessment ID: " + r.getAid() + " Score: " + r.getScore());
            }

        }
    }
}
