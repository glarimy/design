package directory;

import directory.app.DirectoryController;
import directory.app.DirectoryFactory;
import directory.app.EmployeeDTO;
import directory.app.NewEmployee;
import directory.app.Response;
import directory.app.SimpleDirectoryFactory;

public class Client {
    public static void main(String[] args) {
        DirectoryFactory factory = new SimpleDirectoryFactory();
        DirectoryController ctrl = factory.getDirectory();
        NewEmployee emp = new NewEmployee();
        emp.firstName = "Krishna";
        emp.lastName = "Koyya";
        emp.phone = 9731423166L;
        emp.email = "krishnamoha.koyya@gmail.com";
        emp.permLocation = "Santhi Nivas";
        emp.permCity = "Tadepalligudem";
        emp.permPin = 534101;
        emp.currentLocation = "Pai Layout";
        emp.currentCity = "Bengaluru";
        emp.currentPin = 560016;
        Response response = ctrl.add(emp);
        if(response.success){
            EmployeeDTO dto = (EmployeeDTO)response.data;
            System.out.println("Employee ID: " + dto.eid);
            System.out.println("Name: " + dto.name);
            System.out.println("Address: " + dto.currentAddress);
        }
    }
}
