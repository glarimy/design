package directory.service;

import java.util.List;

import directory.domain.DirectoryException;
import directory.domain.DuplicateEmployeeException;
import directory.domain.Employee;
import directory.domain.EmployeeNotFoundException;
import directory.domain.InvalidEmployeeException;
import directory.domain.Name;
import directory.domain.Status;
import directory.domain.Storage;

public class DirectoryImpl implements Directory {
    private Storage storage;
    private int id;

    public DirectoryImpl(Storage storage) {
        this.storage = storage;
    }

    @Override
    public Employee add(Employee e) throws InvalidEmployeeException, DuplicateEmployeeException, DirectoryException {
        // check for duplicate emails and phone numbers
        // write an audit record
        e.setEid(++id);
        e.setStatus(Status.ACTIVE);
        storage.create(e);
        //write an audit record
        return e;
    }

    @Override
    public Employee search(int eid) throws EmployeeNotFoundException, DirectoryException {
        Employee e = storage.read(eid);
        if (e == null)
            throw new EmployeeNotFoundException();
        return e;
    }

    @Override
    public List<Employee> search(Name name) throws DirectoryException {
        return null;
    }

    @Override
    public Employee update(Employee e)
            throws InvalidEmployeeException, DuplicateEmployeeException, EmployeeNotFoundException, DirectoryException {
        return null;
    }

    @Override
    public Employee remove(int eid) throws EmployeeNotFoundException, DirectoryException {
        return null;
    }

}
