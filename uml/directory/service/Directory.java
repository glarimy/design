package directory.service;

import java.util.List;

import directory.domain.DirectoryException;
import directory.domain.DuplicateEmployeeException;
import directory.domain.Employee;
import directory.domain.EmployeeNotFoundException;
import directory.domain.InvalidEmployeeException;
import directory.domain.Name;

public interface Directory {

    Employee add(Employee e) throws InvalidEmployeeException, DuplicateEmployeeException, DirectoryException;

    Employee update(Employee e)
            throws InvalidEmployeeException, DuplicateEmployeeException, EmployeeNotFoundException, DirectoryException;

    Employee remove(int eid) throws EmployeeNotFoundException, DirectoryException;

    Employee search(int eid) throws EmployeeNotFoundException, DirectoryException;

    List<Employee> search(Name name) throws DirectoryException;

}