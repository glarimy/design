package directory.app;

public class NewEmployee {
    public String firstName;
    public String lastName;
    public long phone;
    public String email;
    public String permLocation;
    public String permCity;
    public int permPin;
    public String currentLocation;
    public String currentCity;
    public int currentPin;

}
