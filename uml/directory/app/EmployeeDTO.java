package directory.app;

public class EmployeeDTO {
    public int eid;
    public String name;
    public long phone;
    public String email;
    public String permanentAddress;
    public String currentAddress;
}
