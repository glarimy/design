package directory.app;

import directory.domain.Address;
import directory.domain.DirectoryException;
import directory.domain.Email;
import directory.domain.Employee;
import directory.domain.Name;
import directory.domain.Phone;
import directory.domain.Pincode;
import directory.service.Directory;

public class DirectoryController {
    private Directory dir;

    public DirectoryController(Directory dir) {
        this.dir = dir;
    }

    public Response add(NewEmployee e) {
        Name name = new Name(e.firstName, e.lastName);
        Phone phone = new Phone(e.phone);
        Email email = new Email(e.email);
        Address permanent = new Address(e.permLocation, e.permCity, new Pincode(e.permPin));
        Address current = new Address(e.currentLocation, e.currentCity, new Pincode(e.currentPin));
        Employee emp = new Employee(name, phone, email, permanent, current);
        try {
            emp = dir.add(emp);
            EmployeeDTO dto = new EmployeeDTO();
            dto.eid = emp.getEid();
            dto.name = emp.getName().getName();
            dto.phone = emp.getPhone().getValue();
            dto.email = emp.getEmail().getValue();
            dto.permanentAddress = emp.getPermanent().toString();
            dto.currentAddress = emp.getCurrent().toString();
            Response response = new Response();
            response.success = true;
            response.data = dto;
            return response;
        } catch (DirectoryException de) {
            Response response = new Response();
            response.success = false;
            response.data = "Error";
            return response;
        }
    }

    public Response find(int eid) {
        try {
            Employee emp = dir.search(eid);
            EmployeeDTO dto = new EmployeeDTO();
            dto.eid = emp.getEid();
            dto.name = emp.getName().getName();
            dto.phone = emp.getPhone().getValue();
            dto.email = emp.getEmail().getValue();
            dto.permanentAddress = emp.getPermanent().toString();
            dto.currentAddress = emp.getCurrent().toString();
            Response response = new Response();
            response.success = true;
            response.data = dto;
            return response;
        } catch (DirectoryException de) {
            Response response = new Response();
            response.success = false;
            response.data = "Error";
            return response;
        }
    }
}
