package directory.app;

public interface DirectoryFactory {
    public DirectoryController getDirectory(boolean withAudit);
}
