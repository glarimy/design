package directory.app;

import java.io.FileReader;
import java.util.Properties;

import directory.domain.Storage;
import directory.service.Directory;
import directory.service.DirectoryImpl;
import directory.service.DirectoryWithAudit;

public class SimpleDirectoryFactory implements DirectoryFactory {
    private Storage storage;

    public SimpleDirectoryFactory() {
        try {
            Properties config = new Properties();
            config.load(new FileReader("application.properties"));
            Class claz = Class.forName(config.getProperty("db"));
            storage = (Storage) claz.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    @Override
    public DirectoryController getDirectory(boolean withAudit) {
        Directory directory = new DirectoryImpl(storage);
        if(withAudit){
            directory = new DirectoryWithAudit(directory);
        }
        DirectoryController ctrl = new DirectoryController(directory);
        return ctrl;
    }
}
