package directory.domain;

public enum Status {
    ACTIVE, INACTIVE
}
