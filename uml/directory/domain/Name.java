package directory.domain;

public class Name {
    private String first;
    private String last;

    public Name(String first, String last) {
        //validate
        this.first = first;
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public String getLast() {
        return last;
    }
    public String getName(){
        return first + " " + last;
    }
}