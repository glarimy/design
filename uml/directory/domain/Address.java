package directory.domain;

public class Address {
    private String location;
    private String city;
    private Pincode pincode;

    public Address(String location, String city, Pincode pincode) {
        this.location = location;
        this.city = city;
        this.pincode = pincode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Pincode getPincode() {
        return pincode;
    }

    public void setPincode(Pincode pincode) {
        this.pincode = pincode;
    }

    public String toString(){
        return location + ", " + city + ", " + pincode.getValue();
    }
}
