package directory.domain;

import java.util.List;

public interface Storage {
    public Employee create(Employee e);
    public Employee read(int eid);
    public List<Employee> read(String name);
    public Employee update(Employee e);
    public Employee delete(int eid);
}
