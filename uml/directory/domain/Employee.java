package directory.domain;

public class Employee {
    private int eid;
    private Name name;
    private Phone phone;
    private Email email;
    private Address permanent;
    private Address current;
    private Status status;

    public Employee(int eid, Name name, Phone phone, Email email, Address permanent, Address current, Status status) {
        this.eid = eid;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.permanent = permanent;
        this.current = current;
        this.status = status;
    }
    
    public Employee(Name name, Phone phone, Email email, Address permanent, Address current) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.permanent = permanent;
        this.current = current;
    }
    public int getEid() {
        return eid;
    }
    public void setEid(int eid) {
        this.eid = eid;
    }
    public Name getName() {
        return name;
    }
    public void setName(Name name) {
        this.name = name;
    }
    public Phone getPhone() {
        return phone;
    }
    public void setPhone(Phone phone) {
        this.phone = phone;
    }
    public Email getEmail() {
        return email;
    }
    public void setEmail(Email email) {
        this.email = email;
    }
    public Address getPermanent() {
        return permanent;
    }
    public void setPermanent(Address permanent) {
        this.permanent = permanent;
    }
    public Address getCurrent() {
        return current;
    }
    public void setCurrent(Address current) {
        this.current = current;
    }
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }

    
}
