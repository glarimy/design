package directory.infra;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import directory.domain.Employee;
import directory.domain.Storage;

public class MySqlStorage implements Storage {

    public MySqlStorage() {
    }

    @Override
    public Employee create(Employee e) {
        System.out.println("MySQL is adding the employee");
        return e;
    }

    @Override
    public Employee read(int eid) {
        return null;
    }

    @Override
    public List<Employee> read(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Employee update(Employee e) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Employee delete(int eid) {
        // TODO Auto-generated method stub
        return null;
    }

}
