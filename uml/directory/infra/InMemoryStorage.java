package directory.infra;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import directory.domain.Employee;
import directory.domain.Storage;

public class InMemoryStorage implements Storage {
    private Map<Integer, Employee> entries;

    public InMemoryStorage() {
        this.entries = new HashMap<>();
    }

    @Override
    public Employee create(Employee e) {
        entries.put(e.getEid(), e);
        return e;
    }

    @Override
    public Employee read(int eid) {
        return entries.get(eid);
    }

    @Override
    public List<Employee> read(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Employee update(Employee e) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Employee delete(int eid) {
        // TODO Auto-generated method stub
        return null;
    }

}
