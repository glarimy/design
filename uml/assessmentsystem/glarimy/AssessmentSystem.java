package glarimy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import glarimy.as.api.Assessment;
import glarimy.as.api.Examiner;
import glarimy.as.api.Result;
import glarimy.as.service.AssessmentRepositoryImpl;
import glarimy.as.service.ExaminerImpl;
import glarimy.as.service.ExaminerLogger;
import glarimy.as.service.ResultRepositoryImpl;
import glarimy.dir.api.Email;
import glarimy.dir.api.Employee;
import glarimy.dir.api.EmployeeDirectory;
import glarimy.dir.api.Name;
import glarimy.dir.service.EmployeeDirectoryImpl;
import glarimy.dir.service.EmployeeRepositoryImpl;
import glarimy.qb.api.Answer;
import glarimy.qb.api.Question;
import glarimy.qb.api.QuestionBank;
import glarimy.qb.api.QuizQuestion;
import glarimy.qb.service.QuestionBankImpl;
import glarimy.qb.service.QuestionRepositoryImpl;

public class AssessmentSystem {
    public static void main(String[] args) {
        // Admin adds the employees
        EmployeeDirectory directory = new EmployeeDirectoryImpl(new EmployeeRepositoryImpl());
        directory.add(new Employee(1971, new Name("Krishna", "Koyya"), new Email("krishna@gmail.com")));
        directory.add(new Employee(1972, new Name("Mohan", "Koyya"), new Email("mohan@gmail.com")));

        // SME builds the question bank
        QuestionBank qb = new QuestionBankImpl(new QuestionRepositoryImpl());
        qb.add(new Question("1+1=?", new String[] { "1", "2", "3", "4" }, 2, "Maths"));
        qb.add(new Question("1+2=?", new String[] { "1", "2", "3", "4" }, 3, "Maths"));
        qb.add(new Question("1+3=?", new String[] { "1", "2", "3", "4" }, 4, "Maths"));
        qb.add(new Question("1+4=?", new String[] { "1", "2", "3", "5" }, 4, "Maths"));
        qb.add(new Question("1+5=?", new String[] { "1", "2", "3", "6" }, 4, "Maths"));
        qb.add(new Question("1+6=?", new String[] { "1", "2", "3", "7" }, 4, "Maths"));
        qb.add(new Question("1+7=?", new String[] { "1", "2", "3", "8" }, 4, "Maths"));
        qb.add(new Question("1+8=?", new String[] { "1", "2", "3", "9" }, 4, "Maths"));
        qb.add(new Question("1+9=?", new String[] { "1", "2", "3", "10" }, 4, "Maths"));

        // LnD schedules an assessment
        Examiner examiner = new ExaminerLogger(new ExaminerImpl(qb, new ResultRepositoryImpl(), new AssessmentRepositoryImpl()));
        int aid = examiner.create("Maths", directory.list(), new Date());

        // Employee takes an assessment
        Assessment assessment = examiner.find(aid);
        System.out.println("ASSESSMENT");
        assessment.display();
        List<Answer> answers = new ArrayList<>();
        for (QuizQuestion q : assessment.getQuestions()) {
            answers.add(new Answer(q.getQid(), 4));
        }
        for (Integer eid : assessment.getCandidates()) {
            int score = examiner.evaluate(aid, eid, answers);
            System.out.println("Employee ID: " + eid + " - Score: " + score);
        }

        // LnD schedules another assessment
        aid = examiner.create("Maths", directory.list(), new Date());

        // Employee takes the new assessment
        assessment = examiner.find(aid);
        System.out.println("ASSESSMENT");
        assessment.display();
        answers = new ArrayList<>();
        for (QuizQuestion q : assessment.getQuestions()) {
            answers.add(new Answer(q.getQid(), 3));
        }
        for (Integer eid : assessment.getCandidates()) {
            int score = examiner.evaluate(aid, eid, answers);
            System.out.println("Employee ID: " + eid + " - Score: " + score);
        }

        // Employee or Manager views scores of a employee
        System.out.println("REPORT FOR EMPLOYEE ID: 1971");
        List<Result> results = examiner.resultsForEmployee(1971);
        for(Result r: results){
            System.out.println("Assessment ID: " + r.getAid() + " Score: " + r.getScore());
        }

        // LnD or Manager views scores of an assessment
        System.out.println("REPORT FOR ASSESSMENT ID " + aid);
        results = examiner.resultsForAssessment(aid);
        for(Result r: results){
            System.out.println("Employee ID: " + r.getEid() + " Score: " + r.getScore());
        }

        // LnD or Manager or Employee views all assessments
        System.out.println("ALL ASSESSMENTS");
        List<Assessment> assessments = examiner.assessments();
        for (Assessment a : assessments) {
            System.out.println("Assessment ID: " + a.getAid());
            results = examiner.resultsForAssessment(aid);
            for(Result r: results){
                System.out.println("Employee ID: " + r.getEid() + " Score: " + r.getScore());
            }
        }

        // LnD or Manager or Employee views all assessments
        System.out.println("ALL EMPLOYEES");
        List<Integer> employees = directory.list();
        for (Integer e : employees) {
            System.out.println("Employee: " + e + "["+ directory.find(e).getName() + "]");
            results = examiner.resultsForEmployee(e);
            for(Result r: results){
                System.out.println("Assessment ID: " + r.getAid() + " Score: " + r.getScore());
            }

        }
    }
}
