package glarimy.dir.api;

public class Employee {
    private int eid;
    private Email email;
    private Name name;

    public Employee(int eid, Name name, Email email) {
        this.eid = eid;
        this.name = name;
        this.email = email;
    }

    public int getEid() {
        return eid;
    }

    public String getName() {
        return name.getFullName();
    }

    public String getEmail() {
        return email.getValue();
    }
}
