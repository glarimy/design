package glarimy.dir.api;

import java.util.List;

public interface EmployeeDirectory {
    public Employee add(Employee e);
    public Employee find(int eid);
    public List<Integer> list();
}
