package glarimy.dir.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import glarimy.dir.api.Employee;

public class EmployeeRepositoryImpl implements EmployeeRepository {
    private Map<Integer, Employee> employees = new HashMap<>();

    @Override
    public Employee save(Employee e) {
        employees.put(e.getEid(), e);
        return e;
    }

    @Override
    public Employee find(int eid) {
        return employees.get(eid);
    }

    @Override
    public List<Integer> list() {
        return new ArrayList<>(employees.keySet());
    }
}
