package glarimy.dir.service;

import java.util.List;

import glarimy.dir.api.Employee;
import glarimy.dir.api.EmployeeDirectory;

public class EmployeeDirectoryImpl implements EmployeeDirectory {
    private EmployeeRepository repo;
    
    public EmployeeDirectoryImpl(EmployeeRepository repo) {
        this.repo = repo;
    }

    @Override
    public Employee add(Employee e) {
        return repo.save(e);
    }

    @Override
    public Employee find(int eid) {
        return repo.find(eid);
    }

    @Override
    public List<Integer> list() {
        return repo.list();
    }
    
}
