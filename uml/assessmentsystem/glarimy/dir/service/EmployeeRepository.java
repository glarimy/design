package glarimy.dir.service;

import java.util.List;

import glarimy.dir.api.Employee;

public interface EmployeeRepository {
    public Employee save(Employee e);
    public List<Integer> list();
    public Employee find(int eid);
}
