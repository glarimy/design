package glarimy.as.service;

import java.util.List;

import glarimy.as.api.Result;

public interface ResultRepository {
    public Result save(Result result);

    public Result find(int aid, int eid);

    public List<Result> findByEid(int eid);

    public List<Result> findByAid(int aid);
}
