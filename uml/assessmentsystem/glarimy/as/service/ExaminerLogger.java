package glarimy.as.service;

import java.util.Date;
import java.util.List;

import glarimy.as.api.Assessment;
import glarimy.as.api.Examiner;
import glarimy.as.api.Result;
import glarimy.qb.api.Answer;

public class ExaminerLogger implements Examiner {
    private Examiner target;

    public ExaminerLogger(Examiner target) {
        this.target = target;
    }

    @Override
    public List<Assessment> assessments() {
        System.out.println(new Date() + ": assessments - entering");
        List<Assessment> assessments = target.assessments();
        System.out.println(new Date() + ": assessments - exiting");
        return assessments;
    }

    @Override
    public void cancel(int aid) {
        System.out.println(new Date() + ": cancel - entering");
        target.cancel(aid);
        System.out.println(new Date() + ": cancel - exiting");
    }

    @Override
    public int create(String subject, List<Integer> eids, Date time) {
        System.out.println(new Date() + ": create - entering");
        int aid = target.create(subject, eids, time);
        System.out.println(new Date() + ": create - exiting");
        return aid;
    }

    @Override
    public int evaluate(int aid, int eid, List<Answer> answers) {
        System.out.println(new Date() + ": evaluate - entering");
        int score = target.evaluate(aid, eid, answers);
        System.out.println(new Date() + ": evaluate - exiting");
        return score;
    }

    @Override
    public Assessment find(int aid) {
        System.out.println(new Date() + ": find - entering");
        Assessment assessment = target.find(aid);
        System.out.println(new Date() + ": find - exiting");
        return assessment;
    }

    @Override
    public void reschedule(int aid, Date time) {
        System.out.println(new Date() + ": reschedule - entering");
        target.reschedule(aid, time);
        System.out.println(new Date() + ": reschedule - exiting");
    }

    @Override
    public List<Result> resultsForAssessment(int aid) {
        System.out.println(new Date() + ": resultsForAssessment - entering");
        List<Result> results = target.resultsForAssessment(aid);
        System.out.println(new Date() + ": resultsForAssessment - exiting");
        return results;
    }

    @Override
    public List<Result> resultsForEmployee(int eid) {
        System.out.println(new Date() + ": resultsForEmployee - entering");
        List<Result> results = target.resultsForEmployee(eid);
        System.out.println(new Date() + ": resultsForEmployee - exiting");
        return results;
    }

}
