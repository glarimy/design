package glarimy.as.service;

import java.util.List;

import glarimy.as.api.Assessment;

public interface AssessmentRepository {
    public Assessment save(Assessment assessment);
    public Assessment find(int aid);
    public List<Assessment> list();
    public Assessment update(Assessment assessment);
}
