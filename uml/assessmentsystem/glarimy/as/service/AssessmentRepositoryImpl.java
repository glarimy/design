package glarimy.as.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import glarimy.as.api.Assessment;

public class AssessmentRepositoryImpl implements AssessmentRepository {
    private Map<Integer, Assessment> assessments = new HashMap<>();

    @Override
    public Assessment save(Assessment assessment) {
        assessments.put(assessment.getAid(), assessment);
        return assessment;
    }

    @Override
    public Assessment find(int aid) {
        return assessments.get(aid);
    }

    @Override
    public List<Assessment> list() {
        return new ArrayList<>(assessments.values());
    }

    @Override
    public Assessment update(Assessment assessment) {
        if(assessments.containsKey(assessment.getAid()))
            assessments.put(assessment.getAid(), assessment);
        return assessment;
    }

}
