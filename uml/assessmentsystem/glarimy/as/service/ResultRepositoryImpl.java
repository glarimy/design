package glarimy.as.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import glarimy.as.api.Result;

public class ResultRepositoryImpl implements ResultRepository{
    private List<Result> results = new ArrayList<>();

    @Override
    public Result save(Result result) {
        results.add(result);
        return result;
    }

    @Override
    public Result find(int aid, int eid) {
        // TBD
        return null;
    }

    @Override
    public List<Result> findByAid(int aid) {
        return results.stream().filter(result -> result.getAid() == aid).collect(Collectors.toList());
    }

    @Override
    public List<Result> findByEid(int eid) {
        return results.stream().filter(result -> result.getEid() == eid).collect(Collectors.toList());
    }


}
