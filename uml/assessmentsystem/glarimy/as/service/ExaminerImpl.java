package glarimy.as.service;

import java.util.Date;
import java.util.List;

import glarimy.as.api.Assessment;
import glarimy.as.api.Examiner;
import glarimy.as.api.Result;
import glarimy.qb.api.Answer;
import glarimy.qb.api.QuestionBank;
import glarimy.qb.api.QuizQuestion;

public class ExaminerImpl implements Examiner {
    private QuestionBank qb;
    private AssessmentRepository assessmentRepo;
    private ResultRepository resultRepo;

    public ExaminerImpl(QuestionBank qb, ResultRepository resultRepo, AssessmentRepository assessmentRepo) {
        this.qb = qb;
        this.resultRepo = resultRepo;
        this.assessmentRepo = assessmentRepo;
    }

    @Override
    public int create(String subject, List<Integer> eids, Date time) {
        List<QuizQuestion> questions = this.qb.fetch(subject, 5);
        Assessment assessment = new Assessment(subject, eids, questions, time);
        assessmentRepo.save(assessment);
        return assessment.getAid();
    }

    @Override
    public Assessment find(int aid) {
        return assessmentRepo.find(aid);
    }

    public List<Assessment> assessments() {
        return assessmentRepo.list();
    }

    @Override
    public int evaluate(int aid, int eid, List<Answer> answers) {
        int score = qb.evaluate(answers);
        Result result = new Result(aid, eid, score);
        resultRepo.save(result);
        return score;
    }

    @Override
    public List<Result> resultsForAssessment(int aid) {
        return resultRepo.findByAid(aid);
    }

    @Override
    public List<Result> resultsForEmployee(int eid) {
        return resultRepo.findByEid(eid);
    }

    @Override
    public void cancel(int aid) {
        Assessment assessment = assessmentRepo.find(aid);
        assessment.cancel();
        assessmentRepo.update(assessment);
    }

    @Override
    public void reschedule(int aid, Date time) {
        Assessment assessment = assessmentRepo.find(aid);
        assessment.reschedule(new Date());
        assessmentRepo.update(assessment);

    }

}