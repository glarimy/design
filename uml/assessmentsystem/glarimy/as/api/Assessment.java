package glarimy.as.api;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import glarimy.qb.api.QuizQuestion;

public class Assessment {
    private int aid;
    private List<Integer> candidates;
    private List<QuizQuestion> questions;
    private String subject;
    private Date time;
    private int duration;
    private Status status;

    public Assessment(String subject, List<Integer> candidates, List<QuizQuestion> questions, Date time) {
        this.aid = ThreadLocalRandom.current().nextInt(0, 100);
        this.subject = subject;
        this.candidates = candidates;
        this.questions = questions;
        this.time = time;
        this.duration = 30;
        this.status = Status.SCHEDULED;
    }

    public int getAid() {
        return aid;
    }

    public String getSubject() {
        return subject;
    }

    public List<Integer> getCandidates() {
        return candidates;
    }

    public List<QuizQuestion> getQuestions() {
        return questions;
    }

    public Date getTime() {
        return time;
    }

    public int getDuration() {
        return duration;
    }

    public Status getStatus() {
        return status;
    }

    public void cancel() {
        if(status == Status.SCHEDULED)
            this.status = Status.CANCELLED;
    }

    public void reschedule(Date time) {
        this.time = time;
    }

    public void display() {
        System.out.println("Assessment ID: " + aid);
        System.out.println("Subject: " + subject);
        System.out.println("Candidates: " + candidates);
        for (QuizQuestion q : questions) {
            q.display();
        }
    }
}
