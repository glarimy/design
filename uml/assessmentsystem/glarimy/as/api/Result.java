package glarimy.as.api;

public class Result {
    private int aid;
    private int eid;
    private int score;

    public Result(int aid, int eid, int score) {
        this.aid = aid;
        this.eid = eid;
        this.score = score;
    }

    public int getAid() {
        return aid;
    }

    public int getEid() {
        return eid;
    }

    public int getScore() {
        return score;
    }
}
