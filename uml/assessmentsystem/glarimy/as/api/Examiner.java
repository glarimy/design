package glarimy.as.api;

import java.util.Date;
import java.util.List;

import glarimy.qb.api.Answer;

public interface Examiner {
    public int create(String subject, List<Integer> eids, Date time);
    public List<Assessment> assessments();
    public Assessment find(int aid);
    public int evaluate(int aid, int eid, List<Answer> answers);
    public List<Result> resultsForAssessment(int aid);
    public List<Result> resultsForEmployee(int eid);
    public void reschedule(int aid, Date time);
    public void cancel(int aid);
}
