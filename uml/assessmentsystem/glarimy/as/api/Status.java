package glarimy.as.api;

public enum Status {
    SCHEDULED, CANCELLED, ACTIVE, COMPLETED
}
