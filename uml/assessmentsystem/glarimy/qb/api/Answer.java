package glarimy.qb.api;

public class Answer {
    private int qid;
    private int choice;

    public Answer(int qid, int choice) {
        this.qid = qid;
        this.choice = choice;
    }

    public int getQid() {
        return qid;
    }

    public int getChoice() {
        return choice;
    }
}
