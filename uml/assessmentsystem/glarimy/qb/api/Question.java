package glarimy.qb.api;

public class Question {
    private int qid;
    private String description;
    private String[] options;
    private int answer;
    private String subject;

    public Question(String description, String[] options, int answer, String subject) {
        this.description = description;
        this.options = options;
        this.answer = answer;
        this.subject = subject;
    }

    public int getQid() {
        return qid;
    }

    public void setQid(int qid) {
        this.qid = qid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getOptions() {
        return options;
    }

    public void setOptions(String[] options) {
        this.options = options;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public boolean isCorrect(int answer){
        if(this.answer == answer)
            return true;
        return false;
    }

    public QuizQuestion get() {
        return new QuizQuestion(qid, description, options);
    }
}
