package glarimy.qb.service;

import java.util.List;
import java.util.stream.Collectors;

import glarimy.qb.api.Answer;
import glarimy.qb.api.Question;
import glarimy.qb.api.QuestionBank;
import glarimy.qb.api.QuizQuestion;

public class QuestionBankImpl implements QuestionBank {
    private QuestionRepository repo;

    public QuestionBankImpl(QuestionRepository repo) {
        this.repo = repo;
    }

    @Override
    public void add(Question question) {
        repo.save(question);
    }

    @Override
    public List<QuizQuestion> fetch(String subject, int count) {
        List<Question> questions = repo.get(count);
        return questions.stream().map(q -> q.get()).collect(Collectors.toList());
    }

    @Override
    public int evaluate(List<Answer> answers) {
        int score = 0;
        for (Answer answer : answers) {
            if (repo.find(answer.getQid()).isCorrect(answer.getChoice()))
                score++;
        }
        return score;
    }
}