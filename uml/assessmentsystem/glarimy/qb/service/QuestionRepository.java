package glarimy.qb.service;

import java.util.List;

import glarimy.qb.api.Question;

public interface QuestionRepository {
    public void save(Question question);
    public List<Question> get(int count);
    public Question find(int qid);
}
