package com.glarimy.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WalletServiceBootstrap {

	public static void main(String[] args) {
		SpringApplication.run(WalletServiceBootstrap.class, args);
	}

}
