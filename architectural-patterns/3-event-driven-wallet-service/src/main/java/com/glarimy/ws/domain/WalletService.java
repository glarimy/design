package com.glarimy.ws.domain;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Service
@EnableTransactionManagement
public class WalletService {
	@Autowired
	private WalletRepository repo;

	@Transactional
	public void credit(int orderId, long phone, double amount) throws WalletNotFoundException {
		Wallet wallet = repo.findById(phone).orElseThrow(() -> new WalletNotFoundException());
		wallet.setBalance(wallet.getBalance() + amount);
	}

	@Transactional
	public void debit(int orderId, long phone, double amount)
			throws WalletNotFoundException, InsufficientBalanceException {
		Wallet wallet = repo.findById(phone).orElseThrow(() -> new WalletNotFoundException());
		if (wallet.getBalance() < amount)
			throw new InsufficientBalanceException();
		wallet.setBalance(wallet.getBalance() - amount);
	}
}
