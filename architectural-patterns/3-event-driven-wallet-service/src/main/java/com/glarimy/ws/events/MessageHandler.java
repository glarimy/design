package com.glarimy.ws.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.glarimy.ws.domain.InsufficientBalanceException;
import com.glarimy.ws.domain.WalletNotFoundException;
import com.glarimy.ws.domain.WalletService;

@Component
public class MessageHandler {
	@Autowired
	private WalletService service;

	@Value("${wallet.producer.topic}")
	private String topic;

	@Autowired
	private KafkaTemplate<String, Message> kafka;

	@KafkaListener(topics = "${wallet.consumer.topic}", groupId = "${wallet.consumer.group}")
	public void handle(Message command) {
		System.out.println("Received: " + command);
		Message result = new Message();
		result.setId(command.getId());
		result.setType("result");
		result.setOperation(command.getOperation());
		result.setData(command.getData());

		try {
			if (command.getOperation().equals("credit")) {
				service.credit(command.getId(), (Long) command.getData().get("phone"),
						(Double) command.getData().get("amount"));
				result.setSuccess(true);
			} else if (command.getOperation().equals("debit")) {
				service.debit(command.getId(), (Long) command.getData().get("phone"),
						(Double) command.getData().get("amount"));
				result.setSuccess(true);
			} else {
				System.out.println("Unsupported operation");
				result.setSuccess(false);
			}
		} catch (WalletNotFoundException e) {
			System.out.println("Wallet not found");
			result.setSuccess(false);
		} catch (InsufficientBalanceException e) {
			System.out.println("Insufficient balance");
			result.setSuccess(false);
		}
		System.out.println("Sending: " + result);
		kafka.send(topic, Integer.toString(result.getId()), result);
	}
}
