Title: Architectural Patterns
Description: Pattern-based design and architecture improve the quality of the system without reinventing the wheel. There are scores of patterns published targeting different styles of design. This program aims at discussing patterns relevant to concurrent, distributed systems and microservices. 
Objectives: 
	1. To work with patterns relevant to designing concurrent systems
	2. To work with patterns relevant to designing distributed systems
	3. To work with patterns relevant to messaging
	4. To work with patterns relevant to microservices
	5. To map the patterns with some of the popular tools, frameworks and platforms
Level: Intermediate to Advanced
Target Audience: Developers, Designers and Architects
Prerequisites: 
	1. A very good command of GoF patterns (MUST)
	2. 2-5 years of solution designing experience (MUST)
	3. Working knowledge of RDBMS like MySQL (Desired)
	4. Working knowledge of NoSQL systems like MongoDB (Desired)
	5. Working knowledge of Message Brokers like Apache Kafka (Desired)
	6. Working knowledge of Distributed Systems like Apache Spark (Desired)
	7. Working knowledge of Dockers and Kubernetes (Desired)
Suggested pre-read:
	1. https://www.digitalocean.com/community/tutorials/gangs-of-four-gof-design-patterns
Lab Setup:
	1. A drawing tool like https://app.diagrams.net/
Approach: 
	1. Each of the items (patterns) will be discussed in terms of problem, context, solution and results
	2. Patterns are explained and discussed using models
	3. The patterns are mapped to popular products like MongoDB, Apache Spark, Apache Kafka and etc., 
	4. There will be no code developed during the sessions
Course Schedule: 
	2-Full-Day Sessions:
		DAY-1: Modules 1, 2, 3, 4
		Day-2: Modules 5, 6, 7, 8
	3-Half-Day Sesions:
		DAY-1: Modules 1, 2, 3
		DAY-2: Modules 4, 5
		DAY-3: Modules 6, 7, 8

Course Modules: (T: Theory, D: Discussion)
	Module 1: The story of architecture
		Monoliths (T) 
		Modular (T) 
		Client/Server (T) 
		Web (T) 
		Distributed (T) 
		Messaging (T) 
		SOA (T) 
		REST (T) 
		Microservices (T) 
		Serverless (T) 
		Cloud (T) 

	Module 2: Prime Factors for Modern Architecture
		Scalability (T) 
		Resilience (T) 
		Stability (T) 
		Ability to grow (T) 
		Service Integration (T)	 

	Module 3: Patterns for Messaging
		Point-to-point (T+D)   
		Publish/Subscribe (T+D)   
		Message Bus (T+D)   
		Command (T+D)   
		Document (T+D)   
		Event Notification (T+D)   
		Return Address (T+D)   
		Correlation (T+D)   
		Message Sequence (T+D)   
		Channel Adapter (T+D)   
		Dead Letter Channel (T+D)   
		Filters (T+D)   
		Routers (T+D)   
		Translators (T+D)   
		Pipes (T+D)   
		Endpoints (T+D)   

	Module 4: Patterns for Microservices
		12-factor app (T+D) 
		Service per Host/VM/Container (T+D)
		Gateway (T+D)  
		Backend to Frontend (T+D)  
		Replicated Load-Balanced Services (T+D)  
		Service Registry (T+D)   
		Registrar (T+D)   
		Self Registration (T+D)  
		Service Discovery (T+D)   
		Circuit breaker (T+D)   
		Chassis (T+D)  
		Log Aggregation (T+D) 
		Distributed Tracing (T+D)    
		Access Token (T+D)
		Strangler (T+D)  
		Sidecar (T+D)  
		Ambassador (T+D)   (post) 
		Adatper (T+D)   (pre)

	Module 5: Data Management Patterns
		Database per Service (T+D)  
		Shared Database (T+D)  
		Two-Phase Commit (T+D)  
		Saga (T+D)  
		Choreography (T+D)  
		Orchestration (T+D)  
		Transactional Outbox (T+D)
		Event Sourcing (T+D)  
		Domain Event (T+D)
		CQRS (T+D)
		API Composition (T+D)

	Module 6: Patterns for Distributed Systems
		Master-Worker (T+D)  
		Leaders and Followers (T+D)    
		Scatter and Gather (T+D) map-reduce
		Sharded Services (T+D)
		Follower Reads (T+D)   
		Heart Beat (T+D) 
		Idempotent Consumer (T+D)  
		Master Election (T+D) 
		Consistent Core (T+D) 
		
		Segmented Log (T+D) 
		Write-Ahead Log (T+D)  
		Replicated Log (T+D) 

	Module 7: Patterns for Concurrent Systems
		Active Object (T+D)
		Monitor Object(T+D)
		Lock(T+D) 

		Lamport Clock (T+D)
		Generation Clock (T+D)
		Hybrid Clock (T+D)

		Reactor(T+D) - Team 3
		Proctor(T+D) - Team 3

		Thread-Specific Storage (T+D) 
		Scheduler (T+D)  
		Thread Pool (T+D)  
		Async Completion Token (T+D)  

	Module 8: Conclusion