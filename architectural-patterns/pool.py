from queue import Queue
from threading import Thread, current_thread
import time 

def worker(queue):
    print(current_thread().name, " thread is ready")
    while(True):
        print(current_thread().name, " waiting for the next task")
        task = queue.get()
        print(current_thread().name, " executing the ", task)
        time.sleep(5)
        print(current_thread().name, " executed the ", task)

class ExecutorWithThreadpool:
    def __init__(self):
        self.queue = Queue()
        self.first = Thread(target=worker, args=[self.queue])
        self.second = Thread(target=worker, args=[self.queue])
        self.third = Thread(target=worker, args = [self.queue])
        self.first.start()
        self.second.start()
        self.third.start()

    def submit(self, task):
        self.queue.put(task)

pool = ExecutorWithThreadpool()
while(True):
    task = input("Enter a task")
    pool.submit(task)