package com.glarimy.os.app;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.glarimy.os.domain.Order;
import com.glarimy.os.domain.OrderFailedException;
import com.glarimy.os.domain.OrderNotFoundException;
import com.glarimy.os.domain.OrderService;

@RestController
public class OrderController {

	@Autowired
	private OrderService service;

	@PostMapping("/order")
	public ResponseEntity<Order> create(@RequestBody OrderRequest request) {
		Order order = new Order();
		order.setProductId(request.getProductId());
		order.setPhone(request.getPhone());
		order.setAmount(request.getAmount());
		order.setQuantity(request.getQuantity());
		try {
			order = service.create(order);
			return ResponseEntity.created(URI.create("/order/" + order.getOrderId())).body(order);
		} catch (OrderFailedException e) {
			return ResponseEntity.badRequest().body(e.order);
		}
	}

	@GetMapping("/order/{id}")
	public ResponseEntity<Order> find(@PathVariable("id") int id) {
		try {
			Order order = service.find(id);
			return ResponseEntity.ok().body(order);
		} catch (OrderNotFoundException onfe) {
			return ResponseEntity.notFound().build();
		}
	}
}
