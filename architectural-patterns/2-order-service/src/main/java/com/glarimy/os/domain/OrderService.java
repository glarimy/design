package com.glarimy.os.domain;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

@Service
@EnableTransactionManagement
public class OrderService {
	@Autowired
	private OrderRepository repo;

	@Value("${product.service.url}")
	private String psUrl;

	@Value("${wallet.service.url}")
	private String wsUrl;

	@Transactional
	public Order create(Order order) throws OrderFailedException {
		order = repo.save(order);
		RestTemplate api = new RestTemplate();

		try {
			api.exchange(psUrl + "/product/" + order.getProductId() + "/reserve?quantity=" + order.getQuantity(),
					HttpMethod.POST, null, Product.class);
			try {
				api.exchange(wsUrl + "/wallet/" + order.getPhone() + "/debit?amount=" + order.getAmount(),
						HttpMethod.POST, null, Transaction.class);
				order.setStatus(Status.CONFIRMED);
				return order;
			} catch (HttpStatusCodeException sce) {
				order.setStatus(Status.FAILED);
				api.exchange(psUrl + "/product/" + order.getProductId() + "/release?quantity=" + order.getQuantity(),
						HttpMethod.POST, null, Product.class);
				throw new OrderFailedException(order);
			}

		} catch (HttpStatusCodeException e) {
			order.setStatus(Status.FAILED);
			throw new OrderFailedException(order);
		}
	}

	public Order find(int id) throws OrderNotFoundException {
		return repo.findById(id).orElseThrow(() -> new OrderNotFoundException());
	}

}
