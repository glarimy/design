# Order Service API

## Usage

* Database: http://localhost:8080/h2-console/ with jdbc:h2:mem:orders-db
* Tests: http://localhost:8080/swagger-ui/index.html
* curl -X 'POST' 'http://localhost:8080/order' -H 'accept: */*' -H 'Content-Type: application/json' -d '{"phone": 123, "productId": 78, "amount": 50}'