# Order Service API

## Usage

* Database: http://localhost:8080/h2-console/ with jdbc:h2:mem:orders-db
* curl -X 'POST' 'http://localhost:8080/order' -H 'Content-Type: application/json' -d '{"phone": 9731423166, "productId": 1, "amount": 50, "quantity":1}'
* curl 'http://localhost:8080/order/1'