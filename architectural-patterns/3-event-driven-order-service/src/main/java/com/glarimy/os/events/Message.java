package com.glarimy.os.events;

import java.util.HashMap;
import java.util.Map;

public class Message {
	private int id;
	private String type;
	private String operation;
	private boolean success;
	private Map<String, Object> data = new HashMap<String, Object>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", type=" + type + ", operation=" + operation + ", success=" + success + ", data="
				+ data + "]";
	}

}
