package com.glarimy.os.domain;

@SuppressWarnings("serial")
public class OrderFailedException extends Exception {
	public Order order;

	public OrderFailedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OrderFailedException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public OrderFailedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public OrderFailedException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public OrderFailedException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public OrderFailedException(Order order) {
		this.order = order;
	}

}
