package com.glarimy.os.domain;

public enum Status {
	PENDING, CONFIRMED, FAILED
}
