package com.glarimy.os.events;

import org.apache.kafka.common.serialization.Serializer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MessageSerializer implements Serializer<Message> {

	@Override
	public byte[] serialize(String topic, Message message) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsBytes(message);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
	}
}
