package com.glarimy.os;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderServiceBootstrap {

	public static void main(String[] args) {
		SpringApplication.run(OrderServiceBootstrap.class, args);
	}

}
