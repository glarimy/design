package com.glarimy.os.domain;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.glarimy.os.events.Message;

@Service
@EnableTransactionManagement
public class OrderService {
	@Autowired
	private OrderRepository repo;

	@Value("${order.producer.topic}")
	private String topic;

	@Autowired
	private KafkaTemplate<String, Message> kafka;

	@Transactional
	public Order create(Order order) throws OrderFailedException {
		order = repo.save(order);
		Message command = new Message();
		command.setId(order.getOrderId());
		command.setType("command");
		command.setOperation("process");
		command.getData().put("orderId", order.getOrderId());
		command.getData().put("productId", order.getProductId());
		command.getData().put("phone", order.getPhone());
		command.getData().put("amount", order.getAmount());
		command.getData().put("quantity", order.getQuantity());
		System.out.println("Sending: " + command);
		kafka.send(topic, Integer.toString(command.getId()), command);
		return order;
	}

	@Transactional
	public void confirm(int orderId) throws OrderNotFoundException {
		Optional<Order> order = repo.findById(orderId);
		if (order.isPresent()) {
			order.get().setStatus(Status.CONFIRMED);
			return;
		}
		throw new OrderNotFoundException();
	}

	@Transactional
	public void reject(int orderId) throws OrderNotFoundException {
		Optional<Order> order = repo.findById(orderId);
		if (order.isPresent()) {
			order.get().setStatus(Status.FAILED);
			return;
		}
		throw new OrderNotFoundException();
	}

	public Order find(int id) throws OrderNotFoundException {
		return repo.findById(id).orElseThrow(() -> new OrderNotFoundException());
	}
}