package com.glarimy.os.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.glarimy.os.domain.OrderNotFoundException;
import com.glarimy.os.domain.OrderService;

@Component
public class SagaResultHandler {
	@Autowired
	private OrderService service;

	@KafkaListener(topics = "${order.consumer.topic}", groupId = "${order.consumer.group}")
	public void handle(Message result) {
		System.out.println("Received: " + result);
		try {
			if (result.isSuccess())
				service.confirm(result.getId());
			else
				service.reject(result.getId());
		} catch (OrderNotFoundException onfe) {
			System.out.println("Order not found");
		}
	}
}
