# Archiectural Patterns #

### 1. Microservices 
[https://microservices.io/](https://microservices.io/)

### 2. Messaging and Enterprise Integration Patterns
[https://www.enterpriseintegrationpatterns.com/](https://www.enterpriseintegrationpatterns.com/)

### 3. 12-Factor App
[https://12factor.net/](https://12factor.net/)

### 4. Introduction to Apache Kafka architecture
[https://www.opensourceforu.com/2021/11/apache-kafka-asynchronous-messaging-for-seamless-systems/](https://www.opensourceforu.com/2021/11/apache-kafka-asynchronous-messaging-for-seamless-systems/)

### 5. Introduction to Apache Spark architecture
[https://www.opensourceforu.com/2021/11/apache-spark-aiding-big-data-cluster-computing/](https://www.opensourceforu.com/2021/11/apache-spark-aiding-big-data-cluster-computing/)

### 6. Slides, Code, diagrams and etc., 
Available in the current folder 