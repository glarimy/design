package com.glarimy.ws.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.glarimy.ws.domain.InsufficientBalanceException;
import com.glarimy.ws.domain.Transaction;
import com.glarimy.ws.domain.WalletNotFoundException;
import com.glarimy.ws.domain.WalletService;

@RestController
public class WalletController {

	@Autowired
	private WalletService service;

	@PostMapping("/wallet/{phone}/credit")
	public ResponseEntity<Transaction> credit(@PathVariable("phone") long phone,
			@RequestParam("amount") double amount) {
		try {
			Transaction tx = service.credit(phone, amount);
			return ResponseEntity.ok(tx);
		} catch (WalletNotFoundException e) {
			return ResponseEntity.notFound().build();
		}
	}

	@PostMapping("/wallet/{phone}/debit")
	public ResponseEntity<Transaction> debit(@PathVariable("phone") long phone, @RequestParam("amount") double amount) {
		try {
			Transaction tx = service.debit(phone, amount);
			return ResponseEntity.ok(tx);
		} catch (WalletNotFoundException wnfe) {
			return ResponseEntity.notFound().build();
		} catch (InsufficientBalanceException isbe) {
			return ResponseEntity.badRequest().build();
		}
	}
}
