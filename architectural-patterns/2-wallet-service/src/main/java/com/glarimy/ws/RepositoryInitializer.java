package com.glarimy.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.glarimy.ws.domain.Wallet;
import com.glarimy.ws.domain.WalletRepository;

@Component
public class RepositoryInitializer implements ApplicationRunner {
	@Autowired
	private WalletRepository repo;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		Wallet wallet = new Wallet();
		wallet.setPhone(9731423166L);
		wallet.setBalance(100);
		repo.save(wallet);
	}
}
