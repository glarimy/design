package com.glarimy.ws.domain;

public class Transaction {
	private long phone;
	private double amount;
	private boolean success;
	private double balance;

	public Transaction() {

	}

	public Transaction(long phone, double amount, boolean success, double balance) {
		super();
		this.phone = phone;
		this.amount = amount;
		this.success = success;
		this.balance = balance;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

}
