# Wallet Service API

## Usage

* Database: http://localhost:8080/h2-console/ with jdbc:h2:mem:wallets-db
* Tests: http://localhost:8080/swagger-ui/index.html
* curl -X 'POST' 'http://localhost:8080/wallet/123/debit?amount=50'
* curl -X 'POST' 'http://localhost:8080/wallet/123/credit?amount=50'
