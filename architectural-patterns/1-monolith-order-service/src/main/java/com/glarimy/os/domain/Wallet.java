package com.glarimy.os.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="wallets")
public class Wallet {
	@Id
	private long phone;
	private double balance;

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

}
