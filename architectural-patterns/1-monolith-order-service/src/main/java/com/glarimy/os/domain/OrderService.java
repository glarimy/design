package com.glarimy.os.domain;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Service
@EnableTransactionManagement
public class OrderService {
	@Autowired
	private OrderRepository repo;

	@Autowired
	private ProductService productService;

	@Autowired
	private WalletService walletService;

	@Transactional
	public Order create(Order order) throws OrderFailedException {
		order = repo.save(order);

		try {
			productService.reserve(order.getProductId(), order.getQuantity());
		} catch (Exception e) {
			order.setStatus(Status.FAILED);
			throw new OrderFailedException();
		}

		try {
			walletService.debit(order.getPhone(), order.getAmount());
			order.setStatus(Status.CONFIRMED);
			return order;
		} catch (Exception e) {
			try {
				productService.release(order.getProductId(), order.getQuantity());
			} catch (ProductNotFoundException pnfe) {
			}
			order.setStatus(Status.FAILED);
			throw new OrderFailedException();
		}
	}

	public Order find(int id) throws OrderNotFoundException {
		return repo.findById(id).orElseThrow(() -> new OrderNotFoundException());
	}

}
