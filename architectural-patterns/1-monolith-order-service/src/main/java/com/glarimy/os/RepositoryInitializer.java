package com.glarimy.os;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.glarimy.os.domain.Product;
import com.glarimy.os.domain.ProductRepository;
import com.glarimy.os.domain.Wallet;
import com.glarimy.os.domain.WalletRepository;

@Component
public class RepositoryInitializer implements ApplicationRunner {
	@Autowired
	private ProductRepository productRepo;

	@Autowired
	private WalletRepository walletRepo;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		Wallet wallet = new Wallet();
		wallet.setPhone(9731423166L);
		wallet.setBalance(100);
		walletRepo.save(wallet);

		Product product = new Product();
		product.setProductId(1);
		product.setQuantity(10);
		productRepo.save(product);
	}
}
