log = {}

class Clock:
    def __init__(self):
        self.time = 0

    def synchronize(self, time):
        if(time > self.time):
            self.time = time
        self.time = self.time

    def increment(self):
        self.time = self.time + 1
    
    def getTime(self):
        return self.time

class Request:
    def __init__(self, time):
        self.time = time

class Node:
    def __init__(self, name):
        self.name = name
        self.clock = Clock()
    
    def process(self, request):
        self.clock.synchronize(request.time)
        log[self.clock.getTime()] = self.name + " node processed the request"
        return self.clock.getTime()

    def startTx(self):
        self.clock.increment()
        time = first.process(Request(self.clock.getTime()))
        self.clock.increment()
        time = second.process(Request(self.clock.getTime()))
        self.clock.increment()
        time = third.process(Request(self.clock.getTime()))
        self.clock.increment()
        time = first.process(Request(self.clock.getTime()))

    def commitTx(self):
        print(log)

first = Node("first")
second = Node("second")
third = Node("third")
fourth = Node("fourth")
some = Node("some")
some.startTx()
some.commitTx()
other = Node("other")
other.startTx()
other.commitTx()
