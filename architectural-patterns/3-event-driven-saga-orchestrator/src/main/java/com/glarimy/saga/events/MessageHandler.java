package com.glarimy.saga.events;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.glarimy.saga.domain.Order;

@Service
public class MessageHandler {

	private Map<Integer, Order> orders;

	@Value("${wallet.command.producer.topic}")
	private String wCommandTopic;

	@Value("${product.command.producer.topic}")
	private String pCommandTopic;

	@Value("${saga.result.producer.topic}")
	private String sResultTopic;

	@Autowired
	private KafkaTemplate<String, Message> kafka;

	public MessageHandler() {
		orders = new HashMap<Integer, Order>();
	}

	@KafkaListener(topics = "${saga.command.consumer.topic}", groupId = "${saga.consumer.group}")
	public void onSagaCommand(Message command) {
		System.out.println("Received: " + command);
		Order order = new Order();
		order.setOrderId((Integer) command.getData().get("orderId"));
		order.setPhone((Long) command.getData().get("phone"));
		order.setProductId((Integer) command.getData().get("productId"));
		order.setQuantity((Integer) command.getData().get("quantity"));
		order.setAmount((Double) command.getData().get("amount"));
		orders.put(order.getOrderId(), order);
		reserveProduct(order.getOrderId());
	}

	@KafkaListener(topics = "${product.result.consumer.topic}", groupId = "${saga.consumer.group}}")
	public void onProductResponse(Message response) {
		System.out.println("Received: " + response);
		if (response.getOperation().equals("reserve")) {
			if (response.isSuccess())
				this.debitWallet(response.getId());
			else
				this.rejectOrder(response.getId());
		}
	}

	@KafkaListener(topics = "${wallet.result.consumer.topic}", groupId = "${saga.consumer.group}}")
	public void onWalletResponse(Message response) {
		System.out.println("Received: " + response);
		if (response.getOperation().equals("debit")) {
			if (response.isSuccess())
				this.confirmOrder(response.getId());
			else {
				this.releaseProduct(response.getId());
				this.rejectOrder(response.getId());
			}
		}
	}

	public void reserveProduct(int id) {
		Order order = orders.get(id);
		Message command = new Message();
		command.setId(id);
		command.setType("command");
		command.setOperation("reserve");
		command.getData().put("productId", order.getProductId());
		command.getData().put("quantity", order.getQuantity());
		System.out.println("Sedning" + command);
		kafka.send(pCommandTopic, Integer.toString(id), command);
	}

	public void releaseProduct(int id) {
		Order order = orders.get(id);
		Message command = new Message();
		command.setId(id);
		command.setType("command");
		command.setOperation("release");
		command.getData().put("productId", order.getProductId());
		command.getData().put("quantity", order.getQuantity());
		System.out.println("Sedning" + command);
		kafka.send(pCommandTopic, Integer.toString(id), command);
	}

	public void debitWallet(int id) {
		Order order = orders.get(id);
		Message command = new Message();
		command.setId(id);
		command.setType("command");
		command.setOperation("debit");
		command.getData().put("phone", order.getPhone());
		command.getData().put("amount", order.getAmount());
		System.out.println("Sedning" + command);
		kafka.send(wCommandTopic, Integer.toString(id), command);
	}

	public void confirmOrder(int id) {
		Message result = new Message();
		result.setId(id);
		result.setType("result");
		result.setOperation("process");
		result.setSuccess(true);
		System.out.println("Sedning" + result);
		kafka.send(sResultTopic, Integer.toString(id), result);
	}

	public void rejectOrder(int id) {
		Message result = new Message();
		result.setId(id);
		result.setType("result");
		result.setOperation("process");
		result.setSuccess(false);
		System.out.println("Sedning" + result);
		kafka.send(sResultTopic, Integer.toString(id), result);
	}

}