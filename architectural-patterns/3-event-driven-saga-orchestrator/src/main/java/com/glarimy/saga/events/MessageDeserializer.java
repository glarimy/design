package com.glarimy.saga.events;

import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MessageDeserializer implements Deserializer<Message> {
	@Override
	public Message deserialize(String topic, byte[] data) {
		try {
			return new ObjectMapper().readValue(data, Message.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
