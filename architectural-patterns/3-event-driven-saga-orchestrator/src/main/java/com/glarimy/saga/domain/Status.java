package com.glarimy.saga.domain;

public enum Status {
	PENDING, CONFIRMED, FAILED
}
