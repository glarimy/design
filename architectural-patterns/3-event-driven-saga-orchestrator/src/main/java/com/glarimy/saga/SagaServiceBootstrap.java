package com.glarimy.saga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SagaServiceBootstrap {

	public static void main(String[] args) {
		SpringApplication.run(SagaServiceBootstrap.class, args);
	}

}
