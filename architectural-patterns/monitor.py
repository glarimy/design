import time
from threading import Lock, Thread, current_thread

class Manager:
    def __init__(self):
        self.lock = Lock()
    def allocate(self, guest, duration):
        print(guest, ' requested the room')
        self.lock.acquire()
        print(guest, ' occupied the room')
        time.sleep(duration)
        print(guest, ' left the room')
        self.lock.release()

class Client(Thread):
    def __init__(self, name, duration):
        Thread.__init__(self)
        self.name = name
        self.duration = duration

    def run(self):
        manager.allocate(self.name, self.duration)

manager = Manager()
Client("Krishna", 5).start()
Client("Mohan", 5).start()