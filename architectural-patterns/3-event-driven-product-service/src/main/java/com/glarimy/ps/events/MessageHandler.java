package com.glarimy.ps.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.glarimy.ps.domain.InsufficientInventoryException;
import com.glarimy.ps.domain.ProductNotFoundException;
import com.glarimy.ps.domain.ProductService;

@Component
public class MessageHandler {
	@Autowired
	private ProductService service;

	@Value("${product.producer.topic}")
	private String topic;

	@Autowired
	private KafkaTemplate<String, Message> kafka;

	@KafkaListener(topics = "${product.consumer.topic}", groupId = "${product.consumer.group}")
	public void handle(Message command) {
		System.out.println("Received: " + command);
		Message result = new Message();
		result.setId(command.getId());
		result.setType("result");
		result.setOperation(command.getOperation());
		result.setData(command.getData());

		try {
			if (command.getOperation().equals("reserve")) {
				service.reserve((Integer) command.getData().get("productId"),
						(Integer) command.getData().get("quantity"));
				result.setSuccess(true);
			} else if (command.getOperation().equals("debit")) {
				service.release((Integer) command.getData().get("productId"),
						(Integer) command.getData().get("quantity"));
				result.setSuccess(true);
			} else {
				System.out.println("Unsupported operation");
				result.setSuccess(false);
			}
		} catch (ProductNotFoundException e) {
			System.out.println("Product not found");
			result.setSuccess(false);
		} catch (InsufficientInventoryException e) {
			System.out.println("Insufficient inventory");
			result.setSuccess(false);
		}
		System.out.println("Sending: " + result);
		kafka.send(topic, Integer.toString(result.getId()), result);
	}
}
