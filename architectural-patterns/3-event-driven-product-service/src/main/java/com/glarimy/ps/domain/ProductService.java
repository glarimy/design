package com.glarimy.ps.domain;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Service
@EnableTransactionManagement
public class ProductService {
	@Autowired
	private ProductRepository repo;

	@Transactional
	public Product reserve(int id, int quantity) throws ProductNotFoundException, InsufficientInventoryException {
		Product product = repo.findById(id).orElseThrow(() -> new ProductNotFoundException());
		if (product.getQuantity() < quantity)
			throw new InsufficientInventoryException();
		product.setQuantity(product.getQuantity() - quantity);
		return product;
	}

	@Transactional
	public Product release(int id, int quantity) throws ProductNotFoundException {
		Product product = repo.findById(id).orElseThrow(() -> new ProductNotFoundException());
		product.setQuantity(product.getQuantity() + quantity);
		return product;
	}

}
