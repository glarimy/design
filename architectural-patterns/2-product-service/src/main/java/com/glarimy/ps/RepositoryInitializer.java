package com.glarimy.ps;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.glarimy.ps.domain.Product;
import com.glarimy.ps.domain.ProductRepository;

@Component
public class RepositoryInitializer implements ApplicationRunner {
	@Autowired
	private ProductRepository repo;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		Product product = new Product();
		product.setProductId(1);
		product.setQuantity(10);
		repo.save(product);
	}
}
