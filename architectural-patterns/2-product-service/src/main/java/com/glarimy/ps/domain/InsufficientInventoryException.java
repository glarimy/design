package com.glarimy.ps.domain;

@SuppressWarnings("serial")
public class InsufficientInventoryException extends Exception {

	public InsufficientInventoryException() {
		// TODO Auto-generated constructor stub
	}

	public InsufficientInventoryException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InsufficientInventoryException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InsufficientInventoryException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InsufficientInventoryException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
