package com.glarimy.ps.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "products")
public class Product {
	@Id
	private int productId;
	private int quantity;

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
