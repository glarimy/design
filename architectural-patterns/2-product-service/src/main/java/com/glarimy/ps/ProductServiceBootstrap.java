package com.glarimy.ps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductServiceBootstrap {

	public static void main(String[] args) {
		SpringApplication.run(ProductServiceBootstrap.class, args);
	}

}
