package com.glarimy.ps.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.glarimy.ps.domain.InsufficientInventoryException;
import com.glarimy.ps.domain.Product;
import com.glarimy.ps.domain.ProductNotFoundException;
import com.glarimy.ps.domain.ProductService;

@RestController
public class ProductController {

	@Autowired
	private ProductService service;

	@PostMapping("/product/{id}/reserve")
	public ResponseEntity<Product> reserve(@PathVariable("id") int id, @RequestParam("quantity") int quantity) {
		try {
			Product product = service.reserve(id, quantity);
			return ResponseEntity.ok(product);
		} catch (ProductNotFoundException wnfe) {
			return ResponseEntity.notFound().build();
		} catch (InsufficientInventoryException isbe) {
			return ResponseEntity.badRequest().build();
		}
	}

	@PostMapping("/product/{id}/release")
	public ResponseEntity<Product> release(@PathVariable("id") int id, @RequestParam("quantity") int quantity) {
		try {
			Product product = service.release(id, quantity);
			return ResponseEntity.ok(product);
		} catch (ProductNotFoundException e) {
			return ResponseEntity.notFound().build();
		}

	}
}
