# Wallet Service API

## Usage

* Database: http://localhost:8080/h2-console/ with jdbc:h2:mem:products-db
* Tests: http://localhost:8080/swagger-ui/index.html
* curl -X 'POST' 'http://localhost:8080/product/78/reserve?quantity=1'
* curl -X 'POST' 'http://localhost:8080/product/78/release?quantity=1'
